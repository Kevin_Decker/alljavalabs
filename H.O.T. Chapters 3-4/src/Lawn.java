
public class Lawn {

	private double _width;
	private double _length;
	
	//constructors
	public Lawn()
	{
		
	}
	
	public Lawn(double width, double length)
	{
		_width = width;
		_length = length;
	}
	
	//mutators
	public void setWidth(double w)
	{
		_width = w;
	}
	
	public void setLength(double l)
	{
		_length = l;
	}
	
	//accessors
	public double getWidth()
	{
		return _width;
	}
	
	public double getLength()
	{
		return _length;
	}
	
	//method that calculates and returns the square feet of a lawn
	public double calcSquareFeet()
	{
		double sqFeet = _width * _length;
		return sqFeet;
	}
	
}
