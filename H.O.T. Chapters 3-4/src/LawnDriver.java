
import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter width 1: ");
		double userWidth = userInput.nextDouble();
		
		System.out.println("Enter length 1: ");
		double userLength = userInput.nextDouble();
		
		System.out.println("Enter width 2: ");
		double width = userInput.nextDouble();
		
		System.out.println("Enter length 2: ");
		double length = userInput.nextDouble();
		
		Lawn lawn1 = new Lawn(userWidth, userLength);
		double law1 = lawn1.calcSquareFeet();
		
		Lawn lawn2 = new Lawn();
		lawn2.setWidth(width);
		lawn2.setLength(length);
		double law2 = lawn2.calcSquareFeet();
		
		double weeklyFee = 0;
		
		//conditional to test first set of width and length
		if(law1 < 400)
		{
			weeklyFee = 25;
			System.out.println("SqFeet: " + law1);
			System.out.printf("Weekly Mowing Fee: $%,.2f\n", weeklyFee);
			System.out.printf("Total Fee (20-week season): $%,.2f", weeklyFee * 20);
		}
		else if(law1 < 600)
		{
			weeklyFee = 35;
			System.out.println("\nSqFeet: " + law1);
			System.out.printf("Weekly Mowing Fee: $%,.2f\n", weeklyFee);
			System.out.printf("Total Fee (20-week season): $%,.2f", weeklyFee * 20);
		}
		else
		{
			weeklyFee = 50;
			System.out.println("\nSqFeet: " + law1);
			System.out.printf("Weekly Mowing Fee: $%,.2f\n", weeklyFee);
			System.out.printf("Total Fee (20-week season): $%,.2f", weeklyFee * 20);
		}
		
		System.out.println();
		
		//conditional to test second set of width and length
		if(law2 < 400)
		{
			weeklyFee = 25;
			System.out.println("\nSqFeet: " + law2);
			System.out.printf("Weekly Mowing Fee: $%,.2f\n", weeklyFee);
			System.out.printf("Total Fee (20-week season): $%,.2f", weeklyFee * 20);
		}
		else if(law2 < 600)
		{
			weeklyFee = 35;
			System.out.println("\nSqFeet: " + law2);
			System.out.printf("Weekly Mowing Fee: $%,.2f\n", weeklyFee);
			System.out.printf("Total Fee (20-week season): $%,.2f", weeklyFee * 20);
		}
		else 
		{
			weeklyFee = 50;
			System.out.println("\nSqFeet: " +law2);
			System.out.printf("Weekly Mowing Fee: $%,.2f\n", weeklyFee);
			System.out.printf("Total Fee (20-week season): $%,.2f", weeklyFee * 20);
		}
		
	}

}
