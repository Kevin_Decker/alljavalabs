﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class MainControl : UserControl
    {
        private Form1 _form;

        public MainControl(Form1 form)
        {
            InitializeComponent();
            _form = form;
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            _form.InventoryScreen();
        }
    }
}
