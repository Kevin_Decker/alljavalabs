﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class InventoryControl : UserControl
    {
        private GameManager _curGame;
        RandomItemGenerator random = new RandomItemGenerator();
        private Form1 _form;
        Item dropItem;

        public GameManager CurGame
        {
            get { return _curGame; }
            set { _curGame = value; }
        }

        private void InventoryControl_Load(object sender, EventArgs e)
        {
            weaponLabel.Text = "Bronze Sword - " + _curGame.Player.CalcTotalAttackValue();
            bodyLabel.Text = "Iron Chestpiece - " + _curGame.Player.CalcTotalDefenseValue();
            //_curGame.Player.Bag.AddItem();
            //bagBox.Text = _curGame.Player.Bag.ToString();
        }

        public InventoryControl(Form1 form)
        {
            InitializeComponent();
            _form = form;
        }

        private void fightBtn_Click(object sender, EventArgs e)
        {
            _form.FightScreen();
        }

        private void bagBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void addItemBtn_Click(object sender, EventArgs e)
        {
            Item loot = (Item)lootBag.SelectedItem;

            // FIXME: check if loot is null
            bagBox.Items.Add(loot);
            lootBag.Items.Remove(loot);
            _curGame.Player.Bag.AddItem(loot);
            bagCountLabel.Text = _curGame.Player.Bag.Count.ToString();
        }

        private void removeItemBtn_Click(object sender, EventArgs e)
        {
            Item loot = (Item)bagBox.SelectedItem;

            // FIXME: check if loot is null
            lootBag.Items.Add(loot);
            bagBox.Items.Remove(loot);
            _curGame.Player.Bag.RemoveItem(loot);
            bagCountLabel.Text = _curGame.Player.Bag.Count.ToString();
        }

        private void equiptBtn_Click(object sender, EventArgs e)
        {
            Item item = (Item)bagBox.SelectedItem;
            
            if (item.Slot == InventorySlotId.HELMET)
            {
                headLabel.Text = Convert.ToString(item);
                _curGame.Player.Equipped.Equip(InventorySlotId.HELMET, item);
                //bagBox.SelectedItem = "";
            }
            else if (item.Slot == InventorySlotId.CHESTPIECE)
            {
                bodyLabel.Text = Convert.ToString(item);
                _curGame.Player.Equipped.Equip(InventorySlotId.CHESTPIECE, item);
                //bagBox.SelectedItem = "";
            }
            else if (item.Slot == InventorySlotId.GRIEVES)
            {
                legsLabel.Text = Convert.ToString(item);
                _curGame.Player.Equipped.Equip(InventorySlotId.GRIEVES, item);
                //bagBox.SelectedItem = "";
            }
            else if (item.Slot == InventorySlotId.VAMBRACES)
            {
                armsLabel.Text = Convert.ToString(item);
                _curGame.Player.Equipped.Equip(InventorySlotId.VAMBRACES, item);
                //bagBox.SelectedItem = "";
            }
            else if (item.Slot == InventorySlotId.WEAPON)
            {
                weaponLabel.Text = Convert.ToString(item);
                _curGame.Player.Equipped.Equip(InventorySlotId.WEAPON, item);
                //bagBox.SelectedItem = "";
            }
            else if (item.Slot == InventorySlotId.POTION1)
            {
                potion1Label.Text = Convert.ToString(item);
                _curGame.Player.Equipped.Equip(InventorySlotId.POTION1, item);
            }


            // if potion slot 1 is full move item to slot 2
            //if ()
            //{

            //}
        }

        private void unequipBtn_Click(object sender, EventArgs e)
        {
            Item item = (Item)bagBox.SelectedItem;

            if (item.Slot == InventorySlotId.HELMET)
            {
                headLabel.Text = "Empty";
                _curGame.Player.Equipped.Unequip(InventorySlotId.HELMET);
            }
            else if (item.Slot == InventorySlotId.CHESTPIECE)
            {
                bodyLabel.Text = "Empty";
                _curGame.Player.Equipped.Unequip(InventorySlotId.CHESTPIECE);
            }
            else if (item.Slot == InventorySlotId.GRIEVES)
            {
                legsLabel.Text = "Empty";
                _curGame.Player.Equipped.Unequip(InventorySlotId.GRIEVES);
            }
            else if (item.Slot == InventorySlotId.VAMBRACES)
            {
                armsLabel.Text = "Empty";
                _curGame.Player.Equipped.Unequip(InventorySlotId.VAMBRACES);
            }
            else if (item.Slot == InventorySlotId.WEAPON)
            {
                weaponLabel.Text = "Empty";
                _curGame.Player.Equipped.Unequip(InventorySlotId.WEAPON);
            }
            else if (item.Slot == InventorySlotId.POTION1)
            {
                potion1Label.Text = "Empty";
                _curGame.Player.Equipped.Unequip(InventorySlotId.POTION1);
            }
            else if (item.Slot == InventorySlotId.POTION2)
            {
                potion2Label.Text = "Empty";
                _curGame.Player.Equipped.Unequip(InventorySlotId.POTION2);
            }
        }

        private void Drop_Click(object sender, EventArgs e)
        {
            bagBox.Items.Remove(bagBox.SelectedItem);
            lootBag.Items.Remove(lootBag.SelectedItem);
            //_curGame.Player.Bag.RemoveItem(dropItem);
            bagCountLabel.Text = _curGame.Player.Bag.Count.ToString();
        }

        private void InventoryControl_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                if (_curGame.Enemy != null)
                {
                    bagCountLabel.Text = _curGame.Player.Bag.Count.ToString();

                    lootBag.Items.Clear();

                    for (int i = 0; i < _curGame.Enemy.Bag.Count; i++)
                    {
                        lootBag.Items.Add(_curGame.Enemy.Bag.GetItem(i));
                    }

                    bagBox.Items.Clear();

                    for (int i = 0; i < _curGame.Player.Bag.Count; i++)
                    {
                        bagBox.Items.Add(_curGame.Player.Bag.GetItem(i));
                    }
                    

                    headLabel.Text = Convert.ToString(_curGame.Player.Equipped.GetItem(InventorySlotId.HELMET));
                    bodyLabel.Text = Convert.ToString(_curGame.Player.Equipped.GetItem(InventorySlotId.CHESTPIECE));
                    legsLabel.Text = Convert.ToString(_curGame.Player.Equipped.GetItem(InventorySlotId.GRIEVES));
                    armsLabel.Text = Convert.ToString(_curGame.Player.Equipped.GetItem(InventorySlotId.VAMBRACES));
                    weaponLabel.Text = Convert.ToString(_curGame.Player.Equipped.GetItem(InventorySlotId.WEAPON));
                }
            }
        }


    }
}
