﻿namespace Midterm
{
    partial class FightControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FightControl));
            this.label1 = new System.Windows.Forms.Label();
            this.attackBtn = new System.Windows.Forms.Button();
            this.potion1Btn = new System.Windows.Forms.Button();
            this.potion2Btn = new System.Windows.Forms.Button();
            this.heroPic = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.enemyPic = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.playerHealth = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.enemyHealth = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.depthLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.heroAttackLabel = new System.Windows.Forms.Label();
            this.heroDefenseLabel = new System.Windows.Forms.Label();
            this.enemyAttackLabel = new System.Windows.Forms.Label();
            this.enemyDefenseLabel = new System.Windows.Forms.Label();
            this.enemyLabel = new System.Windows.Forms.Label();
            this.playerLabel = new System.Windows.Forms.Label();
            this.playerDefenseLabel = new System.Windows.Forms.Label();
            this.enemyDefLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.heroPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyPic)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(324, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "Combat";
            // 
            // attackBtn
            // 
            this.attackBtn.Location = new System.Drawing.Point(313, 441);
            this.attackBtn.Name = "attackBtn";
            this.attackBtn.Size = new System.Drawing.Size(143, 47);
            this.attackBtn.TabIndex = 1;
            this.attackBtn.Text = "Attack";
            this.attackBtn.UseVisualStyleBackColor = true;
            this.attackBtn.Click += new System.EventHandler(this.attackBtn_Click);
            // 
            // potion1Btn
            // 
            this.potion1Btn.Location = new System.Drawing.Point(142, 442);
            this.potion1Btn.Name = "potion1Btn";
            this.potion1Btn.Size = new System.Drawing.Size(122, 46);
            this.potion1Btn.TabIndex = 2;
            this.potion1Btn.Text = "Potion 1";
            this.potion1Btn.UseVisualStyleBackColor = true;
            this.potion1Btn.Click += new System.EventHandler(this.potion1Btn_Click);
            // 
            // potion2Btn
            // 
            this.potion2Btn.Location = new System.Drawing.Point(512, 441);
            this.potion2Btn.Name = "potion2Btn";
            this.potion2Btn.Size = new System.Drawing.Size(124, 46);
            this.potion2Btn.TabIndex = 3;
            this.potion2Btn.Text = "Potion 2";
            this.potion2Btn.UseVisualStyleBackColor = true;
            // 
            // heroPic
            // 
            this.heroPic.Location = new System.Drawing.Point(133, 159);
            this.heroPic.Name = "heroPic";
            this.heroPic.Size = new System.Drawing.Size(172, 183);
            this.heroPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.heroPic.TabIndex = 4;
            this.heroPic.TabStop = false;
            this.heroPic.Click += new System.EventHandler(this.heroPic_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(181, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Player";
            // 
            // enemyPic
            // 
            this.enemyPic.Location = new System.Drawing.Point(488, 159);
            this.enemyPic.Name = "enemyPic";
            this.enemyPic.Size = new System.Drawing.Size(175, 183);
            this.enemyPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.enemyPic.TabIndex = 6;
            this.enemyPic.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(523, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "Enemy";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(158, 363);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Health: ";
            // 
            // playerHealth
            // 
            this.playerHealth.AutoSize = true;
            this.playerHealth.BackColor = System.Drawing.Color.Transparent;
            this.playerHealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerHealth.Location = new System.Drawing.Point(248, 363);
            this.playerHealth.Name = "playerHealth";
            this.playerHealth.Size = new System.Drawing.Size(27, 20);
            this.playerHealth.TabIndex = 9;
            this.playerHealth.Text = "50";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(496, 363);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Health: ";
            // 
            // enemyHealth
            // 
            this.enemyHealth.AutoSize = true;
            this.enemyHealth.BackColor = System.Drawing.Color.Transparent;
            this.enemyHealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyHealth.Location = new System.Drawing.Point(597, 363);
            this.enemyHealth.Name = "enemyHealth";
            this.enemyHealth.Size = new System.Drawing.Size(27, 20);
            this.enemyHealth.TabIndex = 11;
            this.enemyHealth.Text = "25";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(337, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Game Depth";
            // 
            // depthLabel
            // 
            this.depthLabel.AutoSize = true;
            this.depthLabel.BackColor = System.Drawing.Color.Transparent;
            this.depthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.depthLabel.Location = new System.Drawing.Point(372, 211);
            this.depthLabel.Name = "depthLabel";
            this.depthLabel.Size = new System.Drawing.Size(18, 20);
            this.depthLabel.TabIndex = 13;
            this.depthLabel.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Atk.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 257);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 20);
            this.label8.TabIndex = 15;
            this.label8.Text = "Def.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(748, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 20);
            this.label9.TabIndex = 16;
            this.label9.Text = "Atk.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(746, 257);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 20);
            this.label10.TabIndex = 17;
            this.label10.Text = "Def.";
            // 
            // heroAttackLabel
            // 
            this.heroAttackLabel.AutoSize = true;
            this.heroAttackLabel.BackColor = System.Drawing.Color.Transparent;
            this.heroAttackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heroAttackLabel.Location = new System.Drawing.Point(64, 194);
            this.heroAttackLabel.Name = "heroAttackLabel";
            this.heroAttackLabel.Size = new System.Drawing.Size(0, 20);
            this.heroAttackLabel.TabIndex = 18;
            // 
            // heroDefenseLabel
            // 
            this.heroDefenseLabel.AutoSize = true;
            this.heroDefenseLabel.BackColor = System.Drawing.Color.Transparent;
            this.heroDefenseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heroDefenseLabel.Location = new System.Drawing.Point(76, 257);
            this.heroDefenseLabel.Name = "heroDefenseLabel";
            this.heroDefenseLabel.Size = new System.Drawing.Size(0, 20);
            this.heroDefenseLabel.TabIndex = 19;
            // 
            // enemyAttackLabel
            // 
            this.enemyAttackLabel.AutoSize = true;
            this.enemyAttackLabel.BackColor = System.Drawing.Color.Transparent;
            this.enemyAttackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyAttackLabel.Location = new System.Drawing.Point(698, 194);
            this.enemyAttackLabel.Name = "enemyAttackLabel";
            this.enemyAttackLabel.Size = new System.Drawing.Size(0, 20);
            this.enemyAttackLabel.TabIndex = 20;
            // 
            // enemyDefenseLabel
            // 
            this.enemyDefenseLabel.AutoSize = true;
            this.enemyDefenseLabel.BackColor = System.Drawing.Color.Transparent;
            this.enemyDefenseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyDefenseLabel.Location = new System.Drawing.Point(709, 257);
            this.enemyDefenseLabel.Name = "enemyDefenseLabel";
            this.enemyDefenseLabel.Size = new System.Drawing.Size(0, 20);
            this.enemyDefenseLabel.TabIndex = 21;
            // 
            // enemyLabel
            // 
            this.enemyLabel.AutoSize = true;
            this.enemyLabel.BackColor = System.Drawing.Color.Transparent;
            this.enemyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyLabel.Location = new System.Drawing.Point(537, 132);
            this.enemyLabel.Name = "enemyLabel";
            this.enemyLabel.Size = new System.Drawing.Size(108, 24);
            this.enemyLabel.TabIndex = 22;
            this.enemyLabel.Text = "enemylabel";
            // 
            // playerLabel
            // 
            this.playerLabel.AutoSize = true;
            this.playerLabel.BackColor = System.Drawing.Color.Transparent;
            this.playerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerLabel.Location = new System.Drawing.Point(189, 132);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(52, 24);
            this.playerLabel.TabIndex = 23;
            this.playerLabel.Text = "Hero";
            // 
            // playerDefenseLabel
            // 
            this.playerDefenseLabel.AutoSize = true;
            this.playerDefenseLabel.BackColor = System.Drawing.Color.Transparent;
            this.playerDefenseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerDefenseLabel.ForeColor = System.Drawing.Color.Black;
            this.playerDefenseLabel.Location = new System.Drawing.Point(64, 257);
            this.playerDefenseLabel.Name = "playerDefenseLabel";
            this.playerDefenseLabel.Size = new System.Drawing.Size(0, 20);
            this.playerDefenseLabel.TabIndex = 24;
            // 
            // enemyDefLabel
            // 
            this.enemyDefLabel.AutoSize = true;
            this.enemyDefLabel.BackColor = System.Drawing.Color.Transparent;
            this.enemyDefLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyDefLabel.Location = new System.Drawing.Point(698, 257);
            this.enemyDefLabel.Name = "enemyDefLabel";
            this.enemyDefLabel.Size = new System.Drawing.Size(0, 20);
            this.enemyDefLabel.TabIndex = 25;
            // 
            // FightControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.Controls.Add(this.enemyDefLabel);
            this.Controls.Add(this.playerDefenseLabel);
            this.Controls.Add(this.playerLabel);
            this.Controls.Add(this.enemyLabel);
            this.Controls.Add(this.enemyDefenseLabel);
            this.Controls.Add(this.enemyAttackLabel);
            this.Controls.Add(this.heroDefenseLabel);
            this.Controls.Add(this.heroAttackLabel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.depthLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.enemyHealth);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.playerHealth);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.enemyPic);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.heroPic);
            this.Controls.Add(this.potion2Btn);
            this.Controls.Add(this.potion1Btn);
            this.Controls.Add(this.attackBtn);
            this.Controls.Add(this.label1);
            this.Name = "FightControl";
            this.Size = new System.Drawing.Size(801, 604);
            this.Load += new System.EventHandler(this.FightControl_Load);
            this.VisibleChanged += new System.EventHandler(this.FightControl_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.heroPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button attackBtn;
        private System.Windows.Forms.Button potion1Btn;
        private System.Windows.Forms.Button potion2Btn;
        private System.Windows.Forms.PictureBox heroPic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox enemyPic;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label playerHealth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label enemyHealth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label depthLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label heroAttackLabel;
        private System.Windows.Forms.Label heroDefenseLabel;
        private System.Windows.Forms.Label enemyAttackLabel;
        private System.Windows.Forms.Label enemyDefenseLabel;
        private System.Windows.Forms.Label enemyLabel;
        private System.Windows.Forms.Label playerLabel;
        private System.Windows.Forms.Label playerDefenseLabel;
        private System.Windows.Forms.Label enemyDefLabel;
    }
}
