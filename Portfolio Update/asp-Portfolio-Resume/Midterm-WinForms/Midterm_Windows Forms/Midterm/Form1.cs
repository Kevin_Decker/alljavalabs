﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class Form1 : Form
    {
        private InventoryControl _invScreen;
        private MainControl _mainScreen;
        private FightControl _fightScreen;
        private GameOver _gameOver;
        private GameManager _curGame = new GameManager();

        public Form1()
        {
            InitializeComponent();

            _mainScreen = new MainControl(this);
            _mainScreen.Dock = DockStyle.Fill;
            _mainScreen.Visible = true;
            this.Controls.Add(_mainScreen);

            _invScreen = new InventoryControl(this);
            _invScreen.Dock = DockStyle.Fill;
            _invScreen.Visible = false;
            this.Controls.Add(_invScreen);

            _fightScreen = new FightControl(this);
            _fightScreen.Dock = DockStyle.Fill;
            _fightScreen.Visible = false;
            this.Controls.Add(_fightScreen);

            _gameOver = new GameOver(this);
            _gameOver.Dock = DockStyle.Fill;
            _gameOver.Visible = false;
            this.Controls.Add(_gameOver);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _invScreen.CurGame = _curGame;
            _fightScreen.CurGame = _curGame;
        }

        public void InventoryScreen()
        {
            _invScreen.Visible = true;
            _mainScreen.Visible = false;
            _fightScreen.Visible = false;
            _gameOver.Visible = false;
        }

        public void FightScreen()
        {
            _fightScreen.Visible = true;
            _invScreen.Visible = false;
            _gameOver.Visible = false;
            _mainScreen.Visible = false;
        }

        public void GameOver()
        {
            _gameOver.Visible = true;
            _fightScreen.Visible = false;
            _invScreen.Visible = false;
            _mainScreen.Visible = false;
        }


    }
}
