﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class FightControl : UserControl
    {
        private GameManager _curGame;
        private Form1 _form;

        public GameManager CurGame {get { return _curGame; } set { _curGame = value; }}

        public FightControl(Form1 form)
        {
            InitializeComponent();
            _form = form;
        }

        private void FightControl_Load(object sender, EventArgs e)
        {
            heroPic.Load("realHero.png");
        }

        private void attackBtn_Click(object sender, EventArgs e)
        {
            _curGame.Attack();
            playerHealth.Text = Convert.ToString(_curGame.Player.CurrentHealth);
            enemyHealth.Text = Convert.ToString(_curGame.Enemy.CurrentHealth);

            if (_curGame.Player.CurrentHealth == 0)
            {
                // go to game over screen
                _form.GameOver();
            }
            else if (_curGame.Enemy.CurrentHealth == 0)
            {
                // go to inventory screen
                _curGame.nextDepth();
                _form.InventoryScreen();
            }
        }

        private void FightControl_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                _curGame.NextBattle();
                enemyHealth.Text = "25";

                //if (_curGame.Enemy == (Rat)_curGame.Enemy)
                //{
                //    enemyPic.Load("rat.png");
                //}
                //else if (_curGame.Enemy == (WildDog)_curGame.Enemy)
                //{
                //    enemyPic.Load("dog.png");
                //}
                //else if (_curGame.Enemy == (Wizard)_curGame.Enemy)
                //{
                //    enemyPic.Load("wizard.png");
                //}
                //else
                //{
                //    enemyPic.Load("");
                //}

                heroAttackLabel.Text = Convert.ToString(_curGame.Player.CalcTotalAttackValue());
                playerDefenseLabel.Text = Convert.ToString(_curGame.Player.CalcTotalDefenseValue());
                enemyAttackLabel.Text = Convert.ToString(_curGame.Enemy.CalcTotalAttackValue());
                enemyDefLabel.Text = Convert.ToString(_curGame.Enemy.CalcTotalDefenseValue());
                enemyLabel.Text = Convert.ToString(_curGame.Enemy);
                playerLabel.Text = Convert.ToString(_curGame.Player);
                depthLabel.Text = Convert.ToString(_curGame.Depth);
            }
        }

        private void heroPic_Click(object sender, EventArgs e)
        {

        }

        private void potion1Btn_Click(object sender, EventArgs e)
        {
            
        }
    }
}
