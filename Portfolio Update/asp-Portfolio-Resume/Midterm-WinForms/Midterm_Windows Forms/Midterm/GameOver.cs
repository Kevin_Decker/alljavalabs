﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Midterm
{
    public partial class GameOver : UserControl
    {
        private Form1 _form;
        //private GameManager _curGame;

        public GameOver(Form1 form)
        {
            InitializeComponent();
            _form = form;
            //roundsLabel.Text = "you lasted " + _curGame.Depth + "rounds.";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _form.InventoryScreen();
        }
    }
}
