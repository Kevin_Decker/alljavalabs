﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class Hero : Character
    {

        public Hero()
        {      
            _name = "Hero";
            _currentHealth = 50;
            _dead = false;
            _attackValue = 0;
            _defenseValue = 0;
        }

        public override string ToString()
        {
            return string.Format("{0}", _name);
        }
    }
}
