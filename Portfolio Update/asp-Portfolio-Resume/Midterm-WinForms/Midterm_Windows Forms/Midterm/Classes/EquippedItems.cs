﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class EquippedItems //: Item
    {
        private Item[] _slots;

        //private string _name;
        //private double _weight;
        //private InventorySlotId _slot;
        //private bool _isNatural;

        //public override bool IsNatural { get { return _isNatural; } }
        //public override string Name { get { return _name; } }
        //public override double Weight { get { return _weight; } }
        //public override InventorySlotId Slot { get { return _slot; } }

        public EquippedItems()
        {
            _slots = new Item[8];
        }

        public Item GetItem(InventorySlotId slot)
        {
            // FIXME: See chapter 2
            //        return _slots[(int)slot];

            if (slot == InventorySlotId.HELMET)
            {
                return _slots[1];
            }
            else if (slot == InventorySlotId.CHESTPIECE)
            {
                return _slots[2];
            }
            else if (slot == InventorySlotId.GRIEVES)
            {
                return _slots[3];
            }
            else if (slot == InventorySlotId.VAMBRACES)
            {
                return _slots[4];
            }
            else if (slot == InventorySlotId.WEAPON)
            {
                return _slots[5];
            }
            else if (slot == InventorySlotId.POTION1)
            {
                return _slots[6];
            }
            else if (slot == InventorySlotId.POTION2)
            {
                return _slots[7];
            }
            else
            {
                return _slots[0];
            }
        }

        public Item Equip(InventorySlotId slot, Item item)
        {
            // FIXME: The below method is ignoring the slot that was passed in
            // FIXME: The below method is not returning the previous item
            //
            // Item prevItem = _slots[(int)slot];
            // _slots[(int)slot] = item;
            // return prevItem;

            if (item.Slot == InventorySlotId.HELMET)
            {
                slot = InventorySlotId.HELMET;
                return _slots[1] = item;
            }
            else if (item.Slot == InventorySlotId.CHESTPIECE)
            {
                slot = InventorySlotId.CHESTPIECE;
                return _slots[2] = item;
            }
            else if (item.Slot == InventorySlotId.GRIEVES)
            {
                slot = InventorySlotId.GRIEVES;
                return _slots[3] = item;
            }
            else if (item.Slot == InventorySlotId.VAMBRACES)
            {
                slot = InventorySlotId.VAMBRACES;
                return _slots[4] = item;
            }
            else if (item.Slot == InventorySlotId.WEAPON)
            {
                slot = InventorySlotId.WEAPON;
                return _slots[5] = item;
            }
            else if (item.Slot == InventorySlotId.POTION1)
            {
                slot = InventorySlotId.POTION1;
                return _slots[6] = item;
            }
            else if (item.Slot == InventorySlotId.POTION2)
            {
                slot = InventorySlotId.POTION2;
                return _slots[7] = item;
            }
            else
            {
                slot = InventorySlotId.UNEQUIPPABLE;
                return _slots[0] = item;
            }
        }

        public Item Unequip(InventorySlotId slot)
        {
            // FIXME: See chapter 2
            // FIXME: The below method is not returning the previous item
            //
            // Item prevItem = _slots[(int)slot];
            // _slots[(int)slot] = null;
            // return prevItem;

            if (slot == InventorySlotId.HELMET)
            {
                return _slots[1] = null;
            }
            else if (slot == InventorySlotId.CHESTPIECE)
            {
                return _slots[2] = null;
            }
            else if (slot == InventorySlotId.GRIEVES)
            {
                return _slots[3] = null;
            }
            else if (slot == InventorySlotId.VAMBRACES)
            {
                return _slots[4] = null;
            }
            else if (slot == InventorySlotId.WEAPON)
            {
                return _slots[5] = null;
            }
            else
            {
                return _slots[0] = null;
            }
        }

        public double CalcTotalWeight()
        {
            return 0;
        }


        public int CalcTotalAttackValue()
        {
            int sum = 0;
            for (int i = 0; i < _slots.Length; i++)
            {
                if (_slots[i] is IWeapon)
                {
                    IWeapon weapon = (IWeapon)_slots[i];
                    sum += weapon.AttackValue;
                }
            }
            return sum;
        }

        public int CalcTotalDefenseValue()
        {
            int sum = 0;
            for (int i = 0; i < _slots.Length; i++)
            {
                if (_slots[i] is IArmor)
                {
                    IArmor armor = (IArmor)_slots[i];
                    sum += armor.DefenseValue;
                }
            }
            return sum;
        }

    }
}
