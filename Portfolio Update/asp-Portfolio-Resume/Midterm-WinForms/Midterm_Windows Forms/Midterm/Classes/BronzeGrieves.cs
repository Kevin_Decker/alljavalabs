﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class BronzeGrieves : Item, IArmor
    {
        private int _defenseValue;
        private string _name;

        // FIXME: variables not needed
        private double _weight;
        private InventorySlotId _slot;
        private bool _isNatural;
        private Guid _id;

        public override bool IsNatural { get { return _isNatural; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _slot; } }

        public int DefenseValue { get { return _defenseValue; } }

        public BronzeGrieves(Random random)
        {
            _name = "Bronze Grieves";
            _weight = 3;
            _isNatural = false;
            _defenseValue = random.Next(0, 4);
            _slot = InventorySlotId.GRIEVES;
            _id = new Guid();
        }

        public override string ToString()
        {
            return string.Format("Bronze Grieves - {0}", _defenseValue);
        }
    }
}
