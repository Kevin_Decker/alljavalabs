﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class Character 
    {
        // instance fields
        protected StoredItems _bag;
        protected string _name;
        protected EquippedItems _equipped;
        protected int _currentHealth;
        protected bool _dead;

        // FIXME: add the dropped item to the character's bag instead
        // protected Item _droppedItem;

        protected int _attackValue;
        //protected bool _isNatural;
        protected int _defenseValue;

        //constructor
        public Character()
        {
            _bag = new StoredItems(20);
            _equipped = new EquippedItems();
            _currentHealth = 50;
            _dead = false;
        }

        // properties
        public string Name
        {
            get { return _name; }
        }


        public StoredItems Bag
        {
            get { return _bag; }
        }

        //public Item DroppedItem
        //{
        //    get { return _droppedItem; }
        //}

        //public int AttackValue
        //{
        //    get { return _attackValue; }
        //    set { _attackValue = value; }
        //}

        //public int DefenseValue
        //{
        //    get { return _defenseValue; }
        //    set { _defenseValue = value; }
        //}

        //public bool IsNatural
        //{
        //    get { return _isNatural; }
        //    set { _isNatural = value; }
        //}

        public EquippedItems Equipped
        {
            get { return _equipped; }
        }

        public int CurrentHealth
        {
            get { return _currentHealth; }
            
        }

        public bool IsDead
        {
            get { return _dead; }
        }

        //methods
        public double CalcTotalWeight()
        {
            return 0;
        }

        public int CalcTotalAttackValue()
        {
            return _attackValue + _equipped.CalcTotalAttackValue();
        }

        public int CalcTotalDefenseValue()
        {
            return _defenseValue + _equipped.CalcTotalDefenseValue();
        }

        public void TakeDamage(int damage)
        {
            _currentHealth -= damage;

            if (_currentHealth <= 0)
            {
                _dead = true;
                _currentHealth = 0;
            }
        }

        //public void Pickup(Item item)
        //{

        //}

        //public void UnequipAll()
        //{

        //}
    }
}
