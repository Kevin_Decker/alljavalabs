﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class WildDog : Character
    {
        Random _random = new Random();

        public WildDog (Random random, RandomItemGenerator itemFactory) : base()
        {
            IronHelmet ihelm = new IronHelmet(_random);
            BronzeSword bsword = new BronzeSword(_random);
            _name = "Wild Dog";
            _currentHealth = 25;
            _dead = false;

            Item item1 = itemFactory.GenerateRandomItem();
            Equipped.Equip(InventorySlotId.HELMET, ihelm);
            Equipped.Equip(InventorySlotId.WEAPON, bsword);
            Bag.AddItem(item1);
            Bag.AddItem(ihelm);
            Bag.AddItem(bsword);
        }

        public override string ToString()
        {
            return string.Format("Wild Dog");
        }
    }
}
