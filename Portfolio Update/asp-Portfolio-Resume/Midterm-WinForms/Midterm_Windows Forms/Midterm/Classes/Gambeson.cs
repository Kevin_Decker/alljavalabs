﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class Gambeson : Item, IArmor
    {
        private int _defenseValue;
        private string _name;
        private double _weight;
        private InventorySlotId _slot;
        private bool _isNatural;
        private Guid _id;

        public override bool IsNatural { get { return _isNatural; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _slot; } }

        public int DefenseValue{get { return _defenseValue; }}

        public Gambeson(Random random)
        {
            _name = "Gambeson";
            _weight = 5;
            _isNatural = false;
            _slot = InventorySlotId.CHESTPIECE;
            _id = new Guid();
            _defenseValue = random.Next(0, 4);
        }

        public override string ToString()
        {
            return string.Format("Gambeson - {0}", _defenseValue);
        }
    }
}
