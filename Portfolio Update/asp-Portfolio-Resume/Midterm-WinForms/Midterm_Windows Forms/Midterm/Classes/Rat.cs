﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class Rat : Character
    {
        Random _random = new Random();

        public Rat(Random random, RandomItemGenerator itemFactory) : base()
        {
            SmallPotion potion = new SmallPotion(random);   
            _name = "Rat";
            _currentHealth = 25;
            _dead = false;
            _attackValue = _random.Next(1, 4);
            _defenseValue = 0;

            Item item = itemFactory.GenerateRandomItem();
            Equipped.Equip(InventorySlotId.POTION1, potion);
            Bag.AddItem(item);
            Bag.AddItem(potion);   
        }

        public override string ToString()
        {
            return string.Format("Rat");
        }
    }
}
