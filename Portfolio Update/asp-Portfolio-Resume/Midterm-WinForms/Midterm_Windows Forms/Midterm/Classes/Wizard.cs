﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class Wizard : Character
    {
        Random _random = new Random();

        public Wizard(Random random, RandomItemGenerator itemFactory) : base()
        {
            IronSword isword = new IronSword(_random);
            SteelVambraces ssvamb = new SteelVambraces(_random);

            _name = "Wizard";
            _currentHealth = 25;
            _dead = false;

            Item item1 = itemFactory.GenerateRandomItem();
            Equipped.Equip(InventorySlotId.WEAPON, isword);
            Equipped.Equip(InventorySlotId.VAMBRACES, ssvamb);
            Bag.AddItem(item1);
            Bag.AddItem(ssvamb);
            Bag.AddItem(isword);
        }

        public override string ToString()
        {
            return string.Format("Wizard");
        }
    }
}
