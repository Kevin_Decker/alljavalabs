﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class SteelSword : Item, IWeapon
    {
        private int _attackValue;
        private string _name;
        private bool _isNatural;
        private InventorySlotId _slot;
        private double _weight;
        private Guid _id;

        public override double Weight { get { return _weight; } }
        public override string Name { get { return _name; } }
        public int AttackValue { get { return _attackValue; } }
        public override bool IsNatural { get { return _isNatural; } }
        public override InventorySlotId Slot { get { return _slot; } }

        public SteelSword(Random random)
        {
            _slot = InventorySlotId.WEAPON;
            _weight = 0;
            _id = new Guid();
            _isNatural = false;
            _attackValue = random.Next(12, 19);
        }

        public override string ToString()
        {
            return string.Format("Steel Sword - {0}", _attackValue);
        }
    }
}
