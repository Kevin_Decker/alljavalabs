﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class IronGrieves : Item, IArmor
    {
        private int _defenseValue;
        private string _name;
        private double _weight;
        private InventorySlotId _slot;
        private bool _isNatural;
        private Guid _id;

        public override bool IsNatural { get { return _isNatural; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _slot; } }

        public int DefenseValue { get { return _defenseValue; } }

        public IronGrieves(Random random)
        {
            _name = "Iron Grieves";
            _slot = InventorySlotId.GRIEVES;
            _id = new Guid();
            _isNatural = false;
            _weight = 0;
            _defenseValue = random.Next(2, 6);
        }

        public override string ToString()
        {
            return string.Format("Iron Grieves - {0}", _defenseValue);
        }
    }
}
