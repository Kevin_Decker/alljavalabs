﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class IronVambraces : Item, IArmor
    {
        private int _defenseValue;
        private string _name;
        private double _weight;
        private InventorySlotId _slot;
        private bool _isNatural;
        private Guid _id;

        public override bool IsNatural { get { return _isNatural; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _slot; } }

        public int DefenseValue { get { return _defenseValue; } }

        public IronVambraces(Random random)
        {
            _slot = InventorySlotId.VAMBRACES;
            _weight = 0;
            _isNatural = false;
            _id = new Guid();
            _defenseValue = random.Next(2, 6);
        }

        public override string ToString()
        {
            return string.Format("Iron Vambraces - {0}", _defenseValue);
        }
    }
}
