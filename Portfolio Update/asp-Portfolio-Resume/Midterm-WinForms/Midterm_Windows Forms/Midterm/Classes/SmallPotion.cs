﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class SmallPotion : Item, IPotion
    {
        private int _healValue;

        private string _name;
        private double _weight;
        private InventorySlotId _slot;
        private bool _isNatural;
        private Guid _id;

        public override bool IsNatural { get { return _isNatural; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _slot; } }

        public int HealValue {get { return _healValue; }}

        public SmallPotion(Random random)
        {
            _weight = 0;
            _slot = InventorySlotId.POTION1;
            _id = new Guid();
            _healValue = random.Next(10, 16);
        }

        public override string ToString()
        {
            return string.Format("Small Potion - HP {0}", _healValue);
        }
    }
}
