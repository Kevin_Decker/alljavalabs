﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class GameManager
    {
        // instance fields
        private Character _player;
        private Character _enemy;
        private RandomEnemyGenerator _randomEnemy;
        private RandomItemGenerator _item;
        private Random _rando;

        private int _depth;
        private bool _gameOver;
        private int _enemyDamage;
        private int _playerDamage;

        public GameManager()
        {
            _rando = new Random();
            _item = new RandomItemGenerator();
            _randomEnemy = new RandomEnemyGenerator();
            _player = new Hero();
            _player.Equipped.Equip(InventorySlotId.WEAPON, _item.StartWeapon());
            _player.Equipped.Equip(InventorySlotId.CHESTPIECE, _item.StartChest());
            //_player.Bag.AddItem(_item.StartChest());
            //_player.Bag.AddItem(_item.StartWeapon());
            _depth = 1;
            _gameOver = false;
        }

        // properties
        public RandomEnemyGenerator RandomEnemy {get { return _randomEnemy; }}
        public RandomItemGenerator Item {get { return _item; }}
        public int EnemyDamage {get { return _enemyDamage; } set { _enemyDamage = value; }}
        public int PlayerDamage {get { return _playerDamage; } set { _playerDamage = value; }}
        public Character Player {get { return _player; }}
        public Character Enemy {get { return _enemy; }}
        public int Depth{get { return _depth; }}
        public bool GameOver{get { return _gameOver; }}

        // methods
        private void ShowGameOver()
        {

        }

        public void Attack()
        {
            _enemyDamage = _enemy.CalcTotalAttackValue() - _player.CalcTotalDefenseValue();
            _playerDamage = _player.CalcTotalAttackValue() - _enemy.CalcTotalDefenseValue();

            // FIXME: this will potentially heal the enemy
            _enemy.TakeDamage(_playerDamage);

            // FIXME: if (_enemyDamage < 0)
            if (_enemy.CalcTotalAttackValue() <= _player.CalcTotalDefenseValue())
            {
                _enemyDamage = 0;
            }
            // FIXME: if (_playerDamage < 0)
            else if (_player.CalcTotalAttackValue() <= _enemy.CalcTotalDefenseValue())
            {
                _playerDamage = 0;
            }

            // FIXME: _enemy.CurrentHealth > 0
            if (_enemy.CurrentHealth != 0)
            {
                _player.TakeDamage(_enemyDamage);
            }
        }

        //public void DrinkPotion(Character drinker, IPotion potion)
        //{

        //}

        public void ManageInventory()
        {

        }

        public void NextBattle()
        {
            _enemy = _randomEnemy.GenerateRandomEnemy(_depth); 
        }

        public void LeaveDungeon()
        {

        }

        public void nextDepth()
        {
            _depth++;
        }

        public override string ToString()
        {
            return GetHashCode().ToString();
        }
    }
}
