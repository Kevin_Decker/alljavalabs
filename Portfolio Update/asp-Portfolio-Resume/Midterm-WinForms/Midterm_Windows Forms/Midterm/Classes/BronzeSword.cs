﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class BronzeSword : Item, IWeapon
    {

        private int _attackValue;
        private string _name;
        private bool _isNatural;
        private InventorySlotId _slot;
        private double _weight;
        private Guid _id;

        public override double Weight { get { return _weight; } }
        public override string Name { get { return _name; } }
        public int AttackValue{get { return _attackValue; }}
        public override bool IsNatural{get { return _isNatural; }}
        public override InventorySlotId Slot { get { return _slot; } }

        public BronzeSword(Random random) : base()
        {
            _name = "Bronze Sword";
            _isNatural = false;
            _slot = InventorySlotId.WEAPON;
            _attackValue = random.Next(4, 11);
            _id = new Guid();
        }

        public override string ToString()
        {
            return string.Format("Bronze Sword - {0}", _attackValue);
        }
    }
}
