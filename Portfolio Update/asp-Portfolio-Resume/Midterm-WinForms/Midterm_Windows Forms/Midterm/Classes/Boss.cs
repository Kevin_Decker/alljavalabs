﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class Boss : Character
    {
        Random _random = new Random();

        public Boss(Random random, RandomItemGenerator item) : base()
        {
            SteelSword newSteel = new SteelSword(random);
            SteelHelmet steelHelm = new SteelHelmet(random);
            SteelVambraces svamb = new SteelVambraces(_random);
            _name = "Mini Boss 1";
            _currentHealth = 25;
            _dead = false;

            Item items = item.GenerateRandomItem();
            Equipped.Equip(InventorySlotId.WEAPON, newSteel);
            Equipped.Equip(InventorySlotId.HELMET, steelHelm);
            Equipped.Equip(InventorySlotId.VAMBRACES, svamb);
            Bag.AddItem(newSteel);
            Bag.AddItem(steelHelm);
            Bag.AddItem(svamb);
            Bag.AddItem(items);
        }

        public override string ToString()
        {
            return string.Format("BOSS");
        }
    }
}
