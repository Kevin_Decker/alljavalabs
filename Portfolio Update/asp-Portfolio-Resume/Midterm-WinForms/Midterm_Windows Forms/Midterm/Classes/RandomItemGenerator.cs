﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class RandomItemGenerator
    {
        // instance field
        private Random _random = new Random();

        // property
        public Random Random {get { return _random; }}

        // method
        public Item GenerateRandomItem()
        {
            int random = _random.Next(1, 13);

            if (random <= 1)
            {
                BronzeSword bronze = new BronzeSword(_random);
                return bronze;
            }
            else if (random <= 2)
            {
                BronzeGrieves grieves = new BronzeGrieves(_random);
                return grieves;
            }
            else if (random <= 3)
            {
                Gambeson gamb = new Gambeson(_random);
                return gamb;
            }
            else if (random <= 4)
            {
                IronChestpiece iron = new IronChestpiece(_random);
                return iron;
            }
            else if (random <= 5)
            {
                IronGrieves igrieves = new IronGrieves(_random);
                return igrieves;
            }
            else if (random <= 6)
            {
                IronHelmet ihelmet = new IronHelmet(_random);
                return ihelmet;
            }
            else if (random <= 7)
            {
                IronSword isword = new IronSword(_random);
                return isword;
            }
            else if (random <= 8)
            {
                IronVambraces ivamb = new IronVambraces(_random);
                return ivamb;
            }
            else if (random <= 9)
            {
                SmallPotion smallPotion = new SmallPotion(_random);
                return smallPotion;
            }
            else if (random <= 10)
            {
                SteelHelmet shelmet = new SteelHelmet(_random);
                return shelmet;
            }
            else if (random <= 11)
            {
                SteelSword ssword = new SteelSword(_random);
                return ssword;
            }
            else if (random <= 12)
            {
                SteelVambraces svamb = new SteelVambraces(_random);
                return svamb;
            }
            else
            {
                return null;
            }
        }

        // starting equipment methods
        public BronzeSword StartWeapon()
        {
            BronzeSword sword = new BronzeSword(_random);
            return sword;
        }

        public IronChestpiece StartChest()
        {
            IronChestpiece ichest = new IronChestpiece(_random);
            return ichest;
        }
    }
}
