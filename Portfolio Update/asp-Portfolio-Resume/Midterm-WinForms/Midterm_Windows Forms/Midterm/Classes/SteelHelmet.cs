﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class SteelHelmet : Item, IArmor
    {
        private int _defenseValue;
        private string _name;
        private double _weight;
        private InventorySlotId _slot;
        private bool _isNatural;
        private Guid _id;

        public override bool IsNatural { get { return _isNatural; } }
        public override string Name { get { return _name; } }
        public override double Weight { get { return _weight; } }
        public override InventorySlotId Slot { get { return _slot; } }

        public int DefenseValue { get { return _defenseValue; } }

        public SteelHelmet(Random random)
        {
            _slot = InventorySlotId.HELMET;
            _isNatural = false;
            _weight = 0;
            _id = new Guid();
            _defenseValue = random.Next(4, 8);
        }

        public override string ToString()
        {
            return string.Format("Steel Helmet - {0}", _defenseValue);
        }
    }
}
