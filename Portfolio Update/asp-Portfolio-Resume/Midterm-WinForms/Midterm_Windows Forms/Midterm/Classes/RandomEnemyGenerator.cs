﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class RandomEnemyGenerator
    {
        // instance field
        private Random _random;
        private RandomItemGenerator _item;

        //properties
        public Random Random {get { return _random; }}
        public RandomItemGenerator Item {get { return _item; }}

        // constructor
        public RandomEnemyGenerator()
        {
            _random = new Random();
            _item = new RandomItemGenerator();
        }

        // method
        public Character GenerateRandomEnemy(int depth)
        {
            int randomSpawn = _random.Next(1, 5);
            if (depth % 5 == 0)
            {
                Boss miniBoss = new Boss(_random, _item);
                return miniBoss;
            }

            if (randomSpawn <= 2)
            {
                Rat rat1 = new Rat(_random, _item);
                return rat1;
            }
            else if (randomSpawn == 3)
            {
                WildDog dog1 = new WildDog(_random, _item);
                return dog1;
            }
            else
            {
                Wizard wizard1 = new Wizard(_random, _item);
                return wizard1;
            }
        }
    }
}
