﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    public class StoredItems //: Item
    {
        // instance fields
        private Item[] _items;
        private int _count;

        // FIXME: Capacity not implemented
        public int Capacity { get; }
        public int Count { get { return _count; } }

        //private string _name;
        //private double _weight;
        //private InventorySlotId _slot;
        //private bool _isNatural;

        // constructor
        public StoredItems(int size)
        {
            _items = new Item[size];
            _count = 0;
        }

        // property
        //public Item[] Items { get { return _items; } }
        //public override bool IsNatural { get { return _isNatural; } }
        //public override string Name { get { return _name; } }
        //public override double Weight { get { return _weight; } }
        //public override InventorySlotId Slot { get { return _slot; } }

        // methods
        public Item GetItem(int index)
        {
            return _items[index];
        }

        public Item SetItem(int index, Item item)
        {
            //_count = index;
            return _items[_count] = item;
        }

        public Item AddItem(Item item)
        {
            // FIXME: throw exception if _count >= _items.Length
            if (_count < 20)
            {
                _items[_count] = item;
                ++_count;
                
            }
            return item;
        }

        public Item RemoveItem(Item item)
        {
            // FIXME: this will not collapse the array to fill empty spots
            for (int i = 0; i < _items.Length; i++)
            {
                if (_items[i] == item)
                {
                    _items[i] = null;
                }
            }
            return item;
        }

        //public void CalcTotalWeight(double weight)
        //{

        //}
    }
}
