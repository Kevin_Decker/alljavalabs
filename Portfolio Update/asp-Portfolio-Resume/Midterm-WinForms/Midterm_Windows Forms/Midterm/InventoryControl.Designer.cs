﻿namespace Midterm
{
    partial class InventoryControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoryControl));
            this.fightBtn = new System.Windows.Forms.Button();
            this.backToMainBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.bagBox = new System.Windows.Forms.ListBox();
            this.lootBag = new System.Windows.Forms.ListBox();
            this.headLabel = new System.Windows.Forms.Label();
            this.weaponLabel = new System.Windows.Forms.Label();
            this.bodyLabel = new System.Windows.Forms.Label();
            this.armsLabel = new System.Windows.Forms.Label();
            this.potion1Label = new System.Windows.Forms.Label();
            this.legsLabel = new System.Windows.Forms.Label();
            this.potion2Label = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.bagCountLabel = new System.Windows.Forms.Label();
            this.addItemBtn = new System.Windows.Forms.Button();
            this.removeItemBtn = new System.Windows.Forms.Button();
            this.equiptBtn = new System.Windows.Forms.Button();
            this.unequipBtn = new System.Windows.Forms.Button();
            this.Drop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fightBtn
            // 
            this.fightBtn.Location = new System.Drawing.Point(696, 484);
            this.fightBtn.Name = "fightBtn";
            this.fightBtn.Size = new System.Drawing.Size(99, 41);
            this.fightBtn.TabIndex = 0;
            this.fightBtn.Text = "FIGHT!";
            this.fightBtn.UseVisualStyleBackColor = true;
            this.fightBtn.Click += new System.EventHandler(this.fightBtn_Click);
            // 
            // backToMainBtn
            // 
            this.backToMainBtn.Location = new System.Drawing.Point(576, 484);
            this.backToMainBtn.Name = "backToMainBtn";
            this.backToMainBtn.Size = new System.Drawing.Size(102, 41);
            this.backToMainBtn.TabIndex = 1;
            this.backToMainBtn.Text = "Back to Main";
            this.backToMainBtn.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(490, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Bag";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(490, 325);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 22);
            this.label2.TabIndex = 5;
            this.label2.Text = "Loot";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(187, 40);
            this.label3.TabIndex = 6;
            this.label3.Text = "Equipment";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(182, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Head";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Weapon";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(177, 297);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Body";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(342, 297);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Arms";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 372);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Potion 1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(182, 372);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Legs";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(330, 372);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 20);
            this.label10.TabIndex = 20;
            this.label10.Text = "Potion 2";
            // 
            // bagBox
            // 
            this.bagBox.FormattingEnabled = true;
            this.bagBox.ItemHeight = 16;
            this.bagBox.Location = new System.Drawing.Point(494, 126);
            this.bagBox.Name = "bagBox";
            this.bagBox.Size = new System.Drawing.Size(289, 132);
            this.bagBox.TabIndex = 21;
            this.bagBox.SelectedIndexChanged += new System.EventHandler(this.bagBox_SelectedIndexChanged);
            // 
            // lootBag
            // 
            this.lootBag.FormattingEnabled = true;
            this.lootBag.ItemHeight = 16;
            this.lootBag.Location = new System.Drawing.Point(494, 350);
            this.lootBag.Name = "lootBag";
            this.lootBag.Size = new System.Drawing.Size(289, 116);
            this.lootBag.TabIndex = 22;
            // 
            // headLabel
            // 
            this.headLabel.AutoSize = true;
            this.headLabel.Location = new System.Drawing.Point(173, 258);
            this.headLabel.Name = "headLabel";
            this.headLabel.Size = new System.Drawing.Size(47, 17);
            this.headLabel.TabIndex = 23;
            this.headLabel.Text = "Empty";
            // 
            // weaponLabel
            // 
            this.weaponLabel.AutoSize = true;
            this.weaponLabel.Location = new System.Drawing.Point(19, 321);
            this.weaponLabel.Name = "weaponLabel";
            this.weaponLabel.Size = new System.Drawing.Size(47, 17);
            this.weaponLabel.TabIndex = 24;
            this.weaponLabel.Text = "Empty";
            // 
            // bodyLabel
            // 
            this.bodyLabel.AutoSize = true;
            this.bodyLabel.Location = new System.Drawing.Point(168, 321);
            this.bodyLabel.Name = "bodyLabel";
            this.bodyLabel.Size = new System.Drawing.Size(47, 17);
            this.bodyLabel.TabIndex = 25;
            this.bodyLabel.Text = "Empty";
            // 
            // armsLabel
            // 
            this.armsLabel.AutoSize = true;
            this.armsLabel.Location = new System.Drawing.Point(331, 321);
            this.armsLabel.Name = "armsLabel";
            this.armsLabel.Size = new System.Drawing.Size(47, 17);
            this.armsLabel.TabIndex = 26;
            this.armsLabel.Text = "Empty";
            // 
            // potion1Label
            // 
            this.potion1Label.AutoSize = true;
            this.potion1Label.Location = new System.Drawing.Point(19, 401);
            this.potion1Label.Name = "potion1Label";
            this.potion1Label.Size = new System.Drawing.Size(47, 17);
            this.potion1Label.TabIndex = 27;
            this.potion1Label.Text = "Empty";
            // 
            // legsLabel
            // 
            this.legsLabel.AutoSize = true;
            this.legsLabel.Location = new System.Drawing.Point(168, 401);
            this.legsLabel.Name = "legsLabel";
            this.legsLabel.Size = new System.Drawing.Size(47, 17);
            this.legsLabel.TabIndex = 28;
            this.legsLabel.Text = "Empty";
            // 
            // potion2Label
            // 
            this.potion2Label.AutoSize = true;
            this.potion2Label.Location = new System.Drawing.Point(321, 401);
            this.potion2Label.Name = "potion2Label";
            this.potion2Label.Size = new System.Drawing.Size(47, 17);
            this.potion2Label.TabIndex = 29;
            this.potion2Label.Text = "Empty";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(719, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 17);
            this.label11.TabIndex = 30;
            this.label11.Text = "/ 20";
            // 
            // bagCountLabel
            // 
            this.bagCountLabel.AutoSize = true;
            this.bagCountLabel.Location = new System.Drawing.Point(697, 92);
            this.bagCountLabel.Name = "bagCountLabel";
            this.bagCountLabel.Size = new System.Drawing.Size(16, 17);
            this.bagCountLabel.TabIndex = 31;
            this.bagCountLabel.Text = "0";
            // 
            // addItemBtn
            // 
            this.addItemBtn.Location = new System.Drawing.Point(548, 287);
            this.addItemBtn.Name = "addItemBtn";
            this.addItemBtn.Size = new System.Drawing.Size(73, 38);
            this.addItemBtn.TabIndex = 32;
            this.addItemBtn.Text = "Add";
            this.addItemBtn.UseVisualStyleBackColor = true;
            this.addItemBtn.Click += new System.EventHandler(this.addItemBtn_Click);
            // 
            // removeItemBtn
            // 
            this.removeItemBtn.Location = new System.Drawing.Point(627, 287);
            this.removeItemBtn.Name = "removeItemBtn";
            this.removeItemBtn.Size = new System.Drawing.Size(77, 38);
            this.removeItemBtn.TabIndex = 33;
            this.removeItemBtn.Text = "Remove";
            this.removeItemBtn.UseVisualStyleBackColor = true;
            this.removeItemBtn.Click += new System.EventHandler(this.removeItemBtn_Click);
            // 
            // equiptBtn
            // 
            this.equiptBtn.Location = new System.Drawing.Point(381, 126);
            this.equiptBtn.Name = "equiptBtn";
            this.equiptBtn.Size = new System.Drawing.Size(81, 37);
            this.equiptBtn.TabIndex = 34;
            this.equiptBtn.Text = "Equip";
            this.equiptBtn.UseVisualStyleBackColor = true;
            this.equiptBtn.Click += new System.EventHandler(this.equiptBtn_Click);
            // 
            // unequipBtn
            // 
            this.unequipBtn.Location = new System.Drawing.Point(381, 169);
            this.unequipBtn.Name = "unequipBtn";
            this.unequipBtn.Size = new System.Drawing.Size(81, 34);
            this.unequipBtn.TabIndex = 35;
            this.unequipBtn.Text = "Unequip";
            this.unequipBtn.UseVisualStyleBackColor = true;
            this.unequipBtn.Click += new System.EventHandler(this.unequipBtn_Click);
            // 
            // Drop
            // 
            this.Drop.Location = new System.Drawing.Point(710, 286);
            this.Drop.Name = "Drop";
            this.Drop.Size = new System.Drawing.Size(87, 39);
            this.Drop.TabIndex = 36;
            this.Drop.Text = "Drop";
            this.Drop.UseVisualStyleBackColor = true;
            this.Drop.Click += new System.EventHandler(this.Drop_Click);
            // 
            // InventoryControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.Controls.Add(this.Drop);
            this.Controls.Add(this.unequipBtn);
            this.Controls.Add(this.equiptBtn);
            this.Controls.Add(this.removeItemBtn);
            this.Controls.Add(this.addItemBtn);
            this.Controls.Add(this.bagCountLabel);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.potion2Label);
            this.Controls.Add(this.legsLabel);
            this.Controls.Add(this.potion1Label);
            this.Controls.Add(this.armsLabel);
            this.Controls.Add(this.bodyLabel);
            this.Controls.Add(this.weaponLabel);
            this.Controls.Add(this.headLabel);
            this.Controls.Add(this.lootBag);
            this.Controls.Add(this.bagBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backToMainBtn);
            this.Controls.Add(this.fightBtn);
            this.Name = "InventoryControl";
            this.Size = new System.Drawing.Size(854, 617);
            this.Load += new System.EventHandler(this.InventoryControl_Load);
            this.VisibleChanged += new System.EventHandler(this.InventoryControl_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button fightBtn;
        private System.Windows.Forms.Button backToMainBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ListBox bagBox;
        private System.Windows.Forms.ListBox lootBag;
        private System.Windows.Forms.Label headLabel;
        private System.Windows.Forms.Label weaponLabel;
        private System.Windows.Forms.Label bodyLabel;
        private System.Windows.Forms.Label armsLabel;
        private System.Windows.Forms.Label potion1Label;
        private System.Windows.Forms.Label legsLabel;
        private System.Windows.Forms.Label potion2Label;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label bagCountLabel;
        private System.Windows.Forms.Button addItemBtn;
        private System.Windows.Forms.Button removeItemBtn;
        private System.Windows.Forms.Button equiptBtn;
        private System.Windows.Forms.Button unequipBtn;
        private System.Windows.Forms.Button Drop;
    }
}
