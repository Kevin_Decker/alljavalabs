﻿goo<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Kevin Decker - Portfolio</title>
    <!--GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--BOOTSTRAP MAIN STYLES -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!--FONTAWESOME MAIN STYLE -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--CUSTOM STYLE -->
    <link href="assets/css/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
     <!-- NAV SECTION -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Kevin's Portfolio</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#header-section">HOME</a></li>
                    <li><a href="#about-section">ABOUT</a></li>
                    <li><a href="#resume-section">RESUME</a></li>
                    <li><a href="#products-section">FREELANCE WORK</a></li>
                    <li><a href="#school-section">SCHOOL WORK</a></li>
                    <li><a href="Java.aspx">JAVA LABS</a></li>
                    <li><a href="#contact-section">CONTACT</a></li>
                </ul>
            </div>
           
        </div>
    </div>
     <!--END NAV SECTION -->
     <!-- HEADER SECTION -->
    <div id="header-section">
        <div class="container">
            <div class="row centered">
                <div class="col-md-8 col-md-offset-2">
                    <h1><strong> Kevin Decker
                        </strong>          
                     
                    </h1>
                     <br /> <br /> <br />
                    <h2> Web Developer</h2>
                     <br />
                  <a href="#about-section">  <i class="fa fa-angle-double-down fa-5x down-icon"></i> </a>
                </div>
            </div>
           
        </div>
       
    </div> 
      <!--END HEADER SECTION -->
    <!--ABOUT SECTION -->
    <div id="about-section" >
        <div class="container" >
            <div class="row main-top-margin text-center">
                <div class="col-md-8 col-md-offset-2 " data-scrollreveal="enter top and move 100px, wait 0.3s">
                    <h1>Who Am I ?</h1>
                    <h4>
                       I will be graduating from Ranken Technical College in May, 2018 with an associate's degree in Information Technology, specifically web technologies. 
                    </h4>
                </div>
            </div>
            <!-- ./ Main Heading-->
            <div class="row main-low-margin text-center">
                <div class="col-md-3 " data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <img id="about-img" class="img-circle" src="assets/img/me.jpg" alt="" />
                    <h4><strong>Kevin Decker</strong> </h4>
                    <p>
                        <a href="https://twitter.com/kevindecker621"><i class="fa fa-twitter-square fa-2x color-twitter"></i></a>
                        <a href="https://plus.google.com/u/0/112177846157605545936"><i class="fa fa-google-plus-square fa-2x color-google-plus"></i></a>
                        <a href="https://www.linkedin.com/in/kevin-decker-76289771/"><i class="fa fa-linkedin-square fa-2x color-linkedin"></i></a>
                    </p>

                </div>
             
                <div class="col-md-7 col-sm-7 col-md-offset-1  text-justify" data-scrollreveal="enter right and move 100px, wait 0.4s">
                    <h3> Background Information</h3>
                    <p>
                         Before Ranken, I attended Southern Illinois University of Edwardsville and received a Bachelor's in Mass Communications. 
                        I was in the TV/Radio industry for about 3 years. 
                        After a brief stint in insurance I decided to pursure my ultimate passion in programming and head back to school.
                    </p>

                    <p>
                         At Ranken, the languages of focus were C#, Java, ASP.NET, JavaScript, HTML, CSS with exposure to several others, 
                        which include jQuery, MVC, SQL, LINQ, and Bootstrap. 
                        Below in the Freelane and School Work sections you can get glimpse of some projects I have completed.
                    </p>

                </div>
            </div>
              <!-- ./ Row Content-->
        </div>
    </div>
    <!-- END ABOUT SECTION -->

 <!--RESUME SECTION -->
    <div id="resume-section" >
        <div class="container" >
            <div class="row main-top-margin text-center">
                <div class="col-md-8 col-md-offset-2 " data-scrollreveal="enter top and move 100px, wait 0.3s">
                    <h1>Resume</h1>
                    <a href="RESUME.docx" style="text-align:center;margin-right:130px;">Download Resume</a>
                    <h3 class="resume-experience" style="margin-left:-140px;text-decoration:underline;"><strong>Professional Experience</strong></h3>
                </div>
            </div>
            <!-- ./ Main Heading-->
            <div id="resume-main-heading" class="row main-low-margin text-center">
                
                <div class="col-md-12">
                    <div class="col-md-4 exp-bg" data-scrollreveal="enter left and move 100px, wait 0.4s">
                        <h4><strong>Ranken Technical College</strong> </h4>
                        <h5 class="mTopSub10"><strong>Web Developer - December 2017 - PRESENT</strong></h5>
                        <ul class="list-group">
                            <li class="list-group-item">Maintain Learn2Earn web application for Ranken Technical College</li>
                            <li class="list-group-item">Update application per requests, including front-end changes as well as back-end </li>
                            <li class="list-group-item">Create documentation for common tasks such as creating new admin logins</li>
                        </ul>
                        <img class="img-circle ranken-logo" src="assets/img/ranken-logo.png" alt="" />
                    </div>

                    <div class="col-md-4 exp-bg" data-scrollreveal="enter left and move 100px, wait 0.4s">
                        <h4><strong>Ranken Technical College - Bookstore</strong> </h4>
                        <h5 class="mTopSub10"><strong>Cashier/Stocker - August 2016 - PRESENT</strong></h5>
                        <ul class="list-group">
                            <li class="list-group-item">Run the register</li>
                            <li class="list-group-item">Stocked the shelves </li>
                            <li class="list-group-item">Built toolkits for future students</li>
                        </ul>
                        <img id="rankenLogo2" class="img-circle ranken-logo" src="assets/img/ranken-logo.png" alt="" />
                    </div>

                    <div class="col-md-4 exp-bg" data-scrollreveal="enter left and move 100px, wait 0.4s">
                        <h4><strong>H&L Senior Benefits, Inc.</strong> </h4>
                        <h5 class="mTopSub10"><strong>Insurance Agent - February 2016 - August 2016</strong></h5>
                        <ul class="list-group">
                            <li class="list-group-item">Maintained aspects of an independent insurance agent</li>
                            <li class="list-group-item">Educated people on the different Medicare supplement products </li>
                            <li class="list-group-item">Sold Medicare supplement products</li>
                        </ul>
                        <img id="" class="img-circle ranken-logo" src="assets/img/me.jpg" alt="" />
                    </div>
                </div>


                <div class="col-md-12" style="margin-top:10px;">
                    <div class="col-md-4 exp-bg" data-scrollreveal="enter left and move 100px, wait 0.4s">
                        <h4><strong>KSDK-TV</strong> </h4>
                        <h5 class="mTopSub10"><strong>Production Assistant - March 2015 - January 2016</strong></h5>
                        <ul class="list-group">
                            <li class="list-group-item">Acted as floor director and web producer for “Show Me St. Louis”</li>
                            <li class="list-group-item">Worked with reporters and assignment editors for news stories </li>
                            <li class="list-group-item">Ran the teleprompter</li>
                        </ul>
                        <img id="" class="img-circle ranken-logo" src="assets/img/ksdk-logo.jpg" alt="" />
                    </div>

                    <div class="col-md-4 exp-bg" data-scrollreveal="enter left and move 100px, wait 0.4s">
                        <h4><strong>KTVI Fox2</strong> </h4>
                        <h5 class="mTopSub10"><strong>Intern Web Producer - March 2015 - August 2015</strong></h5>
                        <ul class="list-group">
                            <li class="list-group-item">Wrote web stories for station’s website</li>
                            <li class="list-group-item">Ran Instagram account for the 9am segment </li>
                            <li class="list-group-item">Shadowed many departments to increase knowledge</li>
                        </ul>
                        <img id="" class="img-circle ranken-logo" src="assets/img/ktvi-logo.jpg" alt="" />
                    </div>

                    <div class="col-md-4 exp-bg" data-scrollreveal="enter left and move 100px, wait 0.4s">
                        <h4><strong>WSIE 88.7FM</strong> </h4>
                        <h5 class="mTopSub10"><strong>Newscaster/Board Op - March 2013 - August 2015</strong></h5>
                        <ul class="list-group">
                            <li class="list-group-item">Wrote and gathered news stories</li>
                            <li class="list-group-item">On-air personality for various morning and afternoon shifts </li>
                            <li class="list-group-item">Ran audio board for SIUE sports events</li>
                        </ul>
                        <img id="" class="img-circle ranken-logo" src="assets/img/wsie-logo.png" alt="" />
                    </div>
                </div>

                <div class="col-md-8 col-md-offset-2">
                    <h3 class="resume-experience" style="text-decoration:underline;margin-left:-140px;"><strong>Summary of Qualifications</strong></h3>
                </div>
                <h4 style="clear:both;text-align:left;">Programming Languages</h4>
                    <dl class="dl-horizontal">
                        <dt>Most Exposure</dt>
                        <dd class="ddFloatLeft">HTML, CSS, Bootstrap, C#</dd>
                        <dt>Some Exposure</dt>
                        <dd class="ddFloatLeft">Java, ASP.NET, ASP.NET MVC</dd>
                        <dt>Minimal Exposure</dt>
                        <dd class="ddFloatLeft">JavaScript, jQuery, SQL, LINQ </dd>
                    </dl>
                <h4 class="ddFloatLeft">Software</h4>
                    <p class="ddFloatLeft">Visual Studio, Eclipse, VMWare, Photoshop, Dreamweaver, Flash, WordPress, Version Control (Bitbucket, GitHub), Various code editing software</p>

                <div class="col-md-8 col-md-offset-2">
                    <h3 class="resume-experience" style="text-decoration:underline;margin-left:-140px;">Education & Certifications</h3>
                </div>
                <p class="resumePar" style="clear: both;"><strong>Ranken Technical College - </strong>Associates Degree, Information Technology, May 2018. Emphasis in web technology and programming. GPA 3.94.</p>
                <p class="resumePar"><strong>Southern Illinois University Edwardsville - </strong>Bachelor of Arts, Mass Communications, 2014. Emphasis in corporate communications.</p>
                <p class="resumePar" style="margin-bottom:10px;"><strong>Certification - </strong>Windows 7 Certification, 70-680.</p>

            </div>
              <!-- ./ Row Content-->
        </div>
    </div>
    <!-- END ABOUT SECTION -->
   
   
    <!--FREELANCE WORK SECTION -->
    <div id="products-section" >
         <div class="container" >
            <div class="row main-top-margin text-center" data-scrollreveal="enter top and move 100px, wait 0.3s">
                <div class="col-md-8 col-md-offset-2 ">
                    <h1>Freelance Projects</h1>
                    <h4>
                       The website featured was completed during my 2nd semester at Ranken, during my HTML/CSS/JavaScript class.
                    </h4>
                </div>
            </div>
             <!-- ./ Main Heading-->
             <hr />
            <div class="row main-low-margin" >
                <div class="col-md-10  col-md-offset-1 ">
                     <div class="col-md-4 col-sm-4" data-scrollreveal="enter left and move 100px, wait 0.8s">
                    <img id="freelance-img" src="assets/img/background.png" alt="" />
                </div>
                <div class="col-md-8 " data-scrollreveal="enter right and move 100px, wait 0.4s">
                    <h4 >  <strong class="color-red">Client: Osterhage's 1 Stop Handyman Shop</strong></h4>
                    <p>
                        This is a small local handyman shop out of Waterloo, Illinois.
                    </p>
                    <p>
                        The process was seamless. Met with the owner to discuss design, layout and content.
                        The project was completed at no cost, and the owner was very pleased with the outcome.
                    </p>
<%--                    <p>
                        <i class="fa fa-check"></i> Responsive Design<br />
                     <i class="fa fa-check"></i> 24x7 Support <br />
                       <i class="fa fa-check"></i> Well Documented<br />
                    </p>--%>
                    <a href="http://www.osterhages1stop.com/" class="btn btn-success" >Visit Site</a>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!--END FREELANCE WORK SECTION -->

    <!--SCHOOL WORK SECTION -->
    <div id="school-section" >
         <div class="container" >
            <div class="row main-top-margin text-center" data-scrollreveal="enter top and move 100px, wait 0.3s">
                <div class="col-md-8 col-md-offset-2 ">
                    <h1>School Work</h1>
                    <h4>
                       Below are several examples of my school work. They range from everyday labs to final projects.
                    </h4>
                </div>
            </div>
             <!-- ./ Main Heading-->
             <hr />
            <div class="row main-low-margin" >
                <div class="col-md-12  col-md-offset-1 ">
                <div class="col-md-5 " data-scrollreveal="enter right and move 100px, wait 0.4s">
                    <h4 >  <strong class="color-red">C# Final Project</strong></h4>
                    <p>
                        The final project was a Roguelike game. This is the largest scale project I completed at Ranken.
                    </p>
                    <p>
                        The game has a battle system where you face opponents at random (3 possible enemies). 
                        When you reach the 5th round of enemies, you'll face a "boss" with increased statistics. 
                        Also, there is an inventory system that allows you to equip and unequip various items between rounds. 
                        There are several tiers of weapons for offensive damage, as well as armor for defense. 
                        These are mostly dropped randomly, however, if you defeat a boss, you will receive better weapons and armor.
                        *Note: There is no end game at the moment. There are still other various bugs.
                    </p>
                    <a href="midterm.exe" class="btn btn-success" >Download Executable</a>
                    <br/><br/><a href="uml_and_cs_code.zip" class="btn btn-success">Download Project Files</a>
                </div>
                <div class="col-md-5 " data-scrollreveal="enter right and move 100px, wait 0.4s">
                    <h4 >  <strong class="color-red">C# Hands-On-Test</strong></h4>
                    <p>
                        This test was a scenario over aggregation and exceptions.
                    </p>
                    <p>
                        This "ice cream shop" has specific validation which is programmed through excpetions. 
                        All validations must be met to have a valid order and be added to the "receipt" on the right-hand side.
                        Your total will be displayed in the lower right hand corner. 
                        There's also a "Print Receipt" button that will write the contents of the textbox/receipt to a .txt file.
                        The .txt file is saved in the same folder as the executable. 
                        Run the executable in the zipped file after downloading project files, in order for the receipt functionality to work.
                    </p>
                    <a href="project.exe" class="btn btn-success" >Download Executable</a>
                    <br/><br/><a href="cs_test.zip" class="btn btn-success">Download Project Files</a>
                </div>
                </div>
                <div class="col-md-12 col-md-offset-1">
                    <div class="col-md-5 " data-scrollreveal="enter right and move 100px, wait 0.4s">
                    <h4 >  <strong class="color-red">Java Text Editor (Side Project)</strong></h4>
                    <p>
                        This was not for any grade during Ranken. I completed this in my own time.
                    </p>
                    <p>
                        This Text Editor program was coded in Java GUI using the JavaFX library. 
                        It's very similar to Notepad, as its functions are on a small scale, unlike Microsoft Word.
                        The program opens with a blank text document (.txt). 
                        In the first menu item named "File" you can see that the user is able to create a new document, 
                        save one they are currently working on, open an existing text file, and close the application.
                        To customize the text, the user can modify the style, alignment, font family, and font sizes.
                    </p>
                    <a href="te.jar" class="btn btn-success" >Download Executable</a>
                    <br/><br/><a href="texteditor.zip" class="btn btn-success">Download Project Files</a>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!--END WORK/PRODUCTS SECTION -->

    <!--CONTACT SECTION -->
    <div id="contact-section">
        <div class="container" >
            <div class="row main-top-margin text-center">
                <div class="col-md-8 col-md-offset-2 " data-scrollreveal="enter top and move 100px, wait 0.3s">
                    <h1>Contact Me</h1>
                </div>
            </div>
            <!-- ./ Main Heading-->
            <div class="row">
                <div class="col-md-12  col-sm-12 ">
                    <div class="col-md-6  " data-scrollreveal="enter left and move 100px, wait 0.4s">
                        <h3> How to Contact Me:</h3>
                        <hr />
                        <p>
                          Email: kdecker621@gmail.com<br />
                          Call: 618-444-9381<br />
                          LinkedIn: <a href="https://www.linkedin.com/in/kevin-decker-76289771/">LinkedIn</a>  
                            </p>
                       
                    </div>
                    <div class="col-md-6  " data-scrollreveal="enter right and move 100px, wait 0.4s">
                        <h3>Drop a Line or Hire Me </h3>
                        <hr />
                        <form>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required="required" placeholder="Name"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required="required" placeholder="Email address"/>
                                    </div>
                                </div>
                            </div>
                              <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <input type="text" class="form-control" required="required" placeholder="Subject"/>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <input type="text" class="form-control"  placeholder="Ref. (IF any)"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <textarea name="message" id="message" required="required" class="form-control" rows="3"  placeholder="Message"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" onclick="SendEmail()" class="btn btn-primary">Submit Request</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
             <!-- ./ Row Content-->
        </div>
    </div>  
    <!--END CONTACT SECTION --> 
    <!--FOOTER SECTION -->
    <div id="footer">
        <div class="container">
            <div class="row ">
                &copy; 2014 www.yourdomain.com | All Right Reserved 				
		
            </div>
            
        </div>
       
    </div>  
    <!--END FOOTER SECTION --> 
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY LIBRARY -->
    <script src="assets/js/jquery.js"></script>
    <!-- CORE BOOTSTRAP LIBRARY -->
    <script src="assets/js/bootstrap.min.js"></script>
     <!-- SCROLL REVEL LIBRARY FOR SCROLLING ANIMATIONS-->
    <script src="assets/js/scrollReveal.js"></script>
       <!-- CUSTOM SCRIPT-->
    <script src="assets/js/custom.js"></script>
    </form>
</body>
</html>
