﻿goo<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Kevin Decker - Portfolio</title>
    <!--GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--BOOTSTRAP MAIN STYLES -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!--FONTAWESOME MAIN STYLE -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--CUSTOM STYLE -->
    <link href="assets/css/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
     <!-- NAV SECTION -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Kevin's Portfolio</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="Default.aspx">BACK TO PORTFOLIO</a></li>
                    <li><a href="Java.aspx">JAVA LABS</a></li>
                </ul>
            </div>
        </div>
    </div>
     <!--END NAV SECTION -->

    <!-- TITLE -->
        <h2 id="javaHeader" style="text-align:center;">JAVA LABS</h2>
    <!-- END TITLE -->

    <!-- 1ST SECTION (6 ITEMS) -->
        <h2>Basic Java Concepts</h2>
        <div class="col-md-12" style="background-color:white;text-align:center;">
            <hr />
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Import Statement</h4>
                    <img class="" src="assets/img/java/import.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Instance Fields</h4>
                    <img class="" src="assets/img/java/instance_fields.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Instance Methods</h4>
                    <img class="" src="assets/img/java/instance_methods.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Constructors</h4>
                    <img class="" src="assets/img/java/constructor.png" alt="" />
                </div>

            <hr />
                
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>If Statement</h4>
                    <img class="" src="assets/img/java/ifstatement.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>If-Else Statement</h4>
                    <img class="" src="assets/img/java/ifelse.png" alt="" width="400" height="129"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>If-Else-If Statement</h4>
                    <img class="" src="assets/img/java/ifelseif.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Switch Statement</h4>
                    <img class="" src="assets/img/java/switch.png" alt="" width="300" height="295" />
                </div>

            <hr />

                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Comparing Strings</h4>
                    <img class="" src="assets/img/java/comparestrings.png" alt=""  width="316" height="118"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>DecimalFormat</h4>
                    <img class="" src="assets/img/java/dformat.png" alt="" width="380" height="60"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>While Loop</h4>
                    <img class="" src="assets/img/java/while.png" alt="" width="312" height="85"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Do-While Loop</h4>
                    <img class="" src="assets/img/java/dowhile.png" alt="" />
                </div>

            <hr />

                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>For Loop</h4>
                    <img class="" src="assets/img/java/forloop.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Running Total</h4>
                    <img class="" src="assets/img/java/runningtotal.png" alt="" width="348" height="225" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Sentinel Value</h4>
                    <img class="" src="assets/img/java/sentinel.png" alt="" width="244" height="221" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Overloaded Methods</h4>
                    <img class="" src="assets/img/java/overmethods.png" alt="" width="350" height="240"/>
                </div>

           <hr />

                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Overloaded Constructors</h4>
                    <img class="" src="assets/img/java/overloadedconstructor.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Static Class Members</h4>
                    <img class="" src="assets/img/java/staticmembers.png" alt="" width="348" height="225" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Aggregation</h4>
                    <img class="" src="assets/img/java/aggregation.png" alt="" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>This Reference Variable</h4>
                    <img class="" src="assets/img/java/this.png" alt="" width="347" height="85"/>
                </div>

           <hr /><br />

            <div class="col-md-12">
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Overriding toString</h4>
                    <img class="" src="assets/img/java/tostring.png" alt="" width="360" height="55"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Overriding Equals</h4>
                    <img class="" src="assets/img/java/equals.png" alt="" width="348" height="225" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Arrays</h4>
                    <img class="" src="assets/img/java/arrays.png" alt="" width="350" height="160" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>ArrayList</h4>
                    <img class="" src="assets/img/java/arraylist.png" alt="" width="350" height="110"/>
                </div>
            </div>

          <hr />

            <div class="col-md-12">
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Sequential Search</h4>
                    <img class="" src="assets/img/java/sequential.png" alt="" width="360" height="210"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Selection Sort</h4>
                    <img class="" src="assets/img/java/sort.png" alt="" width="360" height="240"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Extending Parent Class</h4>
                    <img class="" src="assets/img/java/extend.png" alt="" width="350" height="160" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Polymorphism</h4>
                    <img class="" src="assets/img/java/poly.png" alt="" width="370" height="80"/>
                </div>
            </div>

         <hr />

            <div class="col-md-12">
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Interfaces</h4>
                    <img class="" src="assets/img/java/interface.png" alt="" width="360" height="115"/>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Handling Exceptions</h4>
                    <img class="" src="assets/img/java/handle.png" alt="" width="360" height="190" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>Throwing Exceptions</h4>
                    <img class="" src="assets/img/java/throw.png" alt="" width="360" height="150" />
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>GUI Example 1</h4>
                    <img class="" src="assets/img/java/gui1.png" alt="" width="370" height="140"/>
                    <a href="tiptaxtotal.jar">Download JAR</a>
                </div>
            </div>

        <hr />

            <div class="col-md-12">
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>GUI Example 2</h4>
                    <img class="" src="assets/img/java/gui2.png" alt="" width="360" height="240"/>
                    <a href="joesauto.jar">Download JAR</a>
                </div>
                <div class="col-md-3" data-scrollreveal="enter left and move 100px, wait 0.4s">
                    <h4><strong></strong>GUI Example 3</h4>
                    <img class="" src="assets/img/java/gui3.png" alt="" width="500" height="250"/>
                    <a href="slot_machine.zip">Download JAR</a>
                </div>
            </div>



        </div>

        <hr />

    <!-- END 1ST SECTION -->

    <!--FOOTER SECTION -->
    <div id="footer">
        <div class="container">
            <div class="row ">
                &copy; 2014 www.yourdomain.com | All Right Reserved 				
		
            </div>
            
        </div>
       
    </div>  
    <!--END FOOTER SECTION --> 
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY LIBRARY -->
    <script src="assets/js/jquery.js"></script>
    <!-- CORE BOOTSTRAP LIBRARY -->
    <script src="assets/js/bootstrap.min.js"></script>
     <!-- SCROLL REVEL LIBRARY FOR SCROLLING ANIMATIONS-->
    <script src="assets/js/scrollReveal.js"></script>
       <!-- CUSTOM SCRIPT-->
    <script src="assets/js/custom.js"></script>
    </form>
</body>
</html>
