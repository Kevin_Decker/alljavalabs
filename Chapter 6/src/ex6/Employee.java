package ex6;

public class Employee {
	
	// instance fields
	private String name;
	private int idNumber;
	private String department;
	private String position;
	
	//constructors
	public Employee()
	{
		name = "";
		department = "";
		position = "";
		idNumber = 0;
	}
	
	public Employee(String n, int id, String d, String p)
	{
		name = n;
		idNumber = id;
		department = d;
		position = p;
	}
	
	public Employee(String n, int id)
	{
		name = n;
		idNumber = id;
		department = "";
		position = "";
	}
	
	// getter & setter for name
	public void setName(String str) 
	{
		name = str;
	}
	
	// mutator - method to assign a value to the instance field
	public String getName() 
	{
		return name;
	}
	
	// getter & setter for ID
	public void setID(int idNum) 
	{
		idNumber = idNum;
	}
	
	public int getID() 
	{
		return idNumber;
	}
	
	// getter and setter for department
	public void setDept(String dept) 
	{
		department = dept;
	}
	
	public String getDept() 
	{
		return department;
	}
	
	// getter & setter for position
	public void setPos(String posi) 
	{
		position = posi;
	}
	
	public String getPos() 
	{
		return position;
	}
	
	
	
	
	
	
	
	
	
	
	
}
