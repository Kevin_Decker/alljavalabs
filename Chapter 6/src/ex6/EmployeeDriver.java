package ex6;

public class EmployeeDriver {

	public static void main(String[] args) {

		Employee e1 = new Employee();
		Employee e2 = new Employee("Kevin",1234,"IT","Prez");
		Employee e3 = new Employee("Kevin",4321);
		
		System.out.println(e1.getName());
		System.out.println(e1.getID());
		System.out.println(e1.getDept());
		System.out.println(e1.getPos());
		System.out.println(e1);
		
		System.out.println(e2.getName());
		System.out.println(e2.getID());
		System.out.println(e2.getDept());
		System.out.println(e2.getPos());
		System.out.println();
		
		System.out.println(e3.getName());
		System.out.println(e3.getID());
		System.out.println(e3.getDept());
		System.out.println(e3.getPos());
		System.out.println();
		
	}

}
