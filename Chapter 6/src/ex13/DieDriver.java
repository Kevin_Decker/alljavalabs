package ex13;

public class DieDriver {

	public static void main(String[] args) {
		
		Player player = new Player("Player 1", 50);
		Player computer = new Player("Computer", 50);
		Die die1 = new Die(6);
		Die die2 = new Die(6);
		int playerPoints = player.getCurrentPoints();
		int compPoints = computer.getCurrentPoints();
		
		for(int x = 0; x < 100; ++x)
		{
			int roll1 = die1.Roll();
			
			System.out.println("Player 1 Rolls: " + roll1);
			if(playerPoints - roll1 < 0)
			{
				playerPoints += roll1;
			}
			else 
			{
				playerPoints -= roll1;
			}
			System.out.println(playerPoints);
			
			int roll2 = die2.Roll(); 
			
			System.out.println("Computer Rolls: " + roll2);
			if(compPoints - roll2 < 0)
			{
				compPoints += roll2;
			}
			else
			{
				compPoints -= roll2;
			}
			System.out.println(compPoints);
			System.out.println();
			
			if(playerPoints == 1)// || playerPoints < compPoints)
			{
				System.out.println("Player wins! with a score of " + playerPoints);
				break;
			}
			if(compPoints == 1)// || compPoints < playerPoints)
			{
				System.out.println("Computer wins! with a score of " + compPoints);
				break;
			}
		}
	}
}
