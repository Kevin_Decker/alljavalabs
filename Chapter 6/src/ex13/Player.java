package ex13;

public class Player {
	
	private int currentPoints;
	private String name;
	//private Die die;
	
	//constructor
	public Player(String n, int p)
	{
		currentPoints = p;
		name = n;
	}

	//Getters and Setters
	public int getCurrentPoints() {
		return currentPoints;
	}
	public void setCurrentPoints(int currentPoints) {
		this.currentPoints = currentPoints;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
