package ex4;

import java.util.Scanner;

public class LandDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		double length=0,width=0;
		LandTract[] lands = new LandTract[4];
		for(int x = 0; x < lands.length; ++x)
		{
			System.out.println("Enter length for land " + (x+1));
			length = keyboard.nextDouble();
			System.out.println("Enter width for land " + (x+1));
			width = keyboard.nextDouble();
			
			lands[x] = new LandTract(length, width);
			
			System.out.println(lands[x]);
			//System.out.println("The area of land is " + lands[x].calcArea());
		}
		
		for(int i=0;i<lands.length;++i)
		{
			for(int j=1;j<lands.length;++j)
			{
				if(i != j)
				{
					if(lands[i].equals(lands[j]))
					{
						System.out.printf("Two lands %d and %d are equal.",i+1,j+1);
					}
				}
			}
			/*System.out.printf("Is land %d equal to land %d?", i+1, i);
			if(lands[i].equals(lands[i-1]))
			{
				System.out.println("Yup");
			}
			else
			{
				System.out.println("nope");
			}*/
		}

		

	}

}
