package ex4;

public class LandTract {

	private double length;
	private double width;
	
	//constructor
	public LandTract(double length, double width) {
		super();
		this.length = length;
		this.width = width;
	}

	//Mutators and Accessors
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	
	//method that returns area
	public double calcArea()
	{
		return length * width;
	}

	public boolean equals(LandTract land)
	{
		boolean status;
		if(this.calcArea() == (land.calcArea()))
		{
			status = true;
		}
		else
		{
			status = false;
		}
		return status;
	}
	
	public String toString()
	{
		return String.format("The area of land is " + calcArea());
	}
	
	
}
