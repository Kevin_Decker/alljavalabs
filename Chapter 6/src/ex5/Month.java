package ex5;

public class Month {

	private int monthNumber;
	
	//constructors
	public Month()
	{
		monthNumber = 1;
	}
	
	public Month(int m)
	{	
		if(m < 1 || m > 12)
		{
			monthNumber = 1;
		}
		else
		{
			monthNumber = m;
		}
	}
	
	public Month(String m)
	{
		if(m.equalsIgnoreCase("january"))
		{
			monthNumber = 1;
		}
		else if(m.equalsIgnoreCase("february"))
		{
			monthNumber = 2;
		}
		else if(m.equalsIgnoreCase("march"))
		{
			monthNumber = 3;
		}
		else if(m.equalsIgnoreCase("april"))
		{
			monthNumber = 4;
		}
		else if(m.equalsIgnoreCase("may"))
		{
			monthNumber = 5;
		}
		else if(m.equalsIgnoreCase("june"))
		{
			monthNumber = 6;
		}
		else if(m.equalsIgnoreCase("july"))
		{
			monthNumber = 7;
		}
		else if(m.equalsIgnoreCase("august"))
		{
			monthNumber = 8;
		}
		else if(m.equalsIgnoreCase("september"))
		{
			monthNumber = 9;
		}
		else if(m.equalsIgnoreCase("october"))
		{
			monthNumber = 10;
		}
		else if(m.equalsIgnoreCase("november"))
		{
			monthNumber = 11;
		}
		else if(m.equalsIgnoreCase("december"))
		{
			monthNumber = 12;
		}
		else 
		{	
			monthNumber = 1;
		}
	}

	
	//mutators and accessors
	public int getMonthNumber() {
		return monthNumber;
	}

	public void setMonthNumber(int monthNumber) {
		if(monthNumber < 1 || monthNumber > 12)
		{
			monthNumber = 1;
		}
		else
		{
			this.monthNumber = monthNumber;
		}
	}
	
	//method that returns the month name
	public String getMonthName()
	{
		if(monthNumber == 1)
		{
			return "January";
		}
		else if(monthNumber == 2)
		{
			return "February";
		}
		else if(monthNumber == 3)
		{
			return "March";
		}
		else if(monthNumber == 4)
		{
			return "April";
		}
		else if(monthNumber == 5)
		{
			return "May";
		}
		else if(monthNumber == 6)
		{
			return "June";
		}
		else if(monthNumber == 7)
		{
			return "July";
		}
		else if(monthNumber == 8)
		{
			return "August";
		}
		else if(monthNumber == 9)
		{
			return "September";
		}
		else if(monthNumber == 10)
		{
			return "October";
		}
		else if(monthNumber == 11)
		{
			return "November";
		}
		else if(monthNumber == 12)
		{
			return "December";
		}
		else
		{	
			return "Error";
		}
	}

	public String toString()
	{
		return String.format("%s", getMonthName());
	}
	
	public boolean equals(Month month)
	{
		if(this.monthNumber == month.monthNumber)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public boolean greaterThan(Month month)
	{
		if(this.monthNumber > month.monthNumber)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean lessThan(Month month)
	{
		if(this.monthNumber < month.monthNumber)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
