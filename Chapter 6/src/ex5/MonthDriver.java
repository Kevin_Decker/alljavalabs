package ex5;

import java.util.Scanner;

public class MonthDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a month number in an integer: ");
		int m1 = keyboard.nextInt();
		keyboard.nextLine();
		System.out.println("Enter a second month as a string: ");
		String m2 = keyboard.nextLine();
		
		
		Month month1 = new Month(m1);
		Month month2 = new Month(m2);
		
		System.out.println(month1);
		System.out.println(month2);
		System.out.printf("Are they equal? %s", month1.equals(month2));
	}

}
