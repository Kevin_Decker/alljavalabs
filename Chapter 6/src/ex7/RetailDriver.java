package ex7;

public class RetailDriver {

	public static void main(String[] args) {
		
		RetailItem item = new RetailItem("Candy bar", 1234, 0.75, 1.5);
		System.out.println("Original Example");
		System.out.println(item);
		System.out.println();
		System.out.println("Using Mutators and Accessors");
		
		RetailItem item2 = new RetailItem("soda", 5678, 1.00, 1.99);
		System.out.println(item2);


	}

}
