package ex7;

public class RetailItem {

	private String description;
	private int itemNumber;
	private CostData cost;
	
	//constructor
	public RetailItem(String desc, int itemNum, double wholesale, double retail)
	{
		description = desc;
		itemNumber = itemNum;
		cost = new CostData();
		cost.setRetail(retail);
		cost.setWholesale(wholesale);
	}
	
	public String toString()
	{
		return String.format("Description: %s\nItem Number: %d\nWholesale Cost: $%,.2f\nRetail Price: $%,.2f\n", 
				description, itemNumber, cost.wholesale, cost.retail);
	}
	
	private class CostData {
		public double wholesale, retail;
		
		public double getWholesale() {
			return wholesale;
		}
		public void setWholesale(double wholesale) {
			this.wholesale = wholesale;
		}
		public double getRetail() {
			return retail;
		}
		public void setRetail(double retail) {
			this.retail = retail;
		}

		public CostData()
		{
			
		}
		
		public CostData(double w, double r)
		{
			wholesale = w;
			retail = r;
		}
	}
	
}

