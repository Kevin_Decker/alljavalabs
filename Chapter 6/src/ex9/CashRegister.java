package ex9;

public class CashRegister {

	private RetailItem item;
	private int quantity;
	
	//constructor
	public CashRegister(RetailItem ri, int q)
	{
		item = ri;
		quantity = q;
	}
	
	//method to return the subtotal of the sale, quantity X retail cost
	public double getSubtotal()
	{
		return quantity * item.getRetailCost();
	}
	
	//method that returns the amount of sales tax
	public double getTax()
	{
		return getSubtotal() * .06;
	}
	
	//method that returns the total of the sale, subtotal plus sales tax
	public double getTotal()
	{
		return getSubtotal() + getTax();
	}
	
}
