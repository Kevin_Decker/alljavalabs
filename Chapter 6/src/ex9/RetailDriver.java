package ex9;

import java.util.Scanner;
import java.io.*;

public class RetailDriver {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is the quantity of items being purchased?");
		int quantity = keyboard.nextInt();
		
		RetailItem item1 = new RetailItem("soda", 5678, 1.00, 1.99);
		CashRegister sale1 = new CashRegister(item1, quantity);
		
		PrintWriter receipt = new PrintWriter("receipt.txt");
		receipt.println("SALES RECEIPT");
		receipt.print("Unit Price: $");
		receipt.println(item1.getRetailCost());
		receipt.print("Quantity: ");
		receipt.println(quantity);
		receipt.print("Subtotal: $");
		receipt.println(sale1.getSubtotal());
		receipt.print("Sales Tax: $");
		receipt.println(sale1.getTax());
		receipt.print("Total: $");
		receipt.print(sale1.getTotal());
		
		receipt.close();
		
		System.out.println("Receipt Printed.");
	}

}
