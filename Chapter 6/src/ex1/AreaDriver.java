package ex1;


public class AreaDriver {

	public static void main(String[] args) {
		
		System.out.println("Circle");
		System.out.printf("%,.2f\n", Area.calcArea(10));
		System.out.println();
		System.out.println("Rectangle");
		System.out.printf("%,.2f\n", Area.calcArea(12, 15));
		System.out.println();
		System.out.println("Cylinder");
		System.out.printf("%,.2f\n", Area.calcArea(8, 12.75));
	}

}
