package ex1;

public class Area {
	
	//3 overloaded static methods for calculating area
	//Circles
	public static double calcArea(double radius)
	{
		double r = Math.pow(radius, 2);
		double area = Math.PI*(r);
		return area;
	}
	
	//Rectangles
	public static double calcArea(int length, int width)
	{
		double area = length * width;
		return area; 
	}
	
	//Cylinders
	public static double calcArea(double radius, double height)
	{
		double r = Math.pow(radius, 2);
		double area = Math.PI * (r) * height;
		return area;
	}
}
