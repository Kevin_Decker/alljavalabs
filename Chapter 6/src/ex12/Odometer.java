package ex12;

public class Odometer {

	public final int MAX_MILEAGE = 999999;
	public final int MPG = 24;
	private double currentMileage;
	private double initialMileage;
	private FuelGauge fGauge;
	
	//constructor
	public Odometer(double m, FuelGauge gauge)
	{
		initialMileage = m;
		currentMileage = m;
		fGauge = gauge;
	}

	public double getCurrentMileage() {
		return currentMileage;
	}

	public void setCurrentMileage(double currentMileage) {
		this.currentMileage = currentMileage;
	}
	
	//method to increment the current mileage by 1, max mileage of odometer can store 999,999 miles, if over sets back to 0
	public void increaseMileage()
	{
		if(currentMileage > MAX_MILEAGE)
		{
			currentMileage = 0;
		}
		else
		{
			currentMileage++;
		}
		
		double milesDriven = initialMileage - currentMileage;
		if(milesDriven % MPG == 0)
		{
			fGauge.decreaseFuel();
		}
	}
	
/*	public void decreaseMileage()
	{
		
	}*/
	
}
