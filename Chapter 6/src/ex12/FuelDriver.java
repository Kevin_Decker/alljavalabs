package ex12;

public class FuelDriver {

	public static void main(String[] args) {
		
		FuelGauge fuelGauge = new FuelGauge();
		Odometer odometer = new Odometer(0, fuelGauge);
		
		for(int x = 0; x < 15; ++x)
		{
			fuelGauge.increaseFuel();
		}
		
		while(fuelGauge.getCurrentGallons() > 0)
		{
			odometer.increaseMileage();
			
			System.out.println("Mileage: " + odometer.getCurrentMileage());
			
			System.out.println("Fuel level: " + fuelGauge.getCurrentGallons() + " gallons");
			System.out.println();
		}
	}

}
