package ex12;

public class FuelGauge {

	public final int MAX_GALLONS = 15;
	private int currentGallons;
	
	//constructor
	public FuelGauge(int curGall)
	{
		currentGallons = curGall;
	}
	
	public FuelGauge()
	{
		currentGallons = 0;
	}

	//Getter and Setter
	public int getCurrentGallons() {
		return currentGallons;
	}

	public void setCurrentGallons(int currentGallons) {
		this.currentGallons = currentGallons;
	}
	
	//method to increment amount of fuel by 1 gallon, max of 15 gallons
	public void increaseFuel()
	{
		if(currentGallons < MAX_GALLONS)
		{
			currentGallons++;
		}
		else 
		{
			System.out.println("GAS OVERFLOWING");
		}
	}
	
	//method to decrement amount of fuel by 1 gallon, if amount is greater than 0
	public void decreaseFuel()
	{
		if(currentGallons > 0)
		{
			currentGallons--;
		}
		else
		{
			System.out.println("OUT OF GAS");
		}
		
		if(currentGallons == 0.0)
		{
			System.out.println("OUT OF GAS");
		}
	}	
}
