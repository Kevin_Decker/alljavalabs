package ex10;

public class ParkingMeter {

	private int minutesPurchased;
	
	//constructor
	public ParkingMeter(int minPur)
	{
		super();
		minutesPurchased = minPur;
	}

	
	public int getMinutesPurchased() {
		return minutesPurchased;
	}

	public void setMinutesPurchased(int minutesPurchased) {
		this.minutesPurchased = minutesPurchased;
	}
	
	
	
}
