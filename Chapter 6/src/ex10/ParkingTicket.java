package ex10;

public class ParkingTicket {

	private ParkedCar car;
	private PoliceOfficer officer;
	private double fineAmount;
	private int minutes;
	//private ParkingMeter pMeter;
	
	//constructor
	public ParkingTicket(ParkedCar car, PoliceOfficer officer, int min)
	{
		super();
		this.car = car;
		this.officer = officer;
		this.minutes = min;
		
		getFineAmount();
	}

/*	public ParkingTicket(ParkingTicket p) {
		 
	}*/

	public ParkedCar getCar() {
		return car;
	}

	public void setCar(ParkedCar car) {
		this.car = car;
	}
	
	
	//method to report the details of the car
	public String getCarDetails()
	{
		return String.format("Make: %s\nModel: %s\nColor: %s\nLicense Number: %s\nMinutes Parked: %d", 
				car.getMake(), car.getModel(), car.getColor(), car.getLicenseNum(), car.getMinutesParked());
	}
	
	//method to report the name and badge num of issuing officer
	public String getOfficerDetails()
	{
		return String.format("Name: %s\nBadge Num: %d", officer.getName(), officer.getBadgeNum());
	}
	
	//method to report amount of the fine
	public void getFineAmount()
	{
		
		if(minutes > 0 && minutes <= 60)
		{
			fineAmount = 25;
		}
		else if(minutes > 60)
		{
			fineAmount += 25;
			
			int numOverHours = (minutes / 60) -1;
			
			int remainMinutes = minutes % 60;
			if(remainMinutes != 0)
			{
				numOverHours++;
			}
			
			fineAmount = fineAmount + (numOverHours * 10);
		}
		/*
		double hours = minutes / 60;
		double remainder = minutes % 60;
		//int hoursAsInt = (int)hours;
		
		if(remainder != 0)
		{
			hours++;
			fineAmount += (hours * 10.00);
		}
		
		fineAmount += 25.00;
		*/
		
/*		double min = car.getMinutesParked() - pMeter.getMinutesPurchased();
		double baseFine = 25.00;
		double mod = min % 60;
		if(min <= 60)
		{
			return baseFine;
		}
		else
		{
			return baseFine += (10 * mod);
		}*/
	}
	
	public String toString()
	{
		return String.format("PARKING TICKET\n" + getCarDetails() + "\n" + getOfficerDetails() + "\n" + "Total Fine: $" + fineAmount);
	}
	
}
