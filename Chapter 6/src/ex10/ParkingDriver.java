package ex10;

import java.util.Scanner;

public class ParkingDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		//System.out.println("POLICE REPORT");
		//System.out.println();
		System.out.println("Car Information");
		System.out.println("Make: ");
		String make = keyboard.nextLine();
		System.out.println("Model: ");
		String model = keyboard.nextLine();
		System.out.println("Color: ");
		String color = keyboard.nextLine();
		System.out.println("License Plate Number: ");
		String license = keyboard.nextLine();
		System.out.println("Minutes Parked: ");
		int minutesParked = keyboard.nextInt();
		System.out.println("Minutes Paid: ");
		int minutesPaid = keyboard.nextInt();
		keyboard.nextLine();
		System.out.println();
		System.out.println("Police Officer Information");
		System.out.println("Name: ");
		String name = keyboard.nextLine();
		System.out.println("Badge Number: ");
		int badge = keyboard.nextInt();
		System.out.println();
		
		//In our ParkedCar object we're passing 120 minutes the car was parked
		ParkedCar parkedCar = new ParkedCar(make, model, color, license, minutesParked);
		//In our ParkingMeter object we're passing 60 minutes purchased, which should result in a parking ticket
		ParkingMeter parkingMeter = new ParkingMeter(minutesPaid);
		
		PoliceOfficer newReport = new PoliceOfficer(name, badge);
		//In our ParkingTicket object we're simply passing through our ParkedCar and ParkingMeter objects, which will determine if a ticket is needed.
		ParkingTicket ticket = newReport.calcIllegalPark(parkedCar, parkingMeter);
		
		if(ticket != null)
		{
			System.out.println(ticket);
		}
		else
		{
			System.out.println("NO Parking Violations!");
		}

	}

}
