package ex10;

public class PoliceOfficer {

	private String name;
	private int badgeNum;
	//private ParkedCar pCar;
	//private ParkingMeter pMeter;
	//private ParkingTicket pTicket;
	
	//constructor
	public PoliceOfficer(String n, int badge)
	{
		super();
		name = n;
		badgeNum = badge;
	}
	
	//method to determine if car was parked illegally
	public ParkingTicket calcIllegalPark(ParkedCar car, ParkingMeter meter)
	{
		ParkingTicket ticket = null;
		
		int minutes = car.getMinutesParked() - meter.getMinutesPurchased();
		
		if(minutes > 0)
		{
			ticket = new ParkingTicket(car, this, minutes);
		}
		
		return ticket;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBadgeNum() {
		return badgeNum;
	}

	public void setBadgeNum(int badgeNum) {
		this.badgeNum = badgeNum;
	}


	//method to determine whether the car's time has expired
/*	public boolean isCarExpired()
	{
		if(pCar.getMinutesParked() > pMeter.getMinutesPurchased())
		{
			return true;
		}
		else 
		{
			return false;
		}
	}*/
	
}
