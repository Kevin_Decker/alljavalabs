package ex8;

import java.util.Scanner;

public class RetailDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is the quantity of items being purchased?");
		int quantity = keyboard.nextInt();
		
		RetailItem item1 = new RetailItem("soda", 5678, .75, 1.50);
		
		CashRegister sale1 = new CashRegister(item1, quantity);
		
		System.out.printf("Subtotal: $%,.2f\nSales Tax: $%.2f\nTotal: $%,.2f", 
				sale1.getSubtotal(), sale1.getTax(), sale1.getTotal());

	}

}
