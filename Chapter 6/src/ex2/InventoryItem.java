package ex2;

public class InventoryItem {

	private String description;
	private int units;
	
	//3 constructors & a copy constructor
	public InventoryItem()
	{
		description = "";
		units = 0;
	}
	
	public InventoryItem(String d)
	{
		description = d;
		units = 0;
	}
	
	public InventoryItem(String d, int u)
	{
		description = d;
		units = u;
	}
	
	public InventoryItem(InventoryItem item)
	{
		this.description = item.description;
		this.units = item.units;
	}

	//Mutators and Accessors
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}
	
}
