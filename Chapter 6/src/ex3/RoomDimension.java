package ex3;

public class RoomDimension {

	private double width;
	private double length;
	
	//constructor
	public RoomDimension(double len, double wid)
	{
		length = len;
		width = wid;
	}
	
	//Mutators and Accessors
	public double getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	//method to return area
	public double calcArea()
	{
		return width * length;
	}
	
	public String toString()
	{
		return String.format("With a length of %,.2f and a width of %,.2f, the area is %,.2f.\n", length, width, calcArea());
	}
	
}
