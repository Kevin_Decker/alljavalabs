package ex3;

import java.util.Scanner;

public class RoomDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Welcome to Carpet Calculator");
		System.out.println();
		
		System.out.println("Enter in width of room: ");
		double width = keyboard.nextDouble();
		System.out.println("Enter in length of room: ");
		double length = keyboard.nextDouble();
		System.out.println("Enter cost per square foot: ");
		double cost = keyboard.nextDouble();
		
		RoomDimension room1 = new RoomDimension(length, width);
		RoomCarpet room = new RoomCarpet(room1, cost);
		
		System.out.println(room1);
		System.out.println(room);
	}

}
