package ex3;

public class RoomCarpet {

	private RoomDimension room;
	private double costPerSqFoot;
	
	//constructor
	public RoomCarpet(RoomDimension dim, double cost)
	{
		room = dim;
		costPerSqFoot = cost;
	}
	
	//Mutators and Accessors
 	public RoomDimension getRoom() {
		return room;
	}
	public void setRoom(RoomDimension room) {
		this.room = room;
	}
	public double getCostPerSqFoot() {
		return costPerSqFoot;
	}
	public void setCostPerSqFoot(double costPerSqFoot) {
		this.costPerSqFoot = costPerSqFoot;
	}
	
	//method that returns total cost of carpet
	public double calcTotal()
	{
		return room.calcArea() * costPerSqFoot;
	}
	
	public String toString()
	{
		return String.format("Total Cost: $%,.2f", calcTotal());
	}
	
}
