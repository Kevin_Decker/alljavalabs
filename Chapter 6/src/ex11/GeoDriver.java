package ex11;

import java.util.Scanner;

import ex1.Area;

public class GeoDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		for(;;)
		{
			System.out.println("Geometry Calculator");
			System.out.println();
			System.out.println("1. Calculate the Area of a Circle");
			System.out.println("2. Calculate the Area of a Rectangle");
			System.out.println("3. Calculate the Area of a Triangle");
			System.out.println("4. Quit");
			System.out.println();
			System.out.println("Enter your choice (1-4)");
			int userChoice = keyboard.nextInt();
			
			switch (userChoice) {
			case 1:
				System.out.println("Circle");
				System.out.println("Enter a radius: ");
				double radius = keyboard.nextDouble();
				Geometry.getArea(radius);
				break;
			case 2:
				System.out.println("Rectangle");
				System.out.println("Enter a length: ");
				double length = keyboard.nextDouble();
				System.out.println("Enter a width: ");
				double width = keyboard.nextDouble();
				Geometry.getArea(length, width);
				break;
			case 3:
				System.out.println("Triangle");
				System.out.println("Enter a length for the base: ");
				double lengthOfBase = keyboard.nextDouble();
				System.out.println("Enter a height: ");
				double height = keyboard.nextDouble();
				Geometry.getArea(lengthOfBase, height);
				break;
			case 4:
				System.exit(0);
				break;
			default:
				System.out.println("Error: Not a valid entry");
				break;
			}
		}
	}
}
