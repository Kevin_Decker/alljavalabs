package ex11;

public class Geometry {

	//method to return area of a circle
	public static void getArea(double radius)
	{
		double area = Math.PI * radius * radius;
		
		if(radius < 0)
		{
			System.out.println("Error: No negative values");
		}
		else
		{
			System.out.println(area);
		}
	}
	
	//method to return area of a rectangle
	public static void getArea(int length, int width) 
	{
		int area = length * width;
		
		if(length < 0 || width < 0)
		{
			System.out.println("Error: No negative values");
		}
		else
		{
			System.out.println(area);
		}
	}
	
	//method to return area of a triangle
	public static void getArea(double lengthOfBase, double height)
	{
		double area = lengthOfBase * height * 0.5;
		
		if(lengthOfBase < 0 || height < 0)
		{
			System.out.println("Error: No negative values");
		}
		else
		{
			System.out.println(area);
		}
	}
	
}
