package ex14;
import java.util.Scanner;

public class CoinDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		Coin coin1 = new Coin();
		Coin coin2 = new Coin();
		
		int player1Count = 0;
		int player2Count = 0;
		
		for(;;)
		{
			System.out.println();
			System.out.println("Player 1, guess side of coin: ");
			String p1Guess = keyboard.nextLine();
			coin1.Toss();
			if(coin1.getSideUp().equals(p1Guess))
			{
				player1Count += 1;
				System.out.println("Correct! It was indeed " + coin1.getSideUp());
				System.out.println("Player 1 Score: " + player1Count);
			}
			else
			{
				player1Count -= 1;
				System.out.println("Wrong! It was " + coin1.getSideUp());
				System.out.println("Player 1 Score: " + player1Count);
			}
			System.out.println();
			System.out.println("Player 2, guess side of coin: ");
			String p2Guess = keyboard.nextLine();
			coin2.Toss();
			if(coin2.getSideUp().equals(p2Guess))
			{
				player2Count += 1;
				System.out.println("Correct! It was indeed " + coin2.getSideUp());
				System.out.println("Player 2 Score: " + player2Count);
			}
			else 
			{
				player2Count -= 1;
				System.out.println("Wrong! It was " + coin2.getSideUp());
				System.out.println("Player 2 Score: " + player2Count);
			}
			
			if(player1Count == 5)
			{
				System.out.println();
				System.out.println("Player 1 Wins!");
				System.out.println("Player 1 Score: " + player1Count);
				System.out.println("Player 2 Score: " + player2Count);
				break;
			}
			if(player2Count == 5)
			{
				System.out.println();
				System.out.println("Player 2 Wins!");
				System.out.println("Player 2 Score: " + player2Count);
				System.out.println("Player 1 Score: " + player1Count);
				break;
			}	
		}	
	}
}
