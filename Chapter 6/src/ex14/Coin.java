package ex14;

import java.util.Random;

public class Coin {

	private String sideUp;
	
	//constructor
	public Coin()
	{
		Toss();
	}
	
	//accessor
	public String getSideUp() 
	{
		return sideUp;
	}
	
	//method that simulates tossing a coin, generate a random number
	public void Toss()
	{
		Random random = new Random();
		int randomNum = random.nextInt(2);
		
		if(randomNum == 0)
		{
			sideUp = "heads";
		}
		else
		{
			sideUp = "tails";
		}
		
	}
	
}
