import java.util.Scanner;

public class FastFood
{
    public static void main(String[] args)
    {
        // declaring the total variable that will be used later to calculate the total
        double total;
        
        // declaring integer variables that will accept input as ints
        int numberOfHamburgers, numberOfCheeseburgers, numberOfSodas, numberOfFries;
        
        // declaring a string that will accept the user's name as input
        String name;
        
        
        // creating a scanner object that will allow us to accept user input
        Scanner userInput = new Scanner(System.in);
        
        // asks for number of hamburgers sets the variable to accept an int
        System.out.println("How many hamburgers would you like?");
        numberOfHamburgers = userInput.nextInt();
        
        // asks for number of cheesehburgers sets the variable to accept an int
        System.out.println("How many cheeseburgers would you like?");
        numberOfCheeseburgers = userInput.nextInt();
        
        // asks for number of sodas sets the variable to accept an int
        System.out.println("How many sodas would you like?");
        numberOfSodas = userInput.nextInt();
        
        // asks for number of fries sets the variable to accept an int
        System.out.println("How many fries would you like?");
        numberOfFries = userInput.nextInt();
        
        
        // this allows us to enter the name w/o the program skipping over it and displaying the total
        userInput.nextLine();
        
        
        // this grouping asks for their name and then accepts it as a string
        System.out.println("What is your name?");
        name = userInput.nextLine();
        
        // converts the name into all upper case letters
        String upperCase = name.toUpperCase();
        
        // calculates the total and placing the value in the total variable
        total = numberOfHamburgers * 1.25 + numberOfCheeseburgers * 1.50 + numberOfSodas * 1.95 + numberOfFries * 0.95;

        
        // this prints out the total and the name in all caps
        System.out.println("\nName: " + upperCase);
        System.out.printf("Total: $%,.2f\n", total);
        
    }
}