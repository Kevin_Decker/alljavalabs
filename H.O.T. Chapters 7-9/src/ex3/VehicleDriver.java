package ex3;

public class VehicleDriver {

	public static void main(String[] args) {
		
		//instantiating a vehicle reference that upcasts a car, demonstrating polymorphism
		Vehicle v1 = new Car(0);
		//calling the increaseSpeed method that should increase the speed by 10
		v1.accelerate();
		v1.accelerate();
		//calling the getSpeed accessor to retrieve the current speed
		System.out.println("Car: " + v1.getSpeed());

		System.out.println();
		
		//instantiating a vehicle reference that upcasts a truck, demonstrating polymorphism
		Vehicle v2 = new Truck(0);
		//calling the increaseSpeed method that should increase the speed by 3
		v2.accelerate();
		//calling the getSpeed accessor to retrieve the current speed
		System.out.println("Truck: " + v2.getSpeed());
	}

}
