package ex3;

public class Truck extends Vehicle {

	
	//constructor
	public Truck(int s)
	{
		super(s);
	}
	
	@Override
	public void accelerate()
	{
		speed += 3;
	}
	
}
