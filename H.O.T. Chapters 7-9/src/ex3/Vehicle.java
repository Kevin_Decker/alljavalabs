package ex3;

public class Vehicle {

	protected int speed;
	
	//constructor
	public Vehicle(int s)
	{
		speed = s;
	}
	
	//getter and setter
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	//method that increases the speed field by 5 each time it is called
	public void accelerate()
	{
		speed += 5;
	}
}
