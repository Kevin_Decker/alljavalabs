package ex2;

public class Game {

	// instance fields 
	private String name;
	private int maxNumPlayers;
	
	//constructor
	public Game(String name, int maxPlayers) 
	{
		this.name = name;
		maxNumPlayers = maxPlayers;
	}
	
	//getters and setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMaxNumPlayers() {
		return maxNumPlayers;
	}
	public void setMaxNumPlayers(int maxNumPlayers) {
		this.maxNumPlayers = maxNumPlayers;
	}
	
	//this method is overriding the toString from Object
	@Override
	public String toString()
	{
		return "Game Name: " + name + "\n" + "Max Players: " + maxNumPlayers;
	}
}
