package ex2;

public class GameWithTimeLimit extends Game {

	//instance field
	private int timeLimitInMinutes;
	
	//constructor
	public GameWithTimeLimit(String name, int maxPlayers, int minutes)
	{
		//pushing the name and maxplayers to the superclass
		super(name, maxPlayers);
		this.timeLimitInMinutes = minutes;
	}

	//getter and setter
	public int getMinutes() {
		return timeLimitInMinutes;
	}
	public void setMinutes(int minutes) {
		this.timeLimitInMinutes = minutes;
	}
	
	//this toString is overriding the toString from the Game class..
	@Override
	public String toString()
	{
		return "Game Name: " + this.getName() + "\n" + "Max Players: " + this.getMaxNumPlayers() + "\n" + "Time Limit: " + timeLimitInMinutes;
	}
}
