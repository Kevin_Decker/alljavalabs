package ex2;

public class GameDriver {

	public static void main(String[] args) {
		
		//instantiating a Game object and a GameWithTimeLimit object
		Game newGame = new Game("Crash Bandicoot", 1);
		GameWithTimeLimit gameWithTimeLimit = new GameWithTimeLimit("Fortnite", 100, 30);
		
		//implicitly calling the toString's from both objects/classes
		System.out.println(newGame);
		System.out.println();
		System.out.println(gameWithTimeLimit);
		
	}

}
