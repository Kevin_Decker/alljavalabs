package ex1;

public class ChatAWhile {

	//parallel arrays that stores the area codes and minute rates
	private int[] areaCodes = {262, 414, 608, 715, 815, 920};
	private double[] perMinuteRate = {.07, .10, .05, .16, .24, .14};
	
	//constructor
	public ChatAWhile()
	{
		
	}
	
	//getters and setters
	public int[] getAreaCodes() {
		return areaCodes;
	}
	public void setAreaCodes(int[] areaCodes) {
		this.areaCodes = areaCodes;
	}
	public double[] getPerMinuteRate() {
		return perMinuteRate;
	}
	public void setPerMinuteRate(double[] perMinuteRate) {
		this.perMinuteRate = perMinuteRate;
	}
	
	//method that calculates the total cost based on the number of minutes and the area code passed through
	//the for loop first compares the user input area code to the array of area codes, then multiplies the number of 
	//minutes to the appropriate subscript in the perMinuteRate array
	public double getTotalCost(int minutes, int code)
	{
		double total = 0;
		for(int x = 0; x < areaCodes.length; ++x)
		{
			if(areaCodes[x] == code)
			{
				total = minutes * perMinuteRate[x];
			}
		}
		return total;
	}
}
