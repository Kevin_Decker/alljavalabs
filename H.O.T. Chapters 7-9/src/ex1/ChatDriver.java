package ex1;
import java.util.Scanner;

public class ChatDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		//instantiating a ChatAWhile object
		ChatAWhile chatAWhile = new ChatAWhile();
		//2 local variables that will hold data from the user
		int areaCodes = 0;
		int minutes = 0;
		
		//accepting user input for area code
		System.out.println("Enter in your area code: ");
		areaCodes = keyboard.nextInt();
		
		//accepting user input for minutes
		System.out.println("Enter length of time in minutes");
		minutes = keyboard.nextInt();
		
		// passing user entry of area code and length of minutes to a method that calculates the total cost
		System.out.printf("$%,.2f", chatAWhile.getTotalCost(minutes, areaCodes));
		
		

	}

}
