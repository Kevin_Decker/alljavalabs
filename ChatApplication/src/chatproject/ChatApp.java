package chatproject;

import javafx.scene.control.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class ChatApp extends Application {

	//if isServer is true the application runs as the server/host
	//if isServer is false the application runs as a client
	private boolean isServer = false;
	private TextField input;
	
	Label title;
	private TextArea messages = new TextArea();
	private NetworkConnection connection = isServer ? createServer() : createClient();
	
	@Override
	public void init() throws Exception{
		connection.startConnection();
	}
	
	@Override
	public void stop() throws Exception{
		connection.closeConnection();
	}
	
	public static void main(String[] args) {
		
		launch();

	}
	
	private Server createServer()
	{
		return new Server(55555, data -> {
			Platform.runLater(() -> {
				getMessage();
				messages.appendText(data.toString() + "\n");
			});
		});
	}
	
	private Client createClient()
	{
		//ip address of local host(same machine)
		//multiple machines on same network type internal ip address
		//over Internet type external ip of the server
		//172.16.0.41 ip for ranken server
		return new Client("10.150.0.64", 55555, data -> {
			Platform.runLater(() -> {
				getMessage();
				messages.appendText(data.toString() + "\n");
			});
		});
	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		arg0.setTitle("ChatApp");
		
		title = new Label("ChatApp");
		
		title.setId("titleLabel");
		title.setStyle("-fx-font-size: 24;-fx-text-fill: blue;-fx-font-weight: bold;"
				+ "-fx-font-family:san-serif;-fx-padding:15 0 0 20");

		messages.setPrefHeight(550);
		messages.setStyle("-fx-font-size: 14");
		input = new TextField("Type message here...");
		input.setOnMouseClicked(e ->{
			input.clear();
		});
		input.setMinWidth(510);
		input.setMinHeight(60);
		input.setStyle("-fx-padding: -10 0 5 0;-fx-font-size: 16");

		Button sendBtn = new Button("Send");
		sendBtn.setMinHeight(60);
		sendBtn.setMinWidth(90);
		sendBtn.setId("activeBtn");
		sendBtn.setStyle("-fx-background-color: #7FFFD4");
		
		//action that calls the writeMessage which writes the user's message to the text area
		sendBtn.setOnAction(e -> {
			//simulate a button click, changing the border color (not changing back yet)
			sendBtn.setStyle("-fx-border-color: #5F9EA0;-fx-background-color: #7FFFD4");
			writeMessage();
			});
		input.setOnAction(e -> writeMessage());
		
		HBox hBox = new HBox(input, sendBtn);
		hBox.setStyle("-fx-padding: -10 0 10 0");
		
		VBox root = new VBox(20, title, messages, hBox);
		root.setPrefSize(600, 600);
		//return root;
		Scene scene = new Scene(root);
		input.requestFocus();
		
		//String css = ChatApp.class.getResource("/src/syle.css").toExternalForm();
		//scene.getStylesheets().add(css);
		arg0.setScene(scene);
		arg0.show();
	}
	
	private void writeMessage()
	{
		messages.setStyle("-fx-font-size: 14");
		String message = isServer ? "Kevin (Server): " : "Client: ";
		message += input.getText();
		input.clear();
		
		messages.appendText(message + "\n");
		
		try {
			connection.send(message);
		} catch (Exception e1) {
			messages.appendText("Failed to send\n");
		}
	}
	
	private void getMessage()
	{
		messages.setStyle("-fx-font-size: 14");
		String message = isServer ? "Kevin (Server): " : "Client: ";
		message += input.getText();
		input.clear();
		
		//changes the text color to blue for the server, and orange for the client
		if(isServer)
		{
			setTextColorBlue(message);
		}
		else
		{
			setTextColorOrange(message);
		}
	}
	
	private void setTextColorBlue(String str)
	{
		messages.setStyle("-fx-text-fill: blue");
	}
	
	private void setTextColorOrange(String str)
	{
		messages.setStyle("-fx-text-fill: orange");
	}

}
