package ex12;
import java.util.Scanner;

public class BarChart {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);	
		int[] sales = new int[5];
		
		for(int x = 0; x < 5; ++x)
		{
			System.out.println("Enter today's sales for store " + (x + 1));
			sales[x] = keyboard.nextInt();
		}
		
		System.out.println("SALES BAR CHART");
		
		for(int i = 0; i < 5; ++i)
		{
			System.out.printf("\nStore %d: ", i + 1);
			for(int y = 0; y < sales[i]/100; ++y)
			{
				System.out.print("*");
			}
		}
		

		
		
		
	}

}
