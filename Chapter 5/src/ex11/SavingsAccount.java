package ex11;

public class SavingsAccount {

	private double interestRate;
	private double balance;
	
	//constructor
	public SavingsAccount(double bal)
	{
		balance = bal;
	}
	
	public SavingsAccount()
	{
		
	}
	
	//mutators and accessors
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	//method for subtracting the amount of a withdrawal
	public void calcWithdrawal(double withdraw)
	{
		balance -= withdraw;
	}
	
	//method for adding the amount of a deposit
	public void addDeposit(double deposit)
	{
		balance += deposit;
	}
	
	//method for adding the amount of monthly interest to the balance
	public double addInterestToBalance()
	{
		double monthlyInterest = ((double)interestRate / 12) * balance;
		return balance += monthlyInterest;
	}
	
}
