package ex11;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SavingsDriver {

	public static void main(String[] args) throws IOException {
		
		Scanner keyboard = new Scanner(System.in);
		/*
		System.out.println("What is the annual interest rate?");
		double rate = keyboard.nextDouble();
		
		System.out.println("What is your starting balance?");
		double balance = keyboard.nextDouble();
		
		System.out.println("How many months have passed since the account was established?");
		double months = keyboard.nextDouble();*/
		
		BufferedReader deposits = new BufferedReader(new FileReader("Deposits.txt"));
		BufferedReader withdrawals = new BufferedReader(new FileReader("Withdrawals.txt"));
		
		double deposit=0, withdraw=0;
		String depositStr, withdrawStr;
		depositStr = deposits.readLine();
		withdrawStr = withdrawals.readLine();
		
		SavingsAccount s1 = new SavingsAccount(500);
		
		double totalDeposits=0, totalWithdawals=0, totalInterest=0, totalBalance = 0;
		s1.setInterestRate(.12);
		
		for(int i = 0; i < 5; ++i)
		{

			deposit = Double.parseDouble(depositStr);
			withdraw = Double.parseDouble(withdrawStr);
			
			//double deposit = inputFile.nextDouble();
			s1.addDeposit(deposit);
			
			//double withdrawn = inputFile2.nextDouble();
			s1.calcWithdrawal(withdraw);
			
			s1.addInterestToBalance();
			
			totalDeposits += deposit;
			totalWithdawals += withdraw;
			totalInterest += s1.addInterestToBalance();
			totalBalance = 500 + totalDeposits - totalWithdawals + totalInterest;
			
			depositStr = deposits.readLine();
			withdrawStr = withdrawals.readLine();
		}
		
		System.out.printf("Ending Balance: $%,.2f\n", totalBalance);
		System.out.printf("Total Deposits: $%,.2f\n", totalDeposits);
		System.out.printf("Total Withdrawals: $%,.2f\n", totalWithdawals);
		//System.out.printf("Total Interest Earned: $%,.2f", totalInterest);
		
		keyboard.close();
		deposits.close();
		withdrawals.close();
		
	}

}
