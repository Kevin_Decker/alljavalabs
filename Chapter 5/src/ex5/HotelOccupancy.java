package ex5;

import java.util.Scanner;

public class HotelOccupancy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		double totalRooms=0, occupied=0, totalVacant=0, occupancyRate=0, totalOccupied=0;
		
		System.out.println("How many floors does the hotel have?");
		int floors = keyboard.nextInt();
		
		while(floors < 1)
		{
			System.out.println("How many floors does the hotel have?");
			floors = keyboard.nextInt();
		}
		
		for(int i = 0; i < floors; ++i)
		{
			System.out.println("How many rooms are on floor " + (i+1));
			double rooms = keyboard.nextInt();
			while(rooms >= 10)
			{
				System.out.println("How many of the rooms are occupied?");
				occupied = keyboard.nextInt();
				
				totalRooms = floors * rooms;
				totalOccupied += occupied;
				totalVacant = totalRooms - totalOccupied;
				occupancyRate = (totalOccupied / totalRooms) * 100;
				break;
			}
		}
		
		System.out.println("Total Rooms: " + totalRooms);
		System.out.println("Occupied Rooms: " + totalOccupied);
		System.out.println("Vacant Rooms: " + totalVacant);
		System.out.printf("Occupancy Rate: %%%.2f\n", occupancyRate );
		
	}

}
