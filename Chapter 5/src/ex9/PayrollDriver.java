package ex9;

import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		
		int id=0;
		
		double grossPay=0, stateTax=0, fedTax=0, 
		fica=0, totalGP=0, totalST=0, totalFT=0, 
		totalFICA=0, totalNP=0;
		
		for(;;)
		{
			System.out.println("Enter employee ID #: ");
			id = keyboard.nextInt();
			
			if(id == 0) {break;}
			if(id < 0) {System.out.println("Enter employee ID #: ");
			id = keyboard.nextInt();}
			
			System.out.println("Enter employee gross pay: ");
			grossPay = keyboard.nextDouble();
			
			if(grossPay < 0) {
				System.out.println("Can't be negative: ");
				continue;
			}
			
			System.out.println("Enter state tax: ");
			stateTax = keyboard.nextDouble();
			
			if(stateTax < 0 || stateTax > grossPay) {
				System.out.println("Error, can't be negative or greater than gross pay: ");
				continue;
			}
			
			System.out.println("Enter federal tax: ");
			fedTax = keyboard.nextDouble();
			
			if(fedTax < 0 || fedTax > grossPay) {
				System.out.println("Error, can't be negative or greater than gross pay: ");
				continue;
			}
			
			System.out.println("Enter FICA holdings: ");
			fica = keyboard.nextDouble();
			
			if(fica < 0 || fica > grossPay) {
				System.out.println("Error, can't be negative or greater than gross pay: ");
				continue;
			}
			
			Payroll p1 = new Payroll(id, grossPay, stateTax, fedTax, fica);
			
			System.out.printf("Net Pay: $%,.2f\n", p1.calcNetPay());
			
			totalGP += grossPay;
			totalST += stateTax;
			totalFT += fedTax;
			totalFICA += fica;
			totalNP += p1.calcNetPay();

		}
		System.out.println();
		System.out.printf("Total Gross Pay: $%,.2f\n", totalGP);
		System.out.printf("Total State Tax: $%,.2f\n", totalST);
		System.out.printf("Total Federal Tax: $%,.2f\n", totalFT);
		System.out.printf("Total FICA Holdings: $%,.2f\n", totalFICA);
		System.out.printf("Total Net Pay: $%,.2f", totalNP);
		
	}

}
