package ex9;

public class Payroll {

	private int _idNum;
	private double _grossPay;
	private double _stateTax;
	private double _fedTax;
	private double _ficaHoldings;
	
	//constructor
	public Payroll(int id, double gPay, double state, double fed, double fica)
	{
		_idNum = id;
		_grossPay = gPay;
		_stateTax = state;
		_fedTax = fed;
		_ficaHoldings = fica;
	}
	
	//mutators and accessors
	public int get_idNum() {
		return _idNum;
	}
	public void set_idNum(int _idNum) {
		this._idNum = _idNum;
	}
	public double get_grossPay() {
		return _grossPay;
	}
	public void set_grossPay(double _grossPay) {
		this._grossPay = _grossPay;
	}
	public double get_stateTax() {
		return _stateTax;
	}
	public void set_stateTax(double _stateTax) {
		this._stateTax = _stateTax;
	}
	public double get_fedTax() {
		return _fedTax;
	}
	public void set_fedTax(double _fedTax) {
		this._fedTax = _fedTax;
	}
	public double get_ficaHoldings() {
		return _ficaHoldings;
	}
	public void set_ficaHoldings(double _ficaHoldings) {
		this._ficaHoldings = _ficaHoldings;
	}
	
	//method that calculates the employees net pay
	public double calcNetPay()
	{
		return _grossPay - _stateTax - _fedTax - _ficaHoldings;
	}
	
	
}
