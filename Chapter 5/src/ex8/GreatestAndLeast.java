package ex8;

import java.util.Scanner;

public class GreatestAndLeast {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		int userInt=0, min=Integer.MAX_VALUE, max=Integer.MIN_VALUE;
		
		System.out.println("Enter an integer. Enter -99 to quit: ");
		userInt = keyboard.nextInt();
		
		while(userInt != -99)
		{
			if(userInt > max) 
			{
				max = userInt;
			}
			else if(userInt < min) 
			{
				min = userInt;
			}
			else 
			{		
				System.out.println("Enter an integer. Enter -99 to quit: ");
				userInt = keyboard.nextInt();
			}
		}
		
		System.out.println("Greatest Number: " + max);
		System.out.println("Least Number: " + min);
		
	}

}
