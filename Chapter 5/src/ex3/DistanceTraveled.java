package ex3;

public class DistanceTraveled {

	private double speedOfVehicle;
	private double hours;
	
	//constructor
	
	
	//mutator
	public void setSpeed(double speed)
	{
		speedOfVehicle = speed;
	}
	
	public void setHours(double hour)
	{
		hours = hour;
	}
	
	//accessors
	public double getSpeed()
	{
		return speedOfVehicle;
	}
	
	public double getHours()
	{
		return hours;
	}
	
	//method that returns the distance
	public double getDistance()
	{
		return speedOfVehicle * hours;
		//return distance / hours;
	}
	
}
