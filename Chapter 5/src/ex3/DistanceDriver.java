package ex3;

import java.util.Scanner;
import java.io.*;


public class DistanceDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		double speed=0, time=0;
		
		System.out.println("Enter speed of vehicle: ");
		speed = keyboard.nextDouble();
		
		while(speed < 0)
		{
			System.out.println("Enter speed of vehicle: ");
			speed = keyboard.nextDouble();	
		}
		
		System.out.println("Enter hours traveled: ");
		time = keyboard.nextDouble();
		
		while(time < 1)
		{
			System.out.println("Enter hours traveled: ");
			time = keyboard.nextDouble();
		}

		DistanceTraveled distance1 = new DistanceTraveled();
		distance1.setSpeed(speed);

		for(int hour = 1; hour <= time; ++hour)
		{
			distance1.setHours(hour);
			System.out.println(hour + "\t" + distance1.getDistance());
		}
		
		keyboard.nextLine();
		
		System.out.println("Enter file name: ");
		String fileName = keyboard.nextLine();

		try {
			PrintWriter openFile = new PrintWriter(fileName + ".txt");
			for(int i = 1; i <= time; ++i)
			{
				distance1.setHours(i);
				openFile.println(i + "\t" + distance1.getDistance());
			}
			openFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
}
