package ex14;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileDriver {

	public static void main(String[] args) throws IOException {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is the file name?");
		String filename = keyboard.nextLine();
		
		FileDisplay f1 = new FileDisplay(filename);
		
		f1.displayHead();
		
		System.out.println();
		System.out.println();
		
		f1.displayContents();
		
		System.out.println();
		System.out.println();
		
		f1.displayWithLineNumbers();
	}

}
