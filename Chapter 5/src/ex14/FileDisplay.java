package ex14;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

public class FileDisplay {

	private String file;
	
	//constructor
	public FileDisplay(String file)
	{
		this.file = file;
	}
	
	//method to display first 5 lines of the file's contents
	public void displayHead() throws IOException
	{
		FileReader fileReader = new FileReader(file);
		
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String contents = null;
		int i = 0;
		while((contents = bufferedReader.readLine()) != null && i < 5) 
		{
			System.out.println(contents);
			++i;
		}
		bufferedReader.close();
	}
	
	//method to display all contents
	public void displayContents() throws IOException
	{	
		FileReader fileReader = new FileReader(file);
		
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String contents = null;
		while((contents = bufferedReader.readLine()) != null) 
		{
			System.out.println(contents);
		}
		bufferedReader.close();
		
	}
	
	//method to display all contents with line numbers
	public void displayWithLineNumbers() throws IOException
	{
		FileReader fileReader = new FileReader(file);
		
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String contents = null;
		int count = 1;
		while((contents = bufferedReader.readLine()) != null) 
		{
			System.out.println(count + ". " + contents);
			count++;
		}
		bufferedReader.close();
	}
}
