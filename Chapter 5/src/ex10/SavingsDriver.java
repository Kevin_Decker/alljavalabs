package ex10;
import java.util.Scanner;

public class SavingsDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is the annual interest rate?");
		double rate = keyboard.nextDouble();
		
		System.out.println("What is your starting balance?");
		double balance = keyboard.nextDouble();
		
		System.out.println("How many months have passed since the account was established?");
		double months = keyboard.nextDouble();
		
		SavingsAccount s1 = new SavingsAccount(balance, rate);
		
		double totalDeposits=0, totalWithdawals=0, totalInterest=0, totalBalance = 0;
		
		for(int i = 0; i < months; ++i)
		{
			System.out.println("Amount deposited for this month?");
			double deposit = keyboard.nextDouble();
			totalDeposits += deposit;
			s1.addDeposit(deposit);
			
			System.out.println("What is the amount withdrawn during this month?");
			double withdrawn = keyboard.nextDouble();
			totalWithdawals += withdrawn;
			s1.calcWithdrawal(withdrawn);
			
			
			s1.addInterestToBalance();
			
			totalInterest += s1.getInterestRate();
			
			//totalBalance = balance + totalDeposits - totalWithdawals + totalInterest;
		}
		
		System.out.printf("Ending Balance: $%,.2f\n",s1.getBalance());
		System.out.printf("Total Deposits: $%,.2f\n", totalDeposits);
		System.out.printf("Total Withdrawals: $%,.2f\n", totalWithdawals);
		//System.out.printf("Interest Earned: $%,.2f", totalInterest);
		
	}

}
