package ex13;

public class CentigradeToFahrenheit {

	public static void main(String[] args) {
		
		for(int c = 0; c < 20; ++c)
		{
			double fraction = 9 / (double)5.0;
			double fahrenheit = (fraction * c) + 32;
			
			System.out.println("C: " + c + ", F: " + fahrenheit);
		}

	}

}
