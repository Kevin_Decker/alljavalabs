package ex4;
import java.util.Scanner;

public class PenniesForPay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		double salary = .01;
		double total = 0;
		
		System.out.println("Enter number of days worked: ");
		int numDaysWorked = keyboard.nextInt();
		
		while(numDaysWorked < 1)
		{
			System.out.println("Enter number of days worked: ");
			numDaysWorked = keyboard.nextInt();	
		}
		
		for(int day = 1; day <= numDaysWorked; ++day)
		{
			if(numDaysWorked >= 1)
			{
				System.out.println("Day: " + day + "\t" + salary + " cents");
				
				salary *= 2.0;
				total += salary;
			}
			else
			{
				System.out.println("Enter a number greater than 0");
			}
		}
		System.out.printf("Total: $%.2f", (total / 2.0));
		
	}

}
