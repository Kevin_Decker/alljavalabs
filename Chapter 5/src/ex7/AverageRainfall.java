package ex7;

import java.util.Scanner;

public class AverageRainfall {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		double totalRainfall=0, avgRain=0;
		
		System.out.println("Enter number of years: ");
		int years = keyboard.nextInt();
		
		while(years < 1)
		{
			System.out.println("Can't have a negative. Enter again: ");
			years = keyboard.nextInt();
		}
		
		for(int y = 0; y < years; ++y)
		{
			for(int m = 0; m < 12; ++m)
			{
				System.out.println("Enter inches of rainfall for month " + (m+1));
				double rainfall = keyboard.nextDouble();
				while(rainfall >= 0)
				{
					totalRainfall += rainfall;
					avgRain = totalRainfall / (years * 12);
					break;
				}
			}
		}
		System.out.println("Number of Months: " + years * 12);
		System.out.println("Total Rainfall: " + totalRainfall);
		System.out.printf("Average Rainfall per Month: %.2f", avgRain);
	}

}
