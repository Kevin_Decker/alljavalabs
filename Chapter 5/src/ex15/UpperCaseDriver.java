package ex15;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class UpperCaseDriver {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the name of the file: (contents.txt)");
		String filename = keyboard.nextLine();
		
		File file = new File(filename);
		
		Scanner fileScan = new Scanner(file);
		PrintWriter fileWrite = new PrintWriter("contentsToUpperCase.txt");
		
		UpperCaseFile f1 = new UpperCaseFile(fileScan, fileWrite);
		
		f1.fileConversion();
	}

}
