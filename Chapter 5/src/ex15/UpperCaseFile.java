package ex15;

import java.io.PrintWriter;
import java.util.Scanner;

public class UpperCaseFile {

	private Scanner file1;
	private PrintWriter file2;
	
	//constructor
	public UpperCaseFile(Scanner file1, PrintWriter file2)
	{
		this.file1 = file1;
		this.file2 = file2;
	}
	
	//method to convert characters to upper case
	public void fileConversion()
	{
		while(file1.hasNext())
		{
			file2.println(file1.nextLine().toUpperCase());
		}
		System.out.println("File Converted.");
		file2.close();
		file1.close();
	}
	
}
