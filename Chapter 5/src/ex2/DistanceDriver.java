package ex2;

import java.util.Scanner;

public class DistanceDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		double speed=0, time=0;
		
		System.out.println("Enter speed of vehicle: ");
		speed = keyboard.nextDouble();
		
		while(speed < 0)
		{
			System.out.println("Enter speed of vehicle: ");
			speed = keyboard.nextDouble();	
		}
		
		System.out.println("Enter hours traveled: ");
		time = keyboard.nextDouble();
		
		while(time < 1)
		{
			System.out.println("Enter hours traveled: ");
			time = keyboard.nextDouble();
		}

		DistanceTraveled distance1 = new DistanceTraveled();
		distance1.setSpeed(speed);

		for(int hour = 1; hour <= time; ++hour)
		{
			distance1.setHours(hour);
			System.out.println(hour + "\t" + distance1.getDistance());
			
		}	
	}
}
