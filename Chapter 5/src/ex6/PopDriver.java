package ex6;

import java.util.Scanner;

public class PopDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);

		System.out.println("What is the starting population?");
		int pop = keyboard.nextInt();
		System.out.println("Avergae daily increase (percentage)?");
		double increase = keyboard.nextDouble();
		System.out.println("What is the number of days they will multiply?");
		int days = keyboard.nextInt();
		
		Population pop1 = new Population();
		pop1.setStartingOrganisms(pop);
		pop1.setAvgPopIncrease(increase);
		pop1.setNumDaysMultiply(days);
		
		if(pop < 2 || increase < 0 || days < 1)
		{
			System.out.println("Error");
		}
		else
		{
			pop1.getPopulation();
		}
		
		
	}

}
