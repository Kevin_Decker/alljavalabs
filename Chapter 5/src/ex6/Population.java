package ex6;

public class Population {

	private double startingOrganisms;
	private double avgPopIncrease;
	private int numDaysMultiply;
	
	
	//method that uses loop to display the size of the population for each day
	public double getStartingOrganisms() {
		return startingOrganisms;
	}

	public void setStartingOrganisms(double startingOrganisms) {
		this.startingOrganisms = startingOrganisms;
	}

	public double getAvgPopIncrease() {
		return avgPopIncrease;
	}

	public void setAvgPopIncrease(double avgPopIncrease) {
		this.avgPopIncrease = avgPopIncrease;
	}

	public int getNumDaysMultiply() {
		return numDaysMultiply;
	}

	public void setNumDaysMultiply(int numDaysMultiply) {
		this.numDaysMultiply = numDaysMultiply;
	}


	public void getPopulation()
	{
		for(int i = 1; i <= numDaysMultiply; ++i)
		{
			startingOrganisms = startingOrganisms + (startingOrganisms * avgPopIncrease);
			 System.out.printf("On Day %d at a growth rate of %.2f, you'll have %.2f organisms\n", i, avgPopIncrease, startingOrganisms);
		}	
	}
	
}
