package ex1;

import java.util.Scanner;

public class SumOfNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a positive integer: ");
		int nums=keyboard.nextInt(), sum=0;
		
		int x=0;
		do {
			sum += x;
			++x;
		}
		while(x <= nums);
		
		/*for(int x = 0; x <= nums; ++x)
		{
			sum += x;
		}*/
		
		System.out.printf("Sum: %d", sum);
		
	}

}
