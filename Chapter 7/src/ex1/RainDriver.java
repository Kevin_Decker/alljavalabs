package ex1;
import java.util.Scanner;

public class RainDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		double[] rain = new double[12];
		
		Rainfall rainfall = null;
		
		for(int x = 0; x < rain.length; ++x)
		{
			System.out.println("Enter amount of rain for month " + (x+1));
			rain[x] = keyboard.nextDouble();
			
			while(rain[x] < 0.0)
			{
				System.out.println("Can't be negative. Re enter");
				rain[x] = keyboard.nextDouble();
			}
			
			rainfall = new Rainfall(rain);
		}
		
		System.out.println("Total rainfall this year: " + rainfall.getTotalRainfall());
		System.out.println("Average rainfall this year: " + rainfall.getAverageRainfall());
		System.out.println("Month with highest rainfall: " + rainfall.getMonthWithMostRain());
		System.out.println("Month with lowest rainfall: " + rainfall.getMonthWithLeastRain());
		
	}

}
