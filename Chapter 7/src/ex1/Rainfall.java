package ex1;

public class Rainfall {

	private double months[] = new double[12];
	
	//constructor
	public Rainfall(double m[])
	{
		for(int i = 0; i < m.length; ++i)
		{
			months[i] = m[i];
		}
	}
	
	//method that returns the total rainfall for the year
	public double getTotalRainfall()
	{
		double total = 0.0;
		for(int x = 0; x < months.length; ++x)
		{
			total += months[x];
		}
		return total;
	}
	
	//method that returns the average monthly rainfall
	public double getAverageRainfall()
	{
		double avg = 0;
		for(int x = 0; x < months.length; ++x)
		{
			avg += months[x] / months.length;
		}
		return avg;
	}
	
	//method that returns the month with the most rain
	public double getMonthWithMostRain()
	{
		double high = 0;
		for(int x = 0; x < months.length; ++x)
		{
			if(months[x] > high)
			{
				high = months[x];
			}
		}
		return high;
	}
	
	//method that returns the month with the least rain
	public double getMonthWithLeastRain()
	{
		double low = months[0];
		for(int x = 0; x < months.length; ++x)
		{
			if(months[x] < low)
			{
				low = months[x];
			}
		}
		return low;
	}
	
}






