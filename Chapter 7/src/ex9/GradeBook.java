package ex9;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class GradeBook {

	private String[] studentNames = new String[5];
	private char[] studentLetterGrades = new char[5];
	//------------------------------------------
	ArrayList<Double> student1Scores = new ArrayList<Double>();
	ArrayList<Double> student2Scores = new ArrayList<Double>();
	ArrayList<Double> student3Scores = new ArrayList<Double>();
	ArrayList<Double> student4Scores = new ArrayList<Double>();
	ArrayList<Double> student5Scores = new ArrayList<Double>();
	
	//mutators
	public void setStudent1Scores(double[] scores)
	{
		for(int i=0;i<4;++i)
		{
			student1Scores.add(scores[i]);
		}
	}
	public void setStudent2Scores(double[] scores)
	{
		for(int i = 0; i < 4; ++i)
		{
			student2Scores.add(scores[i]);
		}
	}
	public void setStudent3Scores(double[] scores)
	{
		for(int i = 0; i < 4; ++i)
		{
			student3Scores.add(scores[i]);
		}
	}
	public void setStudent4Scores(double[] scores)
	{
		for(int i = 0; i < 4; ++i)
		{
			student4Scores.add(scores[i]);
		}
	}
	public void setStudent5Scores(double[] scores)
	{
		for(int i = 0; i < 4; ++i)
		{
			student5Scores.add(scores[i]);
		}
	}
	
	//mutator for student names
	public void setStudentNames(String[] names)
	{
		for(int i = 0; i < studentNames.length; ++i)
		{
			studentNames[i] = names[i];
		}
	}
	
	//method that returns a specific student's name
	public String getStudentName(String names)
	{
		for(int x = 0; x < studentNames.length; ++x)
		{
			if(names.equals(studentNames[x]))
			{
				return studentNames[x];
			}
		}
		return names;
	}
	
	//method that returns a specific student's average test score
	public double getStudentAverageScore(String name)
	{
		double avg = 0;
		double sum = 0;
		double low = 0;
		if(name.equals(studentNames[0]))
		{
			//gets the lowest score in array list
			low = student1Scores.get(0);
			for(int x = 0; x < student1Scores.size(); ++x)
			{
				if(student1Scores.get(x) < low)
				{
					low = student1Scores.get(x);
				}
			}
			//removes lowest score in array list
			student1Scores.remove(low);
			
			//gets average of values in array list
			for(int i = 0; i < student1Scores.size(); ++i)
			{
				sum += student1Scores.get(i);
			}
			return sum / student1Scores.size();
		}
		else if(name.equals(studentNames[1]))
		{
			low = student2Scores.get(0);
			for(int x = 0; x < student2Scores.size(); ++x)
			{
				if(student2Scores.get(x) < low)
				{
					low = student2Scores.get(x);
				}
			}
			//removes lowest score in array list
			student2Scores.remove(low);
			
			for(int i = 0; i < student2Scores.size(); ++i)
			{
				sum += student2Scores.get(i);
			}
			return sum / student2Scores.size();
		}
		else if(name.equals(studentNames[2]))
		{
			low = student3Scores.get(0);
			for(int x = 0; x < student3Scores.size(); ++x)
			{
				if(student3Scores.get(x) < low)
				{
					low = student3Scores.get(x);
				}
			}
			//removes lowest score in array list
			student3Scores.remove(low);
			
			for(int i = 0; i < student3Scores.size(); ++i)
			{
				sum += student3Scores.get(i);
			}
			return sum / student3Scores.size();
		}
		else if(name.equals(studentNames[3]))
		{
			low = student4Scores.get(0);
			for(int x = 0; x < student4Scores.size(); ++x)
			{
				if(student4Scores.get(x) < low)
				{
					low = student4Scores.get(x);
				}
			}
			//removes lowest score in array list
			student4Scores.remove(low);
			
			for(int i = 0; i < student4Scores.size(); ++i)
			{
				sum += student4Scores.get(i);
			}
			return sum / student4Scores.size();
		}
		else 
		{
			low = student5Scores.get(0);
			for(int x = 0; x < student5Scores.size(); ++x)
			{
				if(student5Scores.get(x) < low)
				{
					low = student5Scores.get(x);
				}
			}
			//removes lowest score in array list
			student5Scores.remove(low);
			
			for(int i = 0; i < student5Scores.size(); ++i)
			{
				sum += student5Scores.get(i);
			}
			return sum / student5Scores.size();
		}
	}
	
	//method that returns letter grade based on average
	public char getStudentLetterGrade(double average)
	{
		char letterGrade;
		
		if(average < 60)
		{
			letterGrade = 'F';
		}
		else if(average < 70)
		{
			letterGrade = 'D';
		}
		else if(average < 80)
		{
			letterGrade = 'C';
		}
		else if(average < 90)
		{
			letterGrade = 'B';
		}
		else 
		{
			letterGrade = 'A';
		}
		
		return letterGrade;
	}
}
