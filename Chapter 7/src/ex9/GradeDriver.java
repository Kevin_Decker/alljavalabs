package ex9;
import java.util.Scanner;

public class GradeDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		GradeBook gradeBook = new GradeBook();
		
		String[] studentNames = new String[5];
		double[] testScores = new double[4];
		
		for(int x = 0; x < studentNames.length; ++x)
		{
			System.out.println("Enter student name: ");
			studentNames[x] = keyboard.nextLine();
			
			
			for(int i = 0; i < testScores.length; ++i)
			{
				System.out.println("Enter test score " + (i+1));
				testScores[i] = keyboard.nextDouble();
				
				//validation
				while(testScores[i] < 0 && testScores[i] > 100)
				{
					System.out.println("Error: Score can't be below 0 or above 100.");
					testScores[i] = keyboard.nextDouble();
				}
				keyboard.nextLine();
			}
			switch(x)
			{
			case 0:
				gradeBook.setStudent1Scores(testScores);
				break;
			case 1:
				gradeBook.setStudent2Scores(testScores);
				break;
			case 2:
				gradeBook.setStudent3Scores(testScores);
				break;
			case 3:
				gradeBook.setStudent4Scores(testScores);
				break;
			case 4:
				gradeBook.setStudent5Scores(testScores);
				break;
			default:
				break;
			}
			
		}
		
		gradeBook.setStudentNames(studentNames);
		
		for(int x = 0; x < studentNames.length; ++x)
		{
			System.out.println("Name: " + gradeBook.getStudentName(studentNames[x]));
			System.out.println("Average: " + gradeBook.getStudentAverageScore(studentNames[x]));
			System.out.println("Grade: " + gradeBook.getStudentLetterGrade(gradeBook.getStudentAverageScore(studentNames[x])));
			System.out.println();
		}

	}
}
