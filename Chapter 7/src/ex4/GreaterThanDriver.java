package ex4;

import java.util.Random;
import java.util.Scanner;

public class GreaterThanDriver {

	public static void main(String[] args) {
		
		Random random = new Random();
		int[] intArray = new int[50];
		
		// this loop creates an array of 50 random integers
		for(int x = 0; x < intArray.length; ++x)
		{
			intArray[x] = random.nextInt(50) + 1;
		}
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a number between 1-50: ");
		System.out.println("Result will display the random numbers greater than your number. ");
		int n = keyboard.nextInt();
		
		int[] numsReturned = greaterNums(intArray, n);
		
		for(int x = 0; x < numsReturned.length; ++x)
		{
			if(numsReturned[x] != 0)
			{
				System.out.println(numsReturned[x]);
			}
		}
	}
	
	// static method that determines if a number is greater than
	public static int[] greaterNums(int[] intArray, int num)
	{
		int[] largerNumbers = new int[intArray.length];
		int greaterNumsIndex = 0;
		
		//for loop and if determine if numbers in the array are greater than the number entered
		for(int x = 0; x < intArray.length; ++x)
		{
			if(intArray[x] > num)
			{
				largerNumbers[greaterNumsIndex] = intArray[x];
				greaterNumsIndex += 1;
			}
		}
		return largerNumbers;
	}

}
