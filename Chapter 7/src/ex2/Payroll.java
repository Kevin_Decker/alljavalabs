package ex2;

public class Payroll {

	private int[] employeeId = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489};
	private int[] hours = new int[7];
	private double[] payRate = new double[7];
	private double[] wages = new double[7];
	
	//constructor
	public Payroll(int[] hours, double[] payRate) {
		super();
		this.hours = hours;
		this.payRate = payRate;
		
		getGrossPay();
	}
	
	public Payroll()
	{
		
	}
	
	//getters and setters
	public int[] getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int[] employeeId) {
		this.employeeId = employeeId;
	}
	public int[] getHours() {
		return hours;
	}
	public void setHours(int[] hours) {
		this.hours = hours;
	}
	public double[] getPayRate() {
		return payRate;
	}
	public void setPayRate(double[] payRate) {
		this.payRate = payRate;
	}
	public double[] getWages() {
		return wages;
	}
	public void setWages(double[] wages) {
		this.wages = wages;
	}
	
	//method that accepts an employeeId as an argument and returns the employee's gross pay
	public void getGrossPay()
	{
		for(int x = 0; x < employeeId.length; ++x)
		{
			wages[x] = hours[x] * payRate[x];
		}
	}
	
	public double getWagesByEmployee(int id)
	{
		for(int x = 0; x < employeeId.length; ++x)
		{
			if(employeeId[x] == id)
			{
				return wages[x];
			}
		}
		return -1;
	}
	
	
}
