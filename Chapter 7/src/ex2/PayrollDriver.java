package ex2;

import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		Payroll payroll = new Payroll();
		
		int[] employees = payroll.getEmployeeId();
		int[] hours = new int[7];
		double[] payRate = new double[7];
		
		for(int x = 0; x < employees.length; ++x)
		{
			System.out.println("Employee ID: " + employees[x]);
			System.out.println("Enter hours: ");
			hours[x] = keyboard.nextInt();
			
			System.out.println("Enter pay rate: ");
			payRate[x] = keyboard.nextDouble();
			
			while(hours[x] < 0)
			{
				System.out.println("Error: No negative values for hours");
				hours[x] = keyboard.nextInt();
			}
			
			while(payRate[x] < 6.00)
			{
				System.out.println("Error: PayRate can't be less than 6.00");
				payRate[x] = keyboard.nextDouble();
			}
			
			payroll = new Payroll(hours, payRate);
			
		}
		
		for(int x = 0; x < employees.length; ++x)
		{
			System.out.println("Employee ID: " + employees[x] + "\n" + "Gross Pay: $" + payroll.getWagesByEmployee(employees[x]) + "\n");
		}
	}
}
