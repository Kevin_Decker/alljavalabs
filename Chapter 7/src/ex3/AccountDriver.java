package ex3;
import java.util.Scanner;

public class AccountDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		ChargeAccount chargeAccount = new ChargeAccount();

		for(;;)
		{
			System.out.println("Enter your account number: (Enter -99 to Quit)");
			int account = keyboard.nextInt();
			
			chargeAccount.accountIsValid(account);
			
			if(account == (-99))
			{
				break;
			}
		}
	}
}
