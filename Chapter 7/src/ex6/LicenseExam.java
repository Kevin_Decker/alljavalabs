package ex6;

public class LicenseExam {

	private char[] correctAnswers = {'B', 'D', 'A', 'A', 'C', 'A', 'B', 'A', 'C', 'D', 'B', 'C', 'D', 'A', 'D', 'C', 'C', 'B', 'D', 'A'};
	
	//getter and setter for array
	public char[] getCorrectAnswers() {
		return correctAnswers;
	}
	public void setCorrectAnswers(char[] correctAnswers) {
		this.correctAnswers = correctAnswers;
	}

	//method that determines if the student passed
	public boolean passed(char[] answers)
	{
		double percentCorrect = (totalCorrectAnswers(answers) / 20.00) * 100.00;
		if(percentCorrect > 75.00)
		{
			System.out.println("You Passed! with a score of " + percentCorrect);
			return true;
		}
		else
		{
			System.out.println("You failed, with a score of " + percentCorrect);
			return false;
		}
	}
	
	//method that returns the total correct answers
	public int totalCorrectAnswers(char[] answers)
	{
		int correct = 0;
		for(int x = 0; x < answers.length; ++x)
		{
			if(answers[x] == correctAnswers[x])
			{
				correct++;
			}
		}
		return correct;
	}
	
	//method to return the total incorrect answers
	public int incorrectAnswers(char[] answers)
	{
		int incorrect = 0;
		for(int x = 0; x < answers.length; ++x)
		{
			if(answers[x] != correctAnswers[x])
			{
				incorrect++;
			}
		}
		return incorrect;
	}
	
	//method to return an array of the answers that were incorrect
	public int[] questionsMissed(char[] answers)
	{
		int[] missedQ = new int[answers.length];
		for(int x = 0; x < answers.length; ++x)
		{
			if(answers[x] != correctAnswers[x])
			{
				missedQ[x] = (x+1);
			}
		}
		return missedQ;
	}
	
	//method to print the questions missed
	public void printMissedQ(int[] missedQ)
	{
		for(int x = 0; x < missedQ.length; ++x)
		{
			if(missedQ[x] != 0)
			{
				System.out.print(missedQ[x] + ", ");
			}
		}
	}
}
