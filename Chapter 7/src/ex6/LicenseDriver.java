package ex6;
import java.util.Scanner;

public class LicenseDriver {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		LicenseExam licenseExam = new LicenseExam();
		char[] answers = new char[20];
		int[] missedQuestions = new int[20];
		for(int x = 0; x < answers.length; ++x)
		{
			System.out.println("Enter answer for question " + (x+1));
			answers[x] += keyboard.next().charAt(0);
			
			missedQuestions = licenseExam.questionsMissed(answers);
			
			while(answers[x] != 'A' && answers[x] != 'B' && answers[x] != 'C' && answers[x] != 'D')
			{
				System.out.println("Error: Answer has to be A, B, C, or D");
				answers[x] = keyboard.next().charAt(0);
			}
		}
		
		System.out.println(licenseExam.passed(answers));
		System.out.println("Correct: " + licenseExam.totalCorrectAnswers(answers));
		System.out.println("Incorrect: " + licenseExam.incorrectAnswers(answers));
		System.out.print("Missed Questions: ");
		licenseExam.printMissedQ(missedQuestions);

	}

}
