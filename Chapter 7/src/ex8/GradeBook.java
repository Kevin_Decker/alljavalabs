package ex8;

import java.util.Arrays;

public class GradeBook {

	private String[] studentNames = new String[5];
	private char[] studentLetterGrades = new char[5];
	//------------------------------------------
	private double[] student1Scores = new double[4];
	private double[] student2Scores = new double[4];
	private double[] student3Scores = new double[4];
	private double[] student4Scores = new double[4];
	private double[] student5Scores = new double[4];
	
	//mutators
	public void setStudent1Scores(double[] scores)
	{
		for(int i=0;i<student1Scores.length;++i)
		{
			student1Scores[i] = scores[i];
		}
	}
	public void setStudent2Scores(double[] scores)
	{
		for(int i = 0; i < student2Scores.length; ++i)
		{
			student2Scores[i] = scores[i];
		}
	}
	public void setStudent3Scores(double[] scores)
	{
		for(int i = 0; i < student3Scores.length; ++i)
		{
			student3Scores[i] = scores[i];
		}
	}
	public void setStudent4Scores(double[] scores)
	{
		for(int i = 0; i < student4Scores.length; ++i)
		{
			student4Scores[i] = scores[i];
		}
	}
	public void setStudent5Scores(double[] scores)
	{
		for(int i = 0; i < student5Scores.length; ++i)
		{
			student5Scores[i] = scores[i];
		}
	}
	public void setStudentNames(String[] names)
	{
		for(int i = 0; i < studentNames.length; ++i)
		{
			studentNames[i] = names[i];
		}
	}
	
	//method that returns a specific student's name
	public String getStudentName(String names)
	{
		for(int x = 0; x < studentNames.length; ++x)
		{
			if(names.equals(studentNames[x]))
			{
				return studentNames[x];
			}
		}
		return names;
	}
	
	//method that returns a specific student's average test score
	public double getStudentAverageScore(String name)
	{
		double avg = 0;
		double sum = 0;
		if(name.equals(studentNames[0]))
		{
			avg = Arrays.stream(student1Scores).average().getAsDouble();
			return avg;
		}
		else if(name.equals(studentNames[1]))
		{
			avg = Arrays.stream(student2Scores).average().getAsDouble();
			return avg;
		}
		else if(name.equals(studentNames[2]))
		{
			avg = 0;
			avg = Arrays.stream(student3Scores).average().getAsDouble();
			return avg;
		}
		else if(name.equals(studentNames[3]))
		{
			avg = 0;
			avg = Arrays.stream(student4Scores).average().getAsDouble();
			return avg;
		}
		else 
		{
			avg = 0;
			avg = Arrays.stream(student5Scores).average().getAsDouble();
			return avg;
		}
	}
	
	//method that returns letter grade based on average
	public char getStudentLetterGrade(double average)
	{
		char letterGrade;
		
		if(average < 60)
		{
			letterGrade = 'F';
		}
		else if(average < 70)
		{
			letterGrade = 'D';
		}
		else if(average < 80)
		{
			letterGrade = 'C';
		}
		else if(average < 90)
		{
			letterGrade = 'B';
		}
		else 
		{
			letterGrade = 'A';
		}
		
		return letterGrade;
	}
}
