package ex5;
import java.io.*;
import java.util.Scanner;

public class ChargeAccount {

	private int[] accountNumbers;
	
	//constructor
	public ChargeAccount(String filename) throws FileNotFoundException
	{
		File accountsFile = new File(filename);
		Scanner readFile = new Scanner(accountsFile);
		accountNumbers = new int[getLines(filename)];
		
		int count = 0;
		
		while(readFile.hasNext())
		{
			accountNumbers[count] = readFile.nextInt();
			count += 1;
		}
		
		readFile.close();
	}
	
	//method to get all the lines in the text file
	public int getLines(String filename) throws FileNotFoundException
	{
		File accountsFile = new File(filename);
		Scanner readFile = new Scanner(accountsFile);
		
		int lines = 0;
		
		while(readFile.hasNext())
		{
			readFile.nextInt();
			lines += 1;
		}
		
		readFile.close();
		return lines;
	}
	
	//method 
	public boolean accountIsValid(int accountNum)
	{
		for(int x = 0; x < accountNumbers.length; ++x)
		{
			if(accountNumbers[x] == accountNum)
			{
				System.out.println("Valid Account Number");
				return true;
			}
		}
		System.out.println("NOT a valid Account Number");
		return false;
	}
}
