package ex5;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AccountDriver {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner keyboard = new Scanner(System.in);
		
		ChargeAccount chargeAccount = new ChargeAccount("AccountNumbers.txt");

		for(;;)
		{
			System.out.println("Enter your account number: (Enter -99 to Quit)");
			int account = keyboard.nextInt();
			
			chargeAccount.accountIsValid(account);
			
			if(account == (-99))
			{
				break;
			}
		}
	}
}
