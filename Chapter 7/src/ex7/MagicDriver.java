package ex7;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MagicDriver {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner keyboard = new Scanner(System.in);
		
		Magic8Ball magic8Ball = new Magic8Ball("8_ball_responses.txt");

		for(;;)
		{
			System.out.println("Ask the Magic 8 Ball a question:");
			String question = keyboard.nextLine();
			
			magic8Ball.displayResponse();
		}
	}

}
