package ex7;
import java.util.Random;
import java.util.Scanner;
import java.io.*;

public class Magic8Ball {

	private String[] responses;
	
	//constructor
	public Magic8Ball(String filename) throws FileNotFoundException 
	{
		File responsesFile = new File(filename);
		Scanner readFile = new Scanner(responsesFile);
		
		responses = new String[getLines(filename)];
		int count = 0;
		while(readFile.hasNext())
		{
			responses[count] = readFile.nextLine();
			count += 1;
		}

		readFile.close();
	}
	
	//getter and setter
	public String[] getResponses() {
		return responses;
	}
	public void setResponses(String[] responses) {
		this.responses = responses;
	}

	//method that reads the lines of text
	public int getLines(String filename) throws FileNotFoundException
	{
		File responsesFile = new File(filename);
		Scanner readFile = new Scanner(responsesFile);
		int lines = 0;
		
		while(readFile.hasNext())
		{
			readFile.nextLine();
			lines += 1;
		}
		readFile.close();
		return lines;
	}
	
	//method to display the response
	public void displayResponse()
	{

		Random random = new Random();
		int randomResponse = random.nextInt(12);
		System.out.println(responses[randomResponse]);

	}
}
