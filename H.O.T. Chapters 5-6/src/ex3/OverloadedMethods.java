package ex3;

public class OverloadedMethods {

	public static double addNumbers(double num1, double num2)
	{
		return num1 + num2;
	}
	
	public static double addNumbers(double num1, double num2, double num3)
	{
		return num1 + num2 + num3;
	}
	
}
