package ex1;
import java.util.Scanner;
import java.io.*;

public class Sales {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner keyboard = new Scanner(System.in);
		double total = 0;
		double sales[] = new double[5];
		
		for(int x = 0; x < sales.length; ++x)
		{
			System.out.println("Enter sales for day " + (x+1));
			sales[x] = keyboard.nextDouble();

			while(sales[x] < 0) 
			{
				System.out.println("Entry can't be a negative. Re-enter sales");
				sales[x] = keyboard.nextDouble();
			}
			
			total += sales[x];
		}
		File file = new File("C:/Users/kdeck/WeeklySales.txt");
		PrintWriter weeklySales = new PrintWriter(file);
		weeklySales.println("WEEKLY SALES");
		weeklySales.println("Day 1 Sales: $" + sales[0]);
		weeklySales.println("Day 2 Sales: $" + sales[1]);
		weeklySales.println("Day 3 Sales: $" + sales[2]);
		weeklySales.println("Day 4 Sales: $" + sales[3]);
		weeklySales.println("Day 5 Sales: $" + sales[4]);
		weeklySales.println("Total Sales: $" + total);
		
		weeklySales.close();
		System.out.println("Saved Successfully.");
	}

}
