package ex2;

public class Book {

	private String name;
	private String pubDate;
	private String author;
	
	//constructor
	public Book(String name, String pubDate, String author) {
		super();
		this.name = name;
		this.pubDate = pubDate;
		this.author = author;
	}
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	
	
}
