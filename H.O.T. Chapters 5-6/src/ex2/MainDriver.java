package ex2;

public class MainDriver {

	public static void main(String[] args) {
		
		Book book = new Book("Adventures of Tom Sawyer", "June 1876", "Mark Twain");
		Library library = new Library(book, "St. Louis Public Library");
		
		System.out.println(library);
	}

}
