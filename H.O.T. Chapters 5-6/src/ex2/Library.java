package ex2;

public class Library {

	private Book book;
	private String name;
	
	//constructor
	public Library(Book book, String name) {
		super();
		this.book = book;
		this.name = name;
	}
	
	//Getters and Setters
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	//overriding the toString method
	public String toString()
	{
		return String.format("Library: %s\nBook: %s\nPublish Date: %s\nAuthor: %s", name, book.getName(), book.getPubDate(), book.getAuthor());
	}
}
