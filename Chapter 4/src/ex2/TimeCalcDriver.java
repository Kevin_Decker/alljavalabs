package ex2;

import java.util.Scanner;

public class TimeCalcDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter number of seconds: ");
		int seconds = keyboard.nextInt();
		
		if(seconds >= 86400)
		{
			int day = seconds / 86400;
			int hour = (seconds / 3600) % 24;
			int minute = (seconds / 60) % 60;
			int mod = seconds % 60;
			System.out.printf("%d is %d days, %d hours, %d minutes, %d seconds", seconds, day, hour, minute, mod);
		}
		else if(seconds >= 3600)
		{
			int hour = seconds / 3600;
			int minute = (seconds / 60) % 60;
			int mod =  seconds % 60;
			System.out.printf("%d is %d hours and %d minutes, and %d seconds.", seconds, hour, minute, mod);
			
		}
		else if(seconds >= 60)
		{
			int minute = seconds / 60;
			int mod = seconds % 60 ;
			System.out.printf("%d is %d minutes and %d seconds.", seconds, minute, mod);
		}
		else
		{
			System.out.printf("%d", seconds);
		}
	}
}
