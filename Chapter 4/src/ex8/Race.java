package ex8;

public class Race {

	private String name1;
	private String name2;
	private String name3;
	private int minutes1;
	private int minutes2;
	private int minutes3;
	
	//constructor
	public Race(int min1, int min2, int min3, String n1, String n2, String n3)
	{
		minutes1 = min1;
		minutes2 = min2;
		minutes3 = min3;
		name1 = n1;
		name2 = n2;
		name3 = n3;
	}
	
	//mutators
	public void setName1(String name)
	{
		name1 = name;
	}
	
	public void setName2(String name)
	{
		name2 = name;
	}
	
	public void setName3(String name)
	{
		name3 = name;
	}
	
	public void setTime1(int race)
	{
		minutes1 = race;
	}
	
	public void setTime2(int race)
	{
		minutes2 = race;
	}
	
	public void setTime3(int race)
	{
		minutes3 =race;
	}
	
	//accessors
	public String getName1()
	{
		return name1;
	}
	
	public String getName2()
	{
		return name2;
	}
	
	public String getName3()
	{
		return name3;
	}
	
	public int getTime1()
	{
		return minutes1;
	}
	
	public int getTime2()
	{
		return minutes2;
	}
	
	public int getTime3()
	{
		return minutes3;
	}
	
	//method to return 1st place, 2nd place, 3rd place
	public void getRacePlacement()
	{
		if(minutes1 < minutes2 && minutes1 < minutes3)
		{
			System.out.println(name1 + " got first place!");
			if(minutes2 < minutes3)
			{
				System.out.println(name2 + " got second place, and " + name3 + " got third place.");
			}
			else
			{
				System.out.println(name3 + " got second place, and " + name2 + " got third place.");
			}
		}
		else if(minutes2 < minutes1 && minutes2 < minutes3)
		{
			System.out.println(name2 + " got first place!");
			if(minutes1 < minutes3)
			{
				System.out.println(name1 + " got second place, and " + name3 + " got third place.");
			}
			else
			{
				System.out.println(name3 + " got second place, and " + name1 + " got third place.");
			}
		}
		else if(minutes3 < minutes1 && minutes3 < minutes2)
		{
			System.out.println(name3 + " got first place!");
			if(minutes1 < minutes2)
			{
				System.out.println(name1 + " got second place, and " + name2 + " got third place.");
			}
			else
			{
				System.out.println(name2 + " got second place, and " + name1 + " got third place.");
			}
		}
		
	}
	
}
