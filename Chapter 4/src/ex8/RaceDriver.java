package ex8;

import java.util.Scanner;

public class RaceDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter name 1: ");
		String racer1 = userInput.nextLine();
		System.out.println("Enter time 1: ");
		int racerTime1 = userInput.nextInt();
		
		String buffer = userInput.nextLine();
		
		System.out.println("Enter name 2: ");
		String racer2 = userInput.nextLine();
		System.out.println("Enter time 2: ");
		int racerTime2 = userInput.nextInt();		
		
		String buffer2 = userInput.nextLine();
		
		System.out.println("Enter name 3: ");
		String racer3 = userInput.nextLine();
		System.out.println("Enter time 3: ");
		int racerTime3 = userInput.nextInt();
		
		Race race1 = new Race(racerTime1, racerTime2, racerTime3, racer1, racer2, racer3);
		
		race1.getRacePlacement();
	}

}
