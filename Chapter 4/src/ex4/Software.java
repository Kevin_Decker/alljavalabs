package ex4;

public class Software {

	private int numUnitsSold;
	
	//constructor
	public Software(int units)
	{
		numUnitsSold = units;
	}
	
	//mutator
	public void setUnits(int u)
	{
		numUnitsSold = u;
	}
	
	//accessor
	public int getUnits()
	{
		return numUnitsSold;
	}
	
	//method that returns the total cost of purchase
	public double calcTotal()
	{
		if(numUnitsSold >= 10 && numUnitsSold <= 19)
		{
			double unitTotal = numUnitsSold * 99;
			double discount = unitTotal - (.2 * unitTotal);
			return discount;
		}
		else if(numUnitsSold >= 20 && numUnitsSold <= 49)
		{
			double unitTotal = numUnitsSold * 99;
			double discount = unitTotal - (.3 * unitTotal);
			return discount;
		}
		else if(numUnitsSold >= 50 && numUnitsSold <= 99)
		{
			double unitTotal = numUnitsSold * 99;
			double discount = unitTotal - (.4 * unitTotal);
			return discount;
		}
		else if(numUnitsSold >= 100)
		{
			double unitTotal = numUnitsSold * 99;
			double discount = unitTotal - (.5 * unitTotal);
			return discount;
		}
		else 
		{
			return  numUnitsSold * 99;
		}
		
	}
}

