package ex4;

import java.util.Scanner;

public class SoftwareDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Each package retails at $99. Enter a quantity: ");
		int quantity = keyboard.nextInt();
		
		Software order = new Software(quantity);
		
		System.out.printf("Quantity of %d will cost $%,.2f", quantity, order.calcTotal());
	}

}
