package ex17;

import java.util.Scanner;

public class HotDogDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner userInput = new Scanner(System.in);
		
		System.out.println("How many people will be attending the cookout?");
		int people = userInput.nextInt();
		System.out.println("Enter number of hot dogs for each person: ");
		int hotdogs = userInput.nextInt();
		
		HotDog hd = new HotDog(hotdogs, people);
		
		hd.calcDogsToBuns();
	}

}
