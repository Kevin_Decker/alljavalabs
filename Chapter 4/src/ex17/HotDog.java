package ex17;

public class HotDog {

	private int hotDogs;
	private int people;
	
	//constructor
	public HotDog(int dogs, int peeps)
	{
		hotDogs = dogs;
		people = peeps;
	}
	
	//mutators
	public void setHotDogs(int d)
	{
		hotDogs = d;
	}
	
	public void setPeople(int p)
	{
		people = p;
	}
	
	//accessors
	public int getHotDogs()
	{
		return hotDogs;
	}
	
	public int getPeople()
	{
		return people;
	}
	
	//method that calculates hotdogs to buns, including leftover hotdogs and buns
	public void calcDogsToBuns()
	{
		final int QTY_PER_DOG_PACK = 10;
		final int QTY_PER_BUN_PACK = 8;
		
		int totalUnits = people * hotDogs;
		int hotDogPacks = totalUnits / QTY_PER_DOG_PACK;
		if(totalUnits % QTY_PER_DOG_PACK != 0)
		{
			hotDogPacks++;
		}
		int bunPacks = totalUnits / QTY_PER_BUN_PACK;
		if(totalUnits % QTY_PER_BUN_PACK != 0)
		{
			bunPacks++;
		}
		
		int totalDogs = QTY_PER_DOG_PACK * hotDogPacks;
		int totalBuns = QTY_PER_BUN_PACK * bunPacks;
		
		
		int leftoverDogs = totalDogs - totalUnits;
		int leftoverBuns = totalBuns - totalUnits;
		
		System.out.println("Hot dog packages: " + hotDogPacks);
		System.out.println("Bun packages: " + bunPacks);
		System.out.println("Hotdogs leftover: " + leftoverDogs);
		System.out.println("Buns leftover: " + leftoverBuns);
	}
	
}
