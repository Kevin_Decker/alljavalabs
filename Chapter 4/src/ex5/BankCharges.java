package ex5;

public class BankCharges {

	private double endBalance;
	private int numChecks;
	
	//constructor
	public BankCharges(int check)
	{
		numChecks = check;
	}
	
	//mutators
	public void setEndBalance(double bal)
	{
		endBalance = bal;
	}
	
	public void setNumChecks(int check)
	{
		numChecks = check;
	}
	
	//accessors
	public double getEndBalance()
	{
		return endBalance;
	}
	
	public int getNumChecks()
	{
		return numChecks;
	}
	
	//method to return the bank's service fees
	public double calcServiceFees()
	{
		int defaultCharge = 10;
		
		if(numChecks >= 60) 
		{
			double total = numChecks * .04;
			return total += defaultCharge;
			
		}
		else if(numChecks >= 40 && numChecks <= 59)
		{
			double total = numChecks * .06;
			return total += defaultCharge;
		}
		else if(numChecks >= 20 && numChecks <= 39)
		{
			double total = numChecks * .08;
			return total += defaultCharge;
		}
		else 
		{
			double total = numChecks * .10;
			return total += defaultCharge;
		}
	}
}
