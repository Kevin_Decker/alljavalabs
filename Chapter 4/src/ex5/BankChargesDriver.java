package ex5;

import java.util.Scanner;

public class BankChargesDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Bank charges $10 a month, which is reflected in Service Fees");
/*		System.out.println("Enter Account Balance: ");
		int balance = keyboard.nextInt();*/
		System.out.println("Enter number of checks: ");
		int checks = keyboard.nextInt();
		
		BankCharges bank = new BankCharges(checks);
		
		System.out.printf("Checks submitted: %d\nService Fees: $%,.2f", checks, bank.calcServiceFees());
	}

}
