package ex7;

public class FatGram {

	private double calories;
	private double fatGrams; 
	
	//constructor
	public FatGram(double cal, double fat)
	{
		calories = cal;
		fatGrams = fat;
	}
	
	//mutators
	public void setCalories(double calorie)
	{
		calories = calorie;
	}
	
	public void setFatGrams(double fatG)
	{
		fatGrams = fatG;
	}
	
	//accessors
	public double getCalories()
	{
		return calories;
	}
	
	public double getFatGrams()
	{
		return fatGrams;
	}
	
	//method that returns the percentage of calories that come from fat
	public void calcPercenntCalorie()
	{
		double calFromFat = fatGrams * 9;
		
		if(calFromFat <= 30)
		{
			double percent = calFromFat / calories;
			System.out.printf("%.2f calories from fat is %.2f percent from %.2f total calories.\nFood is low in fat!", calFromFat, percent, calories);
		}
		else 
		{
			if(calFromFat > calories)
			{
				System.out.println("Error: Calories from fat cannot be greater than total calories.");
			}
			else 
			{
				double percent = calFromFat / calories;
				System.out.printf("%.2f calories from fat is %.2f percent from %.2f total calories.", calFromFat, percent, calories);
			}
		}
	}
}
