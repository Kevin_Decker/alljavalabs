package ex6;

import java.util.Scanner;

public class ShippingDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter weight of package: ");
		double weight = keyboard.nextDouble();
		
		System.out.println("Enter miles shipped: ");
		int miles = keyboard.nextInt();
		
		ShippingCharges ship = new ShippingCharges(weight, miles);
		
		System.out.printf("Package Weight: %f\nMiles Shipped: %d\nShipping Charges: $%,.2f", weight, miles, ship.calcShipping());
		
	}

}
