package ex6;

public class ShippingCharges {

	private double packageWeight;
	private int miles;
	
	//constructor
	public ShippingCharges(double weight, int mile)
	{
		packageWeight = weight;
		miles = mile;
	}
	
	//mutator
	public void setWeight(double w)
	{
		packageWeight = w;
	}
	
	//accessor
	public double getWeight()
	{
		return packageWeight;
	}
	
	//method that returns shipping charge
	public double calcShipping()
	{
		if(packageWeight > 10) 
		{
			if(miles > 500)
			{
				double shipCharge = 9.6;
				return shipCharge;
			}
			else 
			{
				double shipCharge = 4.8;
				return shipCharge;
			}
		}
		else if(packageWeight >= 6 && packageWeight <= 10)
		{
			if(miles > 500)
			{
				double shipCharge = 7.4;
				return shipCharge;
			}
			else 
			{
				double shipCharge = 3.7;
				return shipCharge;
			}
		}
		else if(packageWeight > 2 && packageWeight <= 5)
		{
			if(miles > 500)
			{
				double shipCharge = 4.4;
				return shipCharge;
			}
			else 
			{
				double shipCharge = 2.2;
				return shipCharge;
			}
		}
		else 
		{
			if(miles > 500)
			{
				double shipCharge = 2.2;
				return shipCharge;
			}
			else 
			{
				double shipCharge = 1.1;
				return shipCharge;
			}
		}

	}
}
