package ex15;

public class BookClub {

	private int booksPurchased;
	
	//constructor
	public BookClub(int books) 
	{
		booksPurchased = books;
	}

	//mutator
	public void setBooksPurchased(int book)
	{
		booksPurchased = book;
	}
	
	//accessor
	public int getBooksPurchased()
	{
		return booksPurchased;
	}
	
	//method that returns number of points rewarded
	public void numOfPointsRewarded()
	{
		switch (booksPurchased) {
		case 0:
			System.out.println("0 points rewarded");
			break;
		case 1:
			System.out.println("5 points rewarded");
			break;
		case 2:
			System.out.println("15 points rewarded");
			break;
		case 3:
			System.out.println("30 points rewarded");
			break;
		case 4:
			System.out.println("60 points rewarded");
			break;
		default:
			System.out.println("60 points rewarded");
			break;
		}
	}
	
}
