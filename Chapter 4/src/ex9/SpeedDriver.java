package ex9;

import java.util.Scanner;

public class SpeedDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Press 1 to select Air\nPress 2 to select Water\nPress 3 to select Steel");
		System.out.println();
		int selection = keyboard.nextInt();
		
		if(selection == 1)
		{
			System.out.println("Speed in Air\nEnter a distance: ");
			double airSpeed = keyboard.nextDouble();
			
			SpeedOfSound s1 = new SpeedOfSound();
			s1.setDistance(airSpeed);
			
			System.out.printf("A distance of %.2f feet would take %.2f seconds.", airSpeed, s1.getSpeedInAir());
		}
		else if(selection == 2)
		{
			System.out.println("Speed in Water\nEnter a distance: ");
			double waterSpeed = keyboard.nextDouble();
			
			SpeedOfSound s2 = new SpeedOfSound();
			s2.setDistance(waterSpeed);
			
			System.out.printf("A distance of %.2f feet would take %.2f seconds.", waterSpeed, s2.getSpeedInWater());
		}
		else if(selection == 3)
		{
			System.out.println("Speed in Steel\nEnter a distance: ");
			double steelSpeed = keyboard.nextDouble();
			
			SpeedOfSound s3 = new SpeedOfSound();
			s3.setDistance(steelSpeed);
			
			System.out.printf("A distance of %.2f feet would take %.2f seconds.", steelSpeed, s3.getSpeedInASteel());
		}
		else 
		{
			System.out.println("Error: not a valid option");
		}
		
	}

}
