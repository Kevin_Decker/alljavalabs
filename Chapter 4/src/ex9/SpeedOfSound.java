package ex9;

public class SpeedOfSound {

	private double distance;

	//mutator
	public void setDistance(double dist)
	{
		distance = dist;
	}
	
	//accessor
	public double getDistance()
	{
		return distance;
	}
	
	//method to get speed in air
	public double getSpeedInAir()
	{
		double time = distance / 1100;
		return time;
	}
	
	//method to get speed in water
	public double getSpeedInWater()
	{
		double time = distance / 4900;
		return time;
	}
	
	//method to get speed in steel
	public double getSpeedInASteel()
	{
		double time = distance / 16400;
		return time;
	}
}
