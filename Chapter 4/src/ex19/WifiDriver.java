package ex19;

import java.util.Scanner;

public class WifiDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Reboot the computer and try to connect.");
		System.out.println("Did that fix the problem?");
		String entry = keyboard.nextLine();
		
		String no = "no";
		String yes = "yes";
		
		if(entry.equalsIgnoreCase(no)) 
		{
			System.out.println("Reboot the computer and try to connect.");
			System.out.println("Did that fix the problem?");
			String entry1 = keyboard.nextLine();
			if(entry1.equalsIgnoreCase(no))
			{
				System.out.println("Make sure the cables between the router & modem are plugged in firmly.");
				System.out.println("Did that fix the problem?");
				String entry2 = keyboard.nextLine();
				if(entry2.equalsIgnoreCase(no))
				{
					System.out.println("Move the router to a new location and try to connect.");
					System.out.println("Did that fix the problem?");
					String entry3 = keyboard.nextLine();
					if(entry3.equalsIgnoreCase(no))
					{
						System.out.println("Get a new router.");
					}
					else 
					{
						if(entry3.equalsIgnoreCase(yes))
						{
							System.out.println("Glad we could help.");
						}
						else
						{
							System.out.println("Error");
						}
					}
				}
				else 
				{
					if(entry2.equalsIgnoreCase(yes))
					{
						System.out.println("Glad we could help.");
					}
					else
					{
						System.out.println("Error");
					}
				}
			}
			else 
			{
				if(entry1.equalsIgnoreCase(yes))
				{
					System.out.println("Glad we could help.");
				}
				else
				{
					System.out.println("Error");
				}
			}
		}
		else 
		{
			if(entry.equalsIgnoreCase(yes))
			{
				System.out.println("Glad we could help.");
			}
			else
			{
				System.out.println("Error");
			}
		}
		
	}

}
