package ex12;
import  java.util.Scanner;

public class MobileDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Press A to select Package A\nPress B to select Package B\nPress C to select Package C");
		char pack = keyboard.next().charAt(0);
		System.out.println("Enter number of minutes used: ");
		int minutes = keyboard.nextInt();
		
		Mobile p = new Mobile(pack, minutes);
		
		if(pack == 'A' || pack == 'a')
		{
			System.out.printf("You chose Package A\nYour total charges is: $%,.2f\nPotential Savings if you Switch to Package B: $%,.2f\nPotential Savings if you Switch to Package C: $%,.2f", p.calcTotalCharges(), p.calcPotentialBSavings(), p.calcPotentialCSavings());
		}
		else if(pack == 'B' || pack == 'b')
		{
			System.out.printf("You chose Package B\nYour total charges is: $%,.2f\nPotential Savings if you Switch to Package C: $%,.2f", p.calcTotalCharges(), p.calcPotentialBToCSavings());
		}
		else if(pack == 'C' || pack == 'c')
		{
			System.out.printf("You chose Package C\nYour total charges is: $%,.2f", p.calcTotalCharges());
		}
		else 
		{
			System.out.println("Not a valid selection");
		}
	}

}
