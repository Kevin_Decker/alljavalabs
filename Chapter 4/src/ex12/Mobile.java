package ex12;

public class Mobile {

	private double monthlyBill;
	private int minutesUsed;
	
	//constructor
	public Mobile(double bill, int min)
	{
		monthlyBill = bill;
		minutesUsed = min;
	}
	
	//mutators
	public void setMonthlyBill(double bill)
	{
		monthlyBill = bill;
	}
	
	public void setMinutesUsed(int minutes)
	{
		minutesUsed = minutes;
	}
	
	//accessors
	public double getMonthlyBill()
	{
		return monthlyBill;
	}
	
	public int getMinutesUsed()
	{
		return minutesUsed;
	}
	
	//method to return total charges
	public double calcTotalCharges()
	{
		double total;
		
		if(monthlyBill == 'A')
		{
			total = 39.99;
			if(minutesUsed > 450)
			{
				int extra = minutesUsed - 450;
				return total += extra * .45;
			}
			else 
			{
				return total;
			}
		}
		else if(monthlyBill == 'B')
		{
			total = 59.99;
			if(minutesUsed > 900)
			{
				int extra = minutesUsed - 900;
				return total += extra * .4;
			}
			else
			{
				return total;
			}
		}
		else if(monthlyBill == 'C')
		{
			return total = 69.99;
		}
		else 
		{
			return 0;
		}
	}
	
	//method to return potential savings for plan B when the customer has package A
	public double calcPotentialBSavings()
	{
		double total;
		if(monthlyBill == 'A')
		{
			total = 39.99;
			double packB = 59.99;
			if(minutesUsed > 450)
			{
				int extra = minutesUsed - 450;
				total += extra * .45;
				return total - packB;
			}
			else 
			{
				return total;
			}
		}
		else
		{
			return 0;
		}
	}
	
	//method to return potential savings for plan C when the customer has package A
	public double calcPotentialCSavings()
	{
		double total;
		if(monthlyBill == 'A')
		{
			total = 39.99;
			double packC = 69.99;
			if(minutesUsed > 450)
			{
				int extra = minutesUsed - 450;
				total += extra * .45;
				return total - packC;
			}
			else 
			{
				return total;
			}
		}
		else
		{
			return 0;
		}
	}
	
	//method to return potential savings for plan C when the customer has package B
	public double calcPotentialBToCSavings()
	{
		double total;
		double packC = 69.99;
		if(monthlyBill == 'B')
		{
			total = 59.99;
			if(minutesUsed > 900)
			{
				int extra = minutesUsed - 900;
				total += extra * .4;
				return total - packC;
			}
			else
			{
				return total;
			}
		}
		else 
		{
			return 0;
		}
	}
}
