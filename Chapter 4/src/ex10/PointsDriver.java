package ex10;

import java.util.Scanner;

public class PointsDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a temperature: ");
		double temp = keyboard.nextDouble();
		
		FreezingBoilingPoints temp1 = new FreezingBoilingPoints(temp);
		
		if(temp1.isEthylFreezing()) { System.out.println("Ethyl will freeze at this temperature"); }
		
		if(temp1.isEthylBoiling()) { System.out.println("Ethyl will boil at this temperature"); }
		
		if(temp1.isOxygenFreezing()) { System.out.println("Oxygen will freeze at this temperature"); }
		
		if(temp1.isOxygenBoiling()) { System.out.println("Oxygen will boil at this temperature"); }
		
		if(temp1.isWaterFreezing()) { System.out.println("Water will freeze at this temperature"); }
		
		if(temp1.isWaterBoiling()) { System.out.println("Water will boil at this temperature"); }
		
		
	}

}
