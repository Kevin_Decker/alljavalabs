package ex10;

public class FreezingBoilingPoints {

	private double temperature;
	
	//constructor
	public FreezingBoilingPoints(double temp)
	{
		temperature = temp;
	}
	
	//mutator
	public void setTemp(double temp)
	{
		temperature = temp;
	}
	
	//accessor
	public double getTemp()
	{
		return temperature;
	}
	
	//method to return true if ethyl is freezing
	public boolean isEthylFreezing()
	{
		if(temperature <= -173)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//method to return true if ethyl is boiling
	public boolean isEthylBoiling()
	{
		if(temperature >= 172)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//method to return true if oxygen is freezing
	public boolean isOxygenFreezing()
	{
		if(temperature <= -362)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//method to return true if oxygen is boiling
	public boolean isOxygenBoiling()
	{
		if(temperature >= -306)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//method to return true if water is freezing
	public boolean isWaterFreezing()
	{
		if(temperature <= 32)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//method to return true if water is boiling
	public boolean isWaterBoiling()
	{
		if(temperature >= 212)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
}
