package ex1;

import java.util.Scanner;

public class RomanNumeralDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a number between 1 and 10");
		int user = keyboard.nextInt();
		
		if(user == 1)
		{
			System.out.println(user + " is I in Roman Numerals");
		}
		else if(user == 2) 
		{
			System.out.println(user + " is II in Roman Numerals");
		}
		else if(user == 3) 
		{
			System.out.println(user + " is III in Roman Numerals");
		}
		else if(user == 4) 
		{
			System.out.println(user + " is IV in Roman Numerals");
		}
		else if(user == 5) 
		{
			System.out.println(user + " is V in Roman Numerals");
		}
		else if(user == 6) 
		{
			System.out.println(user + " is VI in Roman Numerals");
		}
		else if(user == 7) 
		{
			System.out.println(user + " is VII in Roman Numerals");
		}
		else if(user == 8) 
		{
			System.out.println(user + " is VIII in Roman Numerals");
		}
		else if(user == 9) 
		{
			System.out.println(user + " is IX in Roman Numerals");
		}
		else if(user == 10) 
		{
			System.out.println(user + " is X in Roman Numerals");
		}
		
	}

}
