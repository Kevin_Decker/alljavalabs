package ex18;

public class RoulettePocket {

	private int pocketNumber;
	
	//constructor
	public RoulettePocket(int num)
	{
		pocketNumber = num;
	}
	
	//mutator
	public void setPocketNumber(int pN)
	{
		pocketNumber = pN;
	}
	
	//accessor
	public int getPocketNumber()
	{
		return pocketNumber;
	}
	
	//method that returns the pocket number's color
	public void getPocketColor()
	{
		if(pocketNumber == 0 && pocketNumber == 00)
		{
			System.out.println("Pocket color is green");
		}
		else if(pocketNumber >= 1 && pocketNumber <= 10)
		{
			if((pocketNumber % 2) == 0)
			{
				//even numbers
				System.out.println("Pocket color is black");
			}
			else 
			{
				//odd numbers
				System.out.println("Pocket color is red");
			}
		}
		else if(pocketNumber >= 11 && pocketNumber <= 18)
		{
			if((pocketNumber % 2) == 0)
			{
				//even numbers
				System.out.println("Pocket color is red");	
			}
			else 
			{
				//odd numbers
				System.out.println("Pocket color is black");
			}
		}
		else if(pocketNumber >= 19 && pocketNumber <= 28)
		{
			if((pocketNumber % 2) == 0)
			{
				//even numbers
				System.out.println("Pocket color is black");	
			}
			else 
			{
				//odd numbers
				System.out.println("Pocket color is red");
			}	
		}
		else if(pocketNumber >= 29 && pocketNumber <= 36)
		{
			if((pocketNumber % 2) == 0)
			{
				//even numbers
				System.out.println("Pocket color is red");	
			}
			else 
			{
				//odd numbers
				System.out.println("Pocket color is black");
			}	
		}
		else 
		{
			System.out.println("Error: Number is out of range");
		}
	}
	
}
