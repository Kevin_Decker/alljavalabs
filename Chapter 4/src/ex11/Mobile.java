package ex11;

public class Mobile {

	private double monthlyBill;
	private int minutesUsed;
	
	//constructor
	public Mobile(double bill, int min)
	{
		monthlyBill = bill;
		minutesUsed = min;
	}
	
	//mutators
	public void setMonthlyBill(double bill)
	{
		monthlyBill = bill;
	}
	
	public void setMinutesUsed(int minutes)
	{
		minutesUsed = minutes;
	}
	
	//accessors
	public double getMonthlyBill()
	{
		return monthlyBill;
	}
	
	public int getMinutesUsed()
	{
		return minutesUsed;
	}
	
	//method to return total charges
	public double calcTotalCharges()
	{
		double total;
		
		if(monthlyBill == 'A')
		{
			total = 39.99;
			if(minutesUsed > 450)
			{
				int extra = minutesUsed - 450;
				return total += extra * .45;
			}
			else 
			{
				return total;
			}
		}
		else if(monthlyBill == 'B')
		{
			total = 59.99;
			if(minutesUsed > 900)
			{
				int extra = minutesUsed - 900;
				return total += extra * .4;
			}
			else
			{
				return total;
			}
		}
		else if(monthlyBill == 'C')
		{
			return total = 69.99;
		}
		else 
		{
			return 0;
		}
		
	}
}
