package ex13;

public class BMI {

	private double weight;
	private double height;
	
	//constructor
	public BMI(double weigh, double heigh)
	{
		weight = weigh;
		height = heigh;
	}
	
	//mutators
	public void setWeight(double w)
	{
		weight = w;
	}
	
	public void setHeight(double h)
	{
		height = h;
	}
	
	//accessors
	public double getWeight()
	{
		return weight;
	}
	
	public double getHeight()
	{
		return height;
	}
	
	//method to calculate body mass index
	public void calcBodyMassIndex()
	{
		double heightPow = Math.pow(height, 2);
		double bmi = weight * (703/heightPow);
		
		if(bmi >= 18.5 && bmi <= 25)
		{
			System.out.printf("Your BMI is %.2f\nConsidered Optimal Weight", bmi);
		}
		else if(bmi > 25)
		{
			System.out.printf("Your BMI is %.2f\nConsidered Over Weight", bmi);
		}
		else 
		{
			System.out.printf("Your BMI is %.2f\nConsidered Under Weight", bmi);
		}
	}
	
}
