package ex13;
import java.util.Scanner;

public class BMIDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter your weight (pounds): ");
		double weight = keyboard.nextDouble();
		System.out.println("Enter your height (inches): ");
		double height = keyboard.nextDouble();
		
		BMI bodyMassIndex = new BMI(weight, height);
		
		System.out.println("Results: \n");
		bodyMassIndex.calcBodyMassIndex();
		
	}

}
