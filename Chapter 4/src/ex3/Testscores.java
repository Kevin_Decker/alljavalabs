package ex3;

public class Testscores {

	private double test1;
	private double test2;
	private double test3;
	
	//constructors
/*	public Testscores() 
	{
		
	}*/
	
	public Testscores(double tOne, double tTwo, double tThree)
	{
		test1 = tOne;
		test2 = tTwo;
		test3 = tThree;
	}
	
	//mutators
	public void setTest1(double one)
	{
		test1 = one;
	}
	
	public void setTest2(double two)
	{
		test2 = two;
	}
	
	public void setTest3(double three)
	{
		test3 = three;
	}
	
	//accessors
	public double getTest1()
	{
		return test1;
	}
	
	public double getTest2()
	{
		return test2;
	}
	
	public double getTest3()
	{
		return test3;
	}

	//method to return the average
	public double calcAverage()
	{
		return (test1 + test2 + test3) / 3;
	}
	
	//method to return letter grade
	public String letterGrade()
	{
		double average = (test1 + test2 + test3) / 3;
		
		if(average < 60)
		{
			return "F";
		}
		else if(average >= 60 && average <= 69)
		{
			return "D";
		}
		else if(average >= 70 && average <= 79) 
		{
			return "C";
		}
		else if(average >= 80 && average <= 89)
		{
			return "B";
		}
		else if(average >= 90 && average <= 100)
		{
			return "A";
		}
		else
		{
			return "Invalid entry. Cannot get higher than a 100";
		}
	}
	
}
