package ex16;

import java.util.Scanner;

public class MagicDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a month (1-12): ");
		int month = keyboard.nextInt();
		System.out.println("Enter a day (1-31): ");
		int day = keyboard.nextInt();
		System.out.println("Enter a year (last 2 digits - ex. 18 for 2018)");
		int year = keyboard.nextInt();
		
		MagicDate magic = new MagicDate(month, day, year);
		
		magic.isMagic();
		
		
		
	}

}
