package ex16;

public class MagicDate {

	private int month;
	private int day;
	private int year;
	
	//constructor
	public MagicDate(int m, int d, int y)
	{
		month = m;
		day = d;
		year = y;
	}
	
	//mutators
	public void setMonth(int mo)
	{
		month = mo;
	}
	
	public void setDay(int da)
	{
		day = da;
	}
	
	public void setYear(int ye)
	{
		year = ye;
	}
	
	//accessors
	public int getMonth()
	{
		return month;
	}
	
	public int getDay()
	{
		return day;
	}
	
	public int getYear()
	{
		return year;
	}
	
	//method that returns true if the date is magic
	public boolean isMagic()
	{
		if(month * day == year)
		{
			System.out.println("This is a magic date!");
			return true;
		}
		else
		{
			System.out.println("This is not a magic date :(");
			return false;
		}
	}
	
}
