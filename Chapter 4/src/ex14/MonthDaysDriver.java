package ex14;
import java.util.Scanner;

public class MonthDaysDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a month (1-12): ");
		int month = keyboard.nextInt();
		System.out.println("Enter a year: ");
		int year = keyboard.nextInt();
		
		MonthDays md = new MonthDays(month, year);
		
		md.getNumberOfDays();
	}

}
