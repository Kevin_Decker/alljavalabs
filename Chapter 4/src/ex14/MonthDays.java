package ex14;

public class MonthDays {

	private int month;
	private int year;
	
	//constructor
	public MonthDays(int mo, int yea)
	{
		month = mo;
		year = yea;
	}
	
	//mutators
	public void setMonth(int mont)
	{
		month = mont;
	}
	
	public void setYear(int ye)
	{
		year = ye;
	}
	
	//accessors
	public int getMonth()
	{
		return month;
	}
	
	public int getYear()
	{
		return year;
	}
	
	//method that returns the number of days in the inputted month and year
	public void getNumberOfDays()
	{
		switch (month) {
		case 1:
			System.out.println("31 days");
			break;
		case 2:
			if (year % 100 == 0 && year % 400 == 0) 
			{
				System.out.println("29 days - leap year");
			}
			else if(year % 100 != 0 && year % 4 == 0)
			{
				System.out.println("29 days - leap year");
			}
			else 
			{	
				System.out.println("28 days - not a leap year");	
			}
			break;
		case 3:
			System.out.println("31 days");
			break;
		case 4:
			System.out.println("30 days");
			break;
		case 5:
			System.out.println("31 days");
			break;
		case 6:
			System.out.println("30 days");
			break;
		case 7:
			System.out.println("31 days");
			break;
		case 8:
			System.out.println("31 days");
			break;
		case 9:
			System.out.println("30 days");
			break;
		case 10:
			System.out.println("31 days");
			break;
		case 11:
			System.out.println("30 days");
			break;
		case 12:
			System.out.println("31 days");
			break;
		default:
			System.out.println("Error: Not a valid month");
			break;
		}
	}
	
}
