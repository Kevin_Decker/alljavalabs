package ex2;

public class ShiftSupervisor extends Employee {

	private double salary;
	private double bonus;
	
	//constructor
	public ShiftSupervisor(String n, String num, String date, double sal, double b)
	{
		super(n, num, date);
		salary = sal;
		bonus = b;
	}
	
	public ShiftSupervisor()
	{
		
	}

	//getters and setters
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public double getBonus() {
		return bonus;
	}
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	
	@Override
	public String toString()
	{
		return super.toString() + "\n" + "Salary: " + salary + "\n" + "Bonus: " + bonus;
	}
	
}
