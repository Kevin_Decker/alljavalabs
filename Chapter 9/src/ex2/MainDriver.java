package ex2;
import java.util.Scanner;

public class MainDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter name: ");
		String name = keyboard.nextLine();
		System.out.println("Enter employee Number: ");
		String empNum = keyboard.nextLine();
		System.out.println("Enter hire date: ");
		String hireDate = keyboard.nextLine();
		System.out.println("Enter salary: ");
		double salary = keyboard.nextDouble();
		System.out.println("Enter bonus: ");
		double bonus = keyboard.nextDouble();
		
		ShiftSupervisor s1 = new ShiftSupervisor(name, empNum, hireDate, salary, bonus);
		
		System.out.println(s1);

	}

}
