package ex4;
import java.util.Scanner;

public class MainDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter Grammar Score (1-30): ");
		double grammar = keyboard.nextDouble();
		System.out.println("Enter Spelling Score (1-20): ");
		double spelling = keyboard.nextDouble();
		System.out.println("Enter Correct Length Score (1-20): ");
		double correctLength = keyboard.nextDouble();
		System.out.println("Enter Content Score (1-30): ");
		double content = keyboard.nextDouble();
		
		Essay essay = new Essay();
		essay.setScore(grammar, spelling, correctLength, content);
		
		System.out.println("Score: " + essay.getScore() + "\n" + "Grade: " + essay.getGrade());
	}

}
