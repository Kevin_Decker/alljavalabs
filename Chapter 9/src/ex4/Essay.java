package ex4;

public class Essay extends GradedActivity {

	private double grammar;
	private double spelling;
	private double correctLength;
	private double content;
	
	//method
	public void setScore(double gr, double sp, double len, double cnt)
	{
		setGrammar(gr);
		setSpelling(sp);
		setCorrectLength(len);
		setContent(cnt);
		super.setScore(getScore());
	}

	//getters and setters
	public double getGrammar() {
		return grammar;
	}

	public void setGrammar(double grammar) {
		if(grammar <= 30.00)
		{
			this.grammar = grammar;
		}
		else
		{
			System.out.println("Error: too many points awarded");
		}
	}

	public double getSpelling() {
		return spelling;
	}

	public void setSpelling(double spelling) {
		if(spelling <= 20.00)
		{
			this.spelling = spelling;
		}
		else
		{
			System.out.println("Error: too many points awarded");
		}
		
	}

	public double getCorrectLength() {
		return correctLength;
	}

	public void setCorrectLength(double correctLength) {
		if(correctLength <= 20.00)
		{
			this.correctLength = correctLength;
		}
		else
		{
			System.out.println("Error: too many points awarded");
		}
	}

	public double getContent() {
		return content;
	}

	public void setContent(double content) {
		if(content <= 30.00)
		{
			this.content = content;
		}
		else
		{
			System.out.println("Error: too many points awarded");
		}
	}


	public double getScore()
	{
		return grammar + spelling + correctLength + content;
	}
	
	
	
}
