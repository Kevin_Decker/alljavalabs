package ex4;

public class GradedActivity {

	private double score;

	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	//method to get letter grade
	public char getGrade()
	{	
		if(score < 60)
		{
			return 'F';
		}
		else if(score < 70)
		{
			return 'D';
		}
		else if(score < 80)
		{
			return 'C';
		}
		else if(score < 90)
		{
			return 'B';
		}
		else 
		{
			return 'A';
		}
	}
	
}
