package ex5;

public class FinalExam extends GradedActivity {

	private int numQuestions;
	private double pointsEach;
	private int numMissed;
	
	public FinalExam(int questions, int missed)
	{
		double score;
		
		numQuestions = questions;
		numMissed = missed;
		
		pointsEach = 100.0 / questions;
		score = 100.0 - (missed * pointsEach);
		
		setScore(score);
	}
	
	public FinalExam(FinalExam fe)
	{
		this.numQuestions = fe.numQuestions;
		this.pointsEach = fe.pointsEach;
		this.numMissed = fe.numMissed;
		
		double score = 100-(numMissed * pointsEach);
		setScore(score);
	}
	
	

	public double getPointsEach() {
		return pointsEach;
	}

	public int getNumMissed() {
		return numMissed;
	}
	
	public int getNumQuestions()
	{
		return numQuestions;
	}
	
	public String toString()
	{
		return "Your final exam grade is " + this.getScore() + " - " + this.getGrade();
	}
	
}
