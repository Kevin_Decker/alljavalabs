package ex5;

public class PassFailExam extends PassFailActivity {

	private int numQuestions;
	private double pointsEach;
	private int numMissed;
	
	public PassFailExam(int questions, int missed, double minPassing)
	{
		super(minPassing);
		
		double score;
		numQuestions = questions;
		numMissed = missed;
		
		pointsEach = 100.0 / questions;
		score = 100.0 / (missed * pointsEach);
		
		setScore(score);
	}
	
	public PassFailExam(PassFailExam pa)
	{
		super(70);
		this.numQuestions = pa.numQuestions;
		this.pointsEach = pa.pointsEach;
		this.numMissed = pa.numMissed;
		
		double score = 100-(numMissed * pointsEach);
		setScore(score);
		
	}

	public double getPointsEach() {
		return pointsEach;
	}

	public int getNumMissed() {
		return numMissed;
	}
	
	public int getNumQuestions()
	{
		return numQuestions;
	}
	
	public String toString()
	{
		return "Your final exam grade is " + this.getScore() + " - " + this.getGrade();
	}
	
}
