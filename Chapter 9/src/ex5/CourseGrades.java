package ex5;

public class CourseGrades extends GradedActivity {

	private GradedActivity grades[];
	private final int NUM_GRADES = 4;
	
	//constructor
	public CourseGrades()
	{
		grades = new GradedActivity[NUM_GRADES];
	}
	
	//methods
	public void setLab(GradedActivity aLab)
	{
		grades[0] = new GradedActivity(aLab);
	}
	
	public void setPassFailExam(PassFailExam aPassFailExam)
	{
		grades[1] = new PassFailExam(aPassFailExam);
	}
	
	public void setEssay(Essay anEsasy)
	{
		grades[2] = new Essay(anEsasy);
	}
	
	public void setFinalExam(FinalExam aFinalExam)
	{
		grades[3] = new FinalExam(aFinalExam);
	}
	
	//to string
	@Override
	public String toString()
	{
		return "Lab: " + grades[0] + "\n" + "Pass/Fail Exam: " + grades[1].toString() + "\n" + "Essay: " + grades[2].toString() + "\n" + "Final Exam: " + grades[3].toString();
	}
	
}
