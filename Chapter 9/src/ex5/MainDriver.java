package ex5;

public class MainDriver {

	public static void main(String[] args) {
		
		CourseGrades courseGrades = new CourseGrades();
		
		GradedActivity lab = new GradedActivity();
		lab.setScore(90);
		
		Essay essay = new Essay();
		essay.setScore(25, 18, 20, 25);
		
		PassFailExam passFailExam = new PassFailExam(10, 0, 70);
		FinalExam finalExam = new FinalExam(50, 4);
		
		courseGrades.setEssay(essay);
		courseGrades.setFinalExam(finalExam);
		courseGrades.setLab(lab);
		courseGrades.setPassFailExam(passFailExam);
		
		System.out.println(courseGrades);
	}

}
