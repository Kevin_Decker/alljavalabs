package ex1;
import java.util.Scanner;

public class MainDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		ProductionWorker p1;
		
		System.out.println("Enter name: ");
		String name = keyboard.nextLine();
		System.out.println("Enter employee Number: ");
		String empNum = keyboard.nextLine();
		System.out.println("Enter hire date: ");
		String hireDate = keyboard.nextLine();
		System.out.println("Enter shift: (Day=1,Night=2)");
		int shift = keyboard.nextInt();
		System.out.println("Enter pay rate: ");
		double payRate = keyboard.nextDouble();
		
		p1 = new ProductionWorker(name, empNum, hireDate, shift, payRate);
		
		System.out.println(p1);
		
	}

}
