package ex1;

public class ProductionWorker extends Employee {

	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	//constructor
	public ProductionWorker(String n, String num, String date, int sh, double rate)
	{
		super(n, num, date);
		payRate = rate;
		if(sh == 1)
		{
			shift = DAY_SHIFT;
		}
		else if(sh == 2)
		{
			shift = NIGHT_SHIFT;
		}
		else 
		{
			shift = 0;
		}
	}
	
	public ProductionWorker()
	{
		
	}

	//getters and setters
	public int getShift() {
		return shift;
	}
	public void setShift(int shift) {
		this.shift = shift;
	}
	public double getPayRate() {
		return payRate;
	}
	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}
	
	@Override
	public String toString()
	{
		return super.toString() + "\n" + "Shift: " + shift + "\n" + "Pay Rate: " + payRate;
	}
}
