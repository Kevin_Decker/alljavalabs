package ex3;
import java.util.Scanner;;

public class MainDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter Name: ");
		String name = keyboard.nextLine();
		System.out.println("Enter Employee Number: ");
		String empNum = keyboard.nextLine();
		System.out.println("Enter hire date: ");
		String date = keyboard.nextLine();
		
		System.out.println("Enter Shift: (1 or 2)");
		int shift = keyboard.nextInt();
		System.out.println("Enter pay rate: ");
		double payRate = keyboard.nextDouble();
		
		System.out.println("Enter Monthly bonus: ");
		double mBonus = keyboard.nextDouble();
		System.out.println("Enter required training hours: ");
		double rth = keyboard.nextDouble();
		System.out.println("Enter training hours attended: ");
		double tha = keyboard.nextDouble();
		
		TeamLeader teamLeader = new TeamLeader(name, empNum, date, shift, payRate, mBonus, rth, tha);
		
		System.out.println(teamLeader);
	}

}
