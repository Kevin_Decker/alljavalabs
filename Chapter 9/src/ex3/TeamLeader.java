package ex3;

public class TeamLeader extends ProductionWorker {

	private double monthlyBonus;
	private double requiredTrainingHours;
	public double trainingHoursAttended;
	
	public TeamLeader(String n, String num, String date, int sh, double rate, double mb, double rth, double tha)
	{
		super(n, num, date, sh, rate);
		monthlyBonus = mb;
		requiredTrainingHours = rth;
		trainingHoursAttended = tha;
	}
	
	public TeamLeader()
	{
		
	}

	public double getMonthlyBonus() {
		return monthlyBonus;
	}
	public void setMonthlyBonus(double monthlyBonus) {
		this.monthlyBonus = monthlyBonus;
	}
	public double getRequiredTrainingHours() {
		return requiredTrainingHours;
	}
	public void setRequiredTrainingHours(double requiredTrainingHours) {
		this.requiredTrainingHours = requiredTrainingHours;
	}
	public double getTrainingHoursAttended() {
		return trainingHoursAttended;
	}
	public void setTrainingHoursAttended(double trainingHoursAttended) {
		this.trainingHoursAttended = trainingHoursAttended;
	}
	
	@Override
	public String toString()
	{
		return super.toString() + "\n" + "Monthly Bonus: " + monthlyBonus + "\n" + "Required Training Hours: " + 
				requiredTrainingHours + "\n" + "Training Hours Attended: " + trainingHoursAttended;
	}
	
}
