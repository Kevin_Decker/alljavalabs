package ex3;

public class Employee {

	private String name;
	private String employeeNumber;
	private String hireDate;
	
	//constructor
	public Employee(String n, String num, String date)
	{
		name = n;
		employeeNumber = num;
		hireDate = date;
	}
	
	public Employee()
	{
		
	}
	
	//getters and setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getHireDate() {
		return hireDate;
	}
	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	
	private boolean isValidEmpNum(String e)
	{
		if(e.matches("[0-9]{3}-[A-Ma-m]"))
		{
			return true;
		}
		return false;
	}
	
	@Override
	public String toString()
	{
		if(isValidEmpNum(employeeNumber) == true)
		{
			return String.format("Name: %s\nEMP Num: %s\nHire Date: %s\n%s", name, employeeNumber, hireDate, isValidEmpNum(employeeNumber));
		}
		else 
		{
			return "Error in Employee Number format.";
		}
		
	}
}
