import java.util.Scanner;

public class WordGame
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter your name:");
        String name = input.nextLine();
        
        System.out.println("Enter your age:");
        int age = input.nextInt();
        
        input.nextLine();
        
        System.out.println("Enter a city:");
        String city = input.nextLine();
        
        System.out.println("Enter a college:");
        String college = input.nextLine();
        
        System.out.println("Enter a profession:");
        String profession = input.nextLine();
        
        System.out.println("Enter an animal:");
        String animal = input.nextLine();  
        
        System.out.println("Enter a pet name:");
        String pet = input.nextLine();
        
        System.out.println("There once was a person named " + name + " who lived in " + city + ". At the age of " + age +
                           ", \n" + name + " went to college at " + college + ". " + name + " graduated and went to work as a \n" + 
                           profession + ". Then, " + name + " adopted a " + animal + " named " + pet + ". They both lived \nhappily ever after!"  
                          );
    }
}