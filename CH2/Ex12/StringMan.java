import java.util.Scanner;
//import java.lang;

public class StringMan
{
    public static void main(String[] args)
    {
        System.out.println("What is your favorite city?");
        
        Scanner input = new Scanner(System.in);
        String city = input.nextLine();
        
        int stringLength = city.length();
        String upperCase = city.toUpperCase();
        String lowerCase = city.toLowerCase();
        char firstChar = city.charAt(0);
        
        System.out.println("Number of Characters: " + stringLength + "\n" +
                          "Upper Case: " + upperCase + "\n" +
                           "Lower Case: " + lowerCase + "\n" +
                           "First Character: " + firstChar
                          );
        
    }
}