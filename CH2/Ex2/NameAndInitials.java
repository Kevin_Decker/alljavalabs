public class NameAndInitials
{
    public static void main(String[] args)
    {
        String firstName = "Kevin";
        String middleName = "Anonymous";
        String lastName = "Decker";
        
        char firstInitial = 'K';
        char middleInitial = 'A';
        char lastInitial = 'D';
        
        System.out.println("This is my name: " + firstName + " " + middleName + " " + lastName + "\n" +
                          "These are my initials: " + firstInitial + middleInitial + lastInitial);
    }
}