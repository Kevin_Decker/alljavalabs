import java.util.Scanner;

public class RestBill
{
    public static void main(String[] args)
    {
        final double TAX = 0.075;
        final double TIP = 0.18;
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter your meal charge: ");
        double mealCharge = input.nextDouble();
        
        double taxAmount = mealCharge * TAX;
        double tipAmount = mealCharge * TIP;
        double total = taxAmount + tipAmount + mealCharge;
        
        System.out.println("Meal Charge: $" + mealCharge + "\n" +
                           "Tax: $" + taxAmount + "\n" +
                           "Tip: $" + tipAmount + "\n" +
                           "Total: $" + total
                          );
    }
}