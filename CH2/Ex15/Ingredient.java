import java.util.Scanner;

public class Ingredient
{
    public static void main(String[] args)
    {
        final double CUPS_SUGAR = 1.5;
        final double CUPS_BUTTER = 1;
        final double CUPS_FLOUR = 2.75;
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("How many cookies do you want to make?");
        double cookies = input.nextInt();
        
        double cookieRatio = cookies / 48;
        double newSugar = cookieRatio * CUPS_SUGAR;
        double newButter = cookieRatio * CUPS_BUTTER;
        double newFlour = cookieRatio * CUPS_FLOUR;
        
        System.out.println("For " + cookies + " cookies, you will need: " + "\n" +
                           newSugar + " cups of sugar." + "\n" +
                           newButter + " cups of butter." + "\n" +
                           newFlour + " cups of flour."
                          );
        
    }
}