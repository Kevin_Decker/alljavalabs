public class EnergyDrink
{
    public static void main(String[] args)
    {   
        double customersOneOrMore = 15000 * 0.18;
        double citrus = customersOneOrMore * 0.58;
        
        System.out.println("Of 15,000 customers, " + (int)customersOneOrMore + 
                           " purchase an energy drink 1 or more times a week." + "\n" +
                           (int)citrus + " of the customers that purchase 1 or more a week prefer a citrus flavor."
                          );
    }
}