public class StockCom
{
    public static void main(String[] args)
    {
        int shares = 1000;
        double stockPrice = 25.50;
        double commission = 0.02;
        
        double stockAmount = shares * stockPrice;
        double commissionAmount = stockAmount * commission;
        double total = stockAmount + commissionAmount;
        
        System.out.println("Amount paid for the stocks: $" + stockAmount + "\n" +
                           "Commission amount: $" + commissionAmount + "\n" +
                           "Total Amount: $" + total
                          );
    }
}