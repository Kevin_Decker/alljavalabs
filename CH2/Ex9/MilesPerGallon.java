import java.util.Scanner;

public class MilesPerGallon
{
    public static void main(String[] args)
    { 
        Scanner userInput = new Scanner(System.in);
        
        System.out.println("Enter miles driven: ");
        double miles = userInput.nextDouble();
        
        System.out.println("Enter gallons used: ");
        double gallons = userInput.nextDouble();
        
        double mpg = miles / gallons;
        
        System.out.println("MPG is: " + mpg);
    }
}