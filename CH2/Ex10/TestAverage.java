import java.util.Scanner;

public class TestAverage
{
    public static void main(String[] args)
    {
        Scanner test = new Scanner(System.in);
        
        System.out.println("Test Score 1: ");
        double test1 = test.nextDouble();
        
        System.out.println("Test Score 2: ");
        double test2 = test.nextDouble();
        
        System.out.println("Test Score 3: ");
        double test3 = test.nextDouble();
        
        double avg = (test1 + test2 + test3) / 3;
        
        System.out.println("Test 1: " + test1 + "\n" +
                           "Test 2: " + test2 + "\n" +
                           "Test 3: " + test3 + "\n" +
                           "Average: " + avg
                          );
    }
}