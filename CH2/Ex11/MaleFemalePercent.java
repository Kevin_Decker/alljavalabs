import java.util.Scanner;

public class MaleFemalePercent
{
    public static void main(String[] args)
    {       
        Scanner students = new Scanner(System.in);
        
        System.out.println("How many males are in the class?");
        int males = students.nextInt();
        
        System.out.println("How many females are in the class?");
        int females = students.nextInt();
        
        int total = males + females;
        double malePercent = (float)males / total;
        double femalePercent = (float)females / total;
        
        malePercent *= 100;
        femalePercent *= 100;
        
        System.out.printf("Percentage of males: %%%.0f \n", malePercent);
        System.out.printf("Percentage of females: %%%.0f", femalePercent);
    }
}