public class LandCalculation
{
    public static void main(String[] args)
    {
        int feetPerAcre = 43560;
        int tract = 389767;
        
        int acre = tract / feetPerAcre;
        
        System.out.println("A tract of land with " + tract + " square feet has " + acre + " acres.");
    }
}