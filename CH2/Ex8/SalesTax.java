import java.util.Scanner;

public class SalesTax
{
    public static void main(String[] args)
    {
        final double STATE_TAX = 0.055;
        final double COUNTY_TAX = 0.02;
        
        Scanner userInput = new Scanner(System.in);
        
        System.out.println("Enter a purchase amount: ");
        double amount = userInput.nextInt();
        
        double stateTaxTotal = amount * STATE_TAX;
        double countyTaxTotal = amount * COUNTY_TAX;
        double total = amount + stateTaxTotal + countyTaxTotal;
        
        System.out.println("Your purchase amount is: " + amount + "\n" +
                          "State Sales Tax: " + stateTaxTotal + "\n" +
                          "County Sales Tax: " + countyTaxTotal + "\n" +
                          "Total Sales Tax: " + (stateTaxTotal + countyTaxTotal) + "\n" +
                          "Total: " + (total)   
                          );
    }
}