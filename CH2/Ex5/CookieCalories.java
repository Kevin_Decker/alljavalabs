import java.util.Scanner;

public class CookieCalories
{
    public static void main(String[] args)
    {
        int servingToCalories = 300;
        double oneCookieServing = 0.25;
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("How many cookies did you eat? ");
        int cookies = keyboard.nextInt();
        
        double servingTotal = cookies * oneCookieServing;
        double calorieTotal = servingTotal * 300;
        
        System.out.println(calorieTotal + " calories");
    }
}