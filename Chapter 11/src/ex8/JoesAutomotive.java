package ex8;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import java.text.DecimalFormat;

public class JoesAutomotive extends Application {

	private Button btnOil;
	private Button btnLube;
	private Button btnRadiator;
	private Button btnTransmission;
	private Button btnInspection;
	private Button btnMuffler;
	private Button btnTire;
	private Button addService;
	private Label lblTotal;
	private TextField otherServices;
	private double total = 0;
	/////////////////////
	public static void main(String[] args) {
		
		launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		DecimalFormat dFormat = new DecimalFormat("##,##0.00");
		
		Label label = new Label("Click button to buy service\n\n");
		lblTotal = new Label();
		
		btnOil = new Button("Oil Change");
		btnOil.setOnAction(e -> 
		{
			total += 35.00;
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		btnLube = new Button("Lube job");
		btnLube.setOnAction(e -> 
		{
			total += 25.00;
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		btnRadiator = new Button("Radiator flush");
		btnRadiator.setOnAction(e -> 
		{
			total += 50.00;
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		btnTransmission = new Button("Transmission Flush");
		btnTransmission.setOnAction(e -> 
		{
			total += 120.00;
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		btnInspection = new Button("Inspection");
		btnInspection.setOnAction(e -> 
		{
			total += 35.00;
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		btnMuffler = new Button("Muffler Replacement");
		btnMuffler.setOnAction(e -> 
		{
			total += 200.00;
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		btnTire = new Button("Tire Rotation");
		btnTire.setOnAction(e -> 
		{
			total += 20.00;
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		
		Label lblBlank = new Label();
		Label label2 = new Label("Enter hours ($60/hour): ");
		addService = new Button("Add To Total");
		addService.setOnAction(e -> 
		{
			double hours = Double.parseDouble(otherServices.getText());
			total += (hours * 60);
			lblTotal.setText("Total: $" + dFormat.format(total));
		});
		otherServices = new TextField();
		HBox hBox = new HBox(2, label2, otherServices, addService);
		
		GridPane gridPane = new GridPane();
		gridPane.add(label, 0, 0);
		gridPane.add(btnOil, 0, 1);
		gridPane.add(btnLube, 0, 2);
		gridPane.add(btnRadiator, 0, 3);
		gridPane.add(btnTransmission, 0, 4);
		gridPane.add(btnInspection, 0, 5);
		gridPane.add(btnMuffler, 0, 6);
		gridPane.add(btnTire, 0, 7);
		gridPane.add(lblTotal, 1, 1);
		gridPane.add(lblBlank, 0, 8);
		gridPane.add(hBox, 0, 9);
		
		gridPane.setVgap(3);
		
		
		Scene newScene = new Scene(gridPane, 500, 400);
		arg0.setScene(newScene);
		arg0.show();
		
	}

}
