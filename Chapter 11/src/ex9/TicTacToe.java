package ex9;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.scene.image.*;
import java.lang.Exception;
import java.lang.String;
import java.util.Random;

public class TicTacToe extends Application {

	private Random random;
	private Image img;
	private ImageView view;
	private Label lblWins;
	private GridPane gridPane;
	
	public static void main(String[] args) {
		
		launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		lblWins = new Label("");
		lblWins.setAlignment(Pos.CENTER);
		Button btnNewGame = new Button("New Game");
		btnNewGame.setAlignment(Pos.CENTER);
		btnNewGame.setOnAction(new ButtonClickHandler());

		gridPane = new GridPane();
		
		gridPane.add(lblWins, 1, 3);
		gridPane.add(btnNewGame, 1, 4);
		
		gridPane.setAlignment(Pos.CENTER);
		gridPane.setHgap(10);
		gridPane.setVgap(10);
		Scene newScene = new Scene(gridPane, 700, 600);
		arg0.setScene(newScene);
		arg0.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			
			random = new Random();
			
			int[][] game = new int[3][3];
			
				for(int x = 0; x < game.length; ++x)
				{
					
					for(int i = 0; i < game[x].length; ++i)
					{
						int randomNum = random.nextInt(2);
						game[x][i] = randomNum;
						int val = -1;
						
						if(randomNum == 0)
						{
							img = new Image("file:x.png");
							val = 0;
						}
						else
						{
							img = new Image("file:o.png");
							val = 1;
						}
						view = new ImageView(img);
						gridPane.add(view, x, i);
						
						if(game[0][0] == game[0][1] && game[0][0] == game[0][2])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						/////////////////////
						else if(game[1][0] == game[1][1] && game[1][0] == game[1][2])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						
						else if(game[2][0] == game[2][1] && game[2][0] == game[2][2])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						
						else if(game[0][0] == game[1][0] && game[0][0] == game[2][0])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						
						else if(game[0][1] == game[1][1] && game[0][1] == game[2][1])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						
						else if(game[0][2] == game[1][2] && game[0][2] == game[2][2])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						
						else if(game[0][0] == game[1][1] && game[0][0] == game[2][2])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						
						else if(game[0][2] == game[1][1] && game[0][2] == game[2][0])
						{
							if(val == 0)
							{
								lblWins.setText("X Wins!");
							}
							else
							{
								lblWins.setText("O Wins!");
							}
						}
						
						else 
						{
							lblWins.setText("Tie!");
						}
					}
				}
			}
		}
	}

