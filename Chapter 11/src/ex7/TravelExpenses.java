package ex7;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import java.text.DecimalFormat;

public class TravelExpenses extends Application {

	private TextField days;
	private TextField airfareCost;
	private TextField carRentalFees;
	private TextField milesDriven;
	private TextField parkingFees;
	private TextField taxiCharges;
	private TextField seminarFees;
	private TextField lodgingChargesPerNight;
	private Label lbltotalExpenses;
	private Label lblTotalAllowable;
	private Label lblExcess;
	private Label lblSaved;
	
	
	public static void main(String[] args) {
		
		launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		Label lblTravel = new Label("Travel Expenses\n\n");
		
		Label label1 = new Label("# of Days: ");
		days = new TextField();
		HBox hBox1 = new HBox(5, label1, days);
		
		Label label2 = new Label("Cost of Airfare: ");
		airfareCost = new TextField();
		HBox hBox2 = new HBox(5, label2, airfareCost);
		
		Label label3 = new Label("Car Rental Fees: ");
		carRentalFees = new TextField();
		HBox hBox3 = new HBox(5, label3, carRentalFees);
		
		Label label4 = new Label("Miles Driven(personal car): ");
		milesDriven = new TextField();
		HBox hBox4 = new HBox(5, label4, milesDriven);
		
		Label label5 = new Label("Parking Fees: ");
		parkingFees = new TextField();
		HBox hBox5 = new HBox(5, label5, parkingFees);
		
		Label label6 = new Label("Taxi Charges: ");
		taxiCharges = new TextField();
		HBox hBox6 = new HBox(5, label6, taxiCharges);
		
		Label label7 = new Label("Seminar Fees: ");
		seminarFees = new TextField();
		HBox hBox7 = new HBox(5, label7, seminarFees);
		
		Label label8 = new Label("Lodging Charges: ");
		lodgingChargesPerNight = new TextField();
		HBox hBox8 = new HBox(5, label8, lodgingChargesPerNight);
		
		lbltotalExpenses = new Label();
		lblTotalAllowable = new Label();
		lblExcess = new Label();
		lblSaved = new Label();
		
		Button btnCalc = new Button("Calculate");
		btnCalc.setOnAction(new ButtonClickHandler());
		
		GridPane gridPane = new GridPane();
		gridPane.add(lblTravel, 0, 0);
		gridPane.add(hBox1, 0, 1);
		gridPane.add(hBox2, 0, 2);
		gridPane.add(hBox3, 0, 3);
		gridPane.add(hBox4, 0, 4);
		gridPane.add(hBox5, 0, 5);
		gridPane.add(hBox6, 0, 6);
		gridPane.add(hBox7, 0, 7);
		gridPane.add(hBox8, 0, 8);
		gridPane.add(btnCalc, 0, 9);
		gridPane.add(lbltotalExpenses, 0, 10);
		gridPane.add(lblTotalAllowable, 0, 11);
		gridPane.add(lblExcess, 0, 12);
		gridPane.add(lblSaved, 0, 13);
		
		Scene newScene = new Scene(gridPane, 500, 400);
		arg0.setScene(newScene);
		arg0.show();
		
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			
			//company reimbursements
			final double MEALS_PER_DAY = 47;
			final double PARKING_FEES = 20;
			final double TAXI_CHARGES = 40;
			final double LODGING_CHARGES = 195;
			final double PRIVATE_VEHICLE_PER_MILE = .42;
			
			//converting user input to doubles
			double numDays = Double.parseDouble(days.getText());
			double airfare = Double.parseDouble(airfareCost.getText());
			double carFees = Double.parseDouble(carRentalFees.getText());
			double miles = Double.parseDouble(milesDriven.getText()) *  PRIVATE_VEHICLE_PER_MILE;
			double parkFees = Double.parseDouble(parkingFees.getText());
			double taxi = Double.parseDouble(taxiCharges.getText());
			double seminar = Double.parseDouble(seminarFees.getText());
			double lodging = Double.parseDouble(lodgingChargesPerNight.getText());
			
			//calculating the totals to display 4 pieces of info
			double total = airfare + carFees + (parkFees * numDays) + (taxi * numDays) + seminar + (lodging * numDays);
			double totalAllowed = (MEALS_PER_DAY * numDays) + (numDays * PARKING_FEES) + (TAXI_CHARGES * numDays) + (numDays * LODGING_CHARGES) + miles;
			double excess = total - totalAllowed;
			double saved = (totalAllowed - total);
			
			DecimalFormat dFormat = new DecimalFormat("###,##0.00");
			
			//displaying the results
			lbltotalExpenses.setText("Total Expenses: $" + dFormat.format(total));
			lblTotalAllowable.setText("Total Allowed Expenses: $" + dFormat.format(totalAllowed));
			
			if(excess < 0)
			{
				excess = 0;
				lblExcess.setText("Excess to be Paid: $" + dFormat.format(excess));
			}
			else
			{
				lblExcess.setText("Excess to be Paid: $" + dFormat.format(excess));
			}
			
			if(saved < 0)
			{
				saved = 0;
				lblSaved.setText("Amount Saved: $" + dFormat.format(saved));
			}
			else
			{
				lblSaved.setText("Amount Saved: $" + dFormat.format(saved));
			}
			
			
		}
		
	}
}
