package ex10;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.scene.image.*;
import java.lang.Exception;
import java.lang.String;
import java.text.DecimalFormat;
import java.util.Random;

public class SlotMachine extends Application {

	private Random random;
	private Image img1, img2, img3;
	private ImageView view1, view2, view3;
	private Label lblspinAmount;
	private Label lblTotal;
	private TextField amountInserted;
	private GridPane gridPane;
	private double spinAmount;
	private double totalWon;
	
	public static void main(String[] args) {
		
		launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		Button btnSpin = new Button("Spin");
		Label label = new Label("Amount Inserted: $");
		amountInserted = new TextField();
		
		img1 = new Image("file:Cherry.png");
		img2 = new Image("file:7.png");
		img3 = new Image("file:Lemon.png");
		view1 = new ImageView(img1);
		view1.setFitHeight(150);
		view1.setFitWidth(150);
		view2 = new ImageView(img2);
		view2.setFitHeight(150);
		view2.setFitWidth(150);
		view3 = new ImageView(img3);
		view3.setFitHeight(150);
		view3.setFitWidth(150);
		
		spinAmount = 0;
		totalWon = 0;
		btnSpin.setOnAction(new ButtonClickHandler());
		
		lblspinAmount = new Label("");
		lblTotal = new Label("");
		VBox vBox = new VBox(lblspinAmount, lblTotal);
		
		HBox hBox2 = new HBox(3, label, amountInserted);
		/////////////////
		gridPane = new GridPane();
		gridPane.add(view1, 0, 0);
		gridPane.add(view2, 1, 0);
		gridPane.add(view3, 2, 0);
		gridPane.add(btnSpin, 1, 3);
		gridPane.add(hBox2, 0, 1);
		gridPane.add(vBox, 3, 1);
		gridPane.setVgap(10);
		
		Scene newScene = new Scene(gridPane, 900, 600);
		arg0.setScene(newScene);
		arg0.show();
		
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			
			DecimalFormat dFormat = new DecimalFormat("##,##0.00");
			double input = Double.parseDouble(amountInserted.getText());
			random = new Random();
			int num1 = random.nextInt(3);
			int num2 = random.nextInt(3);
			int num3 = random.nextInt(3);
			
			//setting image to the first view
			if(num1 == 0)
			{
				view1.setImage(img1);
			}
			else if(num1 == 1)
			{
				view1.setImage(img2);
			}
			else
			{
				view1.setImage(img3);
			}
			
			//setting image to the second view
			if(num2 == 0)
			{
				view2.setImage(img1);
			}
			else if(num2 == 1)
			{
				view2.setImage(img2);
			}
			else
			{
				view2.setImage(img3);
			}
			
			//setting image to the third view
			if(num3 == 0)
			{
				view3.setImage(img1);
			}
			else if(num3 == 1)
			{
				view3.setImage(img2);
			}
			else
			{
				view3.setImage(img3);
			}
			
			//comparing images and adding to the total
			if(num1 == num2 && num1 == num3)
			{
				spinAmount = input * 3;
				totalWon += spinAmount;
				lblspinAmount.setText("Amount Won this Spin: $" + dFormat.format(spinAmount));
				lblTotal.setText("Total Amount Won: $" + dFormat.format(totalWon));
			}
			else if(num1 == num2 || num1 == num3)
			{
				spinAmount = input * 2;
				totalWon += spinAmount;
				lblspinAmount.setText("Amount Won this Spin: $" + dFormat.format(spinAmount));
				lblTotal.setText("Total Amount Won: $" + dFormat.format(totalWon));
			}
			else if(num2 == num1 || num2 == num3)
			{
				spinAmount = input * 2;
				totalWon += spinAmount;
				lblspinAmount.setText("Amount Won this Spin: $" + dFormat.format(spinAmount));
				lblTotal.setText("Total Amount Won: $" + dFormat.format(totalWon));
			}
			else if(num3 == num1 || num3 == num2)
			{
				spinAmount = input * 2;
				totalWon += spinAmount;
				lblspinAmount.setText("Amount Won this Spin: $" + dFormat.format(spinAmount));
				lblTotal.setText("Total Amount Won: $" + dFormat.format(totalWon));
			}
			
			//////// if all three images are the same, multiply by 3

			else
			{
				spinAmount = input;
				totalWon -= spinAmount;
				lblspinAmount.setText("Amount Won this Spin: $" + dFormat.format(spinAmount));
				lblTotal.setText("Total Amount Won: $" + dFormat.format(totalWon));
			}
		}
	}
}
