package ex5;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;

public class HeadsOrTails extends Application {

	Image img1;
	Image img2;
	ImageView imageView;
	Random random;
	int randomNum;
	
	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		Label label = new Label("Click Button to simulate coin toss");

		Button btn = new Button("Toss Coin!");
		img1 = new Image("file:heads.png");
		img2 = new Image("file:tails.png");
		imageView = new ImageView();
		
		btn.setOnAction(new ButtonClickHandler());
		
		GridPane gridPane = new GridPane();
		gridPane.add(label, 0, 0);
		gridPane.add(btn, 0, 1);
		gridPane.add(imageView, 0, 2);
		
		Scene newScene = new Scene(gridPane, 600, 600);
		arg0.setScene(newScene);
		arg0.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			
			random = new Random();
			randomNum = random.nextInt(2);

			if(randomNum == 1)
			{
				imageView.setImage(img1);
			}
			else
			{
				imageView.setImage(img2);
			}
		}
	}
}
