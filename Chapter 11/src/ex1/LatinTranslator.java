package ex1;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;

public class LatinTranslator extends Application {

	private Label lblMessage;
	
	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		arg0.setTitle("Latin Translator");

		Button btn1 = new Button("sinister");
		Button btn2 = new Button("dexter");
		Button btn3 = new Button("medium");
		lblMessage = new Label("");
		HBox hBox = new HBox(10, btn1, btn2, btn3, lblMessage);
		
		//calls the buttonclickhandler method and sets the label's text to left
		btn1.setOnAction(new Button1ClickHandler());
		
		//calls the buttonclickhandler method and sets the label's text to right
		btn2.setOnAction(new Button2ClickHandler());
		
		//calls the buttonclickhandler method and sets the label's text to center
		btn3.setOnAction(new Button3ClickHandler());
		
		Scene newScnene = new Scene(hBox, 250, 300);
		arg0.setScene(newScnene);
		arg0.show();
	}
	
	class Button1ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblMessage.setText("left");
		}
	}
	
	class Button2ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblMessage.setText("right");
		}
	}
	
	class Button3ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblMessage.setText("middle");
		}
	}
}

