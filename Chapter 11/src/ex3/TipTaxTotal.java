package ex3;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import java.text.DecimalFormat;

public class TipTaxTotal extends Application {

	private TextField foodCharge;
	private Label lblTip;
	private Label lblSalesTax;
	private Label lblTotal;
	
	public static void main(String[] args) {
		
		launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		Label label = new Label("Enter food charge: ");
		foodCharge = new TextField();
		Button btn = new Button("Calculate");
		Label tip = new Label("Tip: ");
		lblTip = new Label();
		Label sales =  new Label("Sales Tax: ");
		lblSalesTax = new Label();
		Label total = new Label("Total: ");
		lblTotal = new Label();
		
		btn.setOnAction(new ButtonClickHandler());
		
		GridPane gridPane = new GridPane();
		gridPane.add(label, 0, 0);
		gridPane.add(foodCharge, 0, 1);
		gridPane.add(btn, 0, 2);
		gridPane.add(tip, 0, 3);
		gridPane.add(lblTip, 1, 3);
		gridPane.add(sales, 0, 4);
		gridPane.add(lblSalesTax, 1, 4);
		gridPane.add(total, 0, 5);
		gridPane.add(lblTotal, 1, 5);
		
		Scene newScene = new Scene(gridPane, 600, 400);
		arg0.setScene(newScene);
		arg0.show();
		
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			
			double charge = Double.parseDouble(foodCharge.getText());
			final double TIP = .18;
			final double SALES_TAX = .07;
			
			DecimalFormat dFormat = new DecimalFormat("#,##0.00");
			
			double tip = charge * .18;
			double sales = charge * SALES_TAX;
			double total = charge + tip + sales;
			
			lblTip.setText("$" + dFormat.format(tip));
			lblSalesTax.setText("$" + dFormat.format(sales));
			lblTotal.setText("$" + dFormat.format(total));
			
		}
	}
}
