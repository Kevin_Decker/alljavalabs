package ex2;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;

public class NameFormatter extends Application{

	private Label lblFormat;
	private TextField firstName;
	private TextField middleName;
	private TextField lastName;
	private TextField prefTitle;
	
	public static void main(String[] args) {
	
		launch(args);
	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		arg0.setTitle("Name Formatter");
		
		Label firstLabel = new Label("First Name: ");
		Label middleLabel = new Label("Middle Name: ");
		Label lastLabel = new Label("Last Name: ");
		Label prefLabel = new Label("Pref Title: ");
		
		//text fields for user entry
		firstName = new TextField();
		middleName = new TextField();
		lastName = new TextField();
		prefTitle = new TextField();
	
		
		//buttons to change the formatting
		Button btn1 = new Button("Format 1");
		Button btn2 = new Button("Format 2");
		Button btn3 = new Button("Format 3");
		Button btn4 = new Button("Format 4");
		Button btn5 = new Button("Format 5");
		Button btn6 = new Button("Format 6");
		
		//button event handlers
		btn1.setOnAction(new Button1ClickHandler());
		btn2.setOnAction(new Button2ClickHandler());
		btn3.setOnAction(new Button3ClickHandler());
		btn4.setOnAction(new Button4ClickHandler());
		btn5.setOnAction(new Button5ClickHandler());
		btn6.setOnAction(new Button6ClickHandler());
		
		lblFormat = new Label("");
		//lblFormat.setAlignment(Pos.BOTTOM_LEFT);
		
		HBox hBox = new HBox(5, btn1, btn2, btn3, btn4, btn5, btn6, lblFormat);
		VBox vBox = new VBox(5,firstLabel, firstName,middleLabel, middleName,lastLabel, lastName,prefLabel, prefTitle, hBox);
		Scene newScene = new Scene(vBox, 600, 400);
		arg0.setScene(newScene);
		arg0.show();
	}
	
	//button click handlers for all 6 buttons
	class Button1ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblFormat.setText(prefTitle.getText() + ". " + firstName.getText() + " " + middleName.getText() + " " + lastName.getText());
		}
	}
	
	class Button2ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblFormat.setText(firstName.getText() + " " + middleName.getText() + " " + lastName.getText());
		}
	}
	
	class Button3ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblFormat.setText(firstName.getText() + " " + lastName.getText());
		}
	}
	
	class Button4ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblFormat.setText(lastName.getText() + ", " + firstName.getText() + " " + middleName.getText() + ", " + prefTitle.getText() + ".");
		}
	}
	
	class Button5ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblFormat.setText(lastName.getText() + ", " + firstName.getText() + " " + middleName.getText());
		}
	}
	
	class Button6ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) {
			lblFormat.setText(lastName.getText() + ", " + firstName.getText());
		}
	}

}
