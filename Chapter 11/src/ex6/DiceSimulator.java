package ex6;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;

public class DiceSimulator extends Application {

	Image img1;
	Image img2;
	Image img3;
	Image img4;
	Image img5;
	Image img6;
	ImageView imageView;
	ImageView imageView2; 
	Random random;
	int randomNum;
	
	public static void main(String[] args) {
		
		launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		Label label = new Label("Roll the Dice!");
		Button btn = new Button("Roll!");
		
		img1 = new Image("file:dice1.png");
		img2 = new Image("file:dice2.png");
		img3 = new Image("file:dice3.png");
		img4 = new Image("file:dice4.png");
		img5 = new Image("file:dice5.png");
		img6 = new Image("file:dice6.png");
		imageView = new ImageView();
		imageView2 = new ImageView();
		
		btn.setOnAction(new ButtonClickHandler());
		
		GridPane gridPane = new GridPane();
		gridPane.add(label, 0, 0);
		gridPane.add(btn, 0, 1);
		gridPane.add(imageView, 0, 2);
		gridPane.add(imageView2, 1, 2);
		
		Scene newScene = new Scene(gridPane, 500, 400);
		arg0.setScene(newScene);
		arg0.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			
			random = new Random();
			randomNum = random.nextInt(6);
			int randomNum2 = random.nextInt(6);
			
			switch(randomNum)
			{
			case 0: 
				imageView.setImage(img1);
				break;
			case 1: 
				imageView.setImage(img2);
				break;
			case 2:
				imageView.setImage(img3);
				break;
			case 3:
				imageView.setImage(img4);
				break;
			case 4:
				imageView.setImage(img5);
				break;
			case 5:
				imageView.setImage(img6);
				break;
			default:
				break;
			}
			
			switch(randomNum2)
			{
			case 0: 
				imageView2.setImage(img1);
				break;
			case 1: 
				imageView2.setImage(img2);
				break;
			case 2:
				imageView2.setImage(img3);
				break;
			case 3:
				imageView2.setImage(img4);
				break;
			case 4:
				imageView2.setImage(img5);
				break;
			case 5:
				imageView2.setImage(img6);
				break;
			default:
				break;
			}
			
		}
		
	}
}
