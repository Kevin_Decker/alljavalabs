package ex4;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.event.*;

public class PropertyTax extends Application{

	private Label lblAssessment;
	private Label lblTax;
	private TextField propertyValue;
	
	public static void main(String[] args) {
		
		launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		Label label = new Label("Enter actual property value");
		Label prop = new Label("Assessment Value: ");
		lblAssessment = new Label();
		lblTax = new Label();
		Label tax = new Label("Property Tax: ");
		propertyValue = new TextField();
		Button btn = new Button("Calculate");
		
		btn.setOnAction(new ButtonClickHandler());
		
		GridPane gridPane = new GridPane();
		
		gridPane.add(label, 0, 0);
		gridPane.add(propertyValue, 0, 1);
		gridPane.add(btn, 0, 2);
		gridPane.add(prop, 0, 3);
		gridPane.add(lblAssessment, 1, 3);
		gridPane.add(tax, 0, 4);
		gridPane.add(lblTax, 1, 4);
		
		Scene newScene = new Scene(gridPane, 600, 400);
		arg0.setScene(newScene);
		arg0.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			
			double propVal = Double.parseDouble(propertyValue.getText());
			final double PROPERTY_TAX = .64;
			final double TAXES = .6;
			double assessmentValue = propVal * TAXES;
			double propertyTax = (assessmentValue * PROPERTY_TAX) / 100;
			
			String assessment = String.valueOf(assessmentValue);
			lblAssessment.setText("$" + assessment);
			String tax = String.valueOf(propertyTax);
			lblTax.setText("$" + tax);
		}
		
	}
}
