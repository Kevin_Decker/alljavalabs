package ex3;

public class InvalidPrice extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidPrice()
	{
		System.out.println("Can't be a negative price.");
	}
	
}
