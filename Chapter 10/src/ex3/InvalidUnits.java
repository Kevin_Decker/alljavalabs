package ex3;

public class InvalidUnits extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUnits()
	{
		System.out.println("Can't have negative units.");
	}
	
}
