package ex3;

public class RetailItem {

	private String description;
	private int unitsOnHand;
	private double price;
	
	//constructor
	public RetailItem(String desc, int units, double price)
	{
		description = desc;
		unitsOnHand = units;
		this.price = price;
	}
	
	//getters and setters
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getUnitsOnHand() {
		return unitsOnHand;
	}
	public void setUnitsOnHand(int unitsOnHand) {
		this.unitsOnHand = unitsOnHand;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString()
	{
		return "Description: " + description + "\n" + "Units On Hand: " + unitsOnHand + "\n" + "Price: $" + price;
	}
}
