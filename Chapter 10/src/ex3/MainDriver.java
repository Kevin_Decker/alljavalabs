package ex3;

import java.util.Scanner;

public class MainDriver {

	public static void main(String[] args) throws InvalidPrice, InvalidUnits {
		
		Scanner keyboard = new Scanner(System.in);
		
		try 
		{
			System.out.println("Enter Description: ");
			String description = keyboard.nextLine();
			System.out.println("Enter Price: ");
			double price = keyboard.nextDouble();
			System.out.println("Enter Units on Hand: ");
			int units = keyboard.nextInt();
			
			if(price < 0)
			{
				throw new InvalidPrice();
			}
			
			if(units < 0)
			{
				throw new InvalidUnits();
			}
			
			RetailItem retailItem = new RetailItem(description, units, price);
			System.out.println(retailItem);
		}
		catch(InvalidPrice e)
		{
			System.out.println(e.getMessage());
		}
		catch(InvalidUnits e)
		{
			System.out.println(e.getMessage());
		}

	}

}
