package ex6;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class FileDriver {

	public static void main(String[] args) throws IOException {
		
		Scanner keyboard = new Scanner(System.in);
		int[] numbers = {56,4,81, 23};
		int[] outputNums = new int[4];
		
		System.out.println("Enter a text file to write to: ");
		String file = keyboard.nextLine();

		FileArray.writeArray(file, numbers);
		FileArray.readArray(file, outputNums);
		System.out.println(Arrays.toString(outputNums));
		

		}
		
		
	}

