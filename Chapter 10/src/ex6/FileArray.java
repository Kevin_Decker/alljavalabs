package ex6;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileArray {

	public static void writeArray(String filename, int[] array) throws IOException
	{
		FileOutputStream stream = new FileOutputStream(filename);
		DataOutputStream outputFile = new DataOutputStream(stream);
		
		for(int x = 0; x < array.length; ++x)
		{
			try 
			{
				outputFile.writeInt(array[x]);
			}
			catch(FileNotFoundException e)
			{
				e.getMessage();
			}
			catch (IOException e) 
			{
				e.getMessage();
			}
			
		}
		outputFile.close();
	}
	
	public static void readArray(String filename, int[] array) throws IOException
	{
		FileInputStream stream = new FileInputStream(filename);
		DataInputStream inputFile = new DataInputStream(stream);
		
		for(int x = 0; x < array.length; ++x)
		{
			try
			{
				array[x] = inputFile.readInt();
			}
			catch(FileNotFoundException e)
			{
				e.getMessage();
			}
			catch (IOException e) 
			{
				e.getMessage();
			}
		}
		inputFile.close();
	}
	
}
