package ex7;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class EncryptDriver {

	public static void main(String[] args) throws IOException {
		
		//////////////////////////// READING THE ORIGINAL FILE
		
		int[] empty = new int[5];
		File filename = new File("test.txt");
		Scanner inputFile = new Scanner(filename);
		int x = 0;
		while(inputFile.hasNext())
		{
			empty[x] = inputFile.nextInt();
			x++;
		}
		
		inputFile.close();
		
		/////////////////////////////// ENCRYPTING
		
		FileOutputStream outputStream = new FileOutputStream("encrypted.dat");
		DataOutputStream outputFile = new DataOutputStream(outputStream);
		
		for(int i = 0; i < empty.length; ++i)
		{
			char val =  (char)(empty[i] + '0');
			outputFile.writeChar(val+10);
			//outputFile.writeInt(empty[i] + 10);
		}
		System.out.println("Encryption Complete.");
		outputFile.close();
		
	}

}
