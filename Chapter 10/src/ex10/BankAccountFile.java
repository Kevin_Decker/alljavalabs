package ex10;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;

public class BankAccountFile {

	private final int RECORD_SIZE = 44;
	private RandomAccessFile bankAccount;
	
	public BankAccountFile(String filename) throws FileNotFoundException
	{
		bankAccount = new RandomAccessFile(filename, "rw");
	}
	
}
