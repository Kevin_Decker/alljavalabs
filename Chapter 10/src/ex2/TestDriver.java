package ex2;

import java.util.Scanner;

public class TestDriver {

	public static void main(String[] args) throws InvalidTestScore {
		
		Scanner keyboard = new Scanner(System.in);
		double[] scores = new double[3];
		
		try 
		{
			for(int x = 0; x < scores.length; ++x)
			{
				System.out.println("Enter a test score: " + (x+1));
				scores[x] = keyboard.nextDouble();
			}
			TestScores testScores = new TestScores(scores);
			System.out.println("Average: " + testScores.getAverageTestScores());
		}
		catch(InvalidTestScore e)
		{
		
		}
	}
}
