package ex2;

public class InvalidTestScore extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTestScore()
	{
		System.out.println("Can't be negative or higher than a hundo.");
	}
	
}
