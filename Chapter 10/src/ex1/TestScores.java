package ex1;

public class TestScores {

	private double[] testScores;
	
	//constructor
	public TestScores(double[] scores)
	{
		testScores = scores;
	}
	
	//method that returns the average of the testscores
	public double getAverageTestScores()
	{
		double sum = 0;
		for(int i = 0; i < testScores.length; ++i)
		{
			sum += testScores[i];
			
			if(testScores[i] < 0 || testScores[i] > 100)
			{
				throw new IllegalArgumentException("Negative or higher than 100");
			}
		}
		return sum / testScores.length;
	}
}
