package ex9;

import java.io.IOException;
import java.util.Scanner;

public class TestDriver {

	public static void main(String[] args) throws IllegalArgumentException, IOException, ClassNotFoundException {
		
		Scanner keyboard = new Scanner(System.in);
		double[] scores = new double[5];
		
		try 
		{
			for(int x = 0; x < scores.length; ++x)
			{
				System.out.println("Enter a test score: " + (x+1));
				scores[x] = keyboard.nextDouble();
			}
			
			TestScores testScores = new TestScores(scores);
			System.out.println("Average: " + testScores.getAverageTestScores());
			System.out.println();
			testScores.serializeScores();
			//testScores.displayObjects();
			testScores.deserializeScores();
			testScores.displayObjects();
			
		}
		catch(IllegalArgumentException e)
		{
			System.out.println(e.getMessage());
		}
	}
}
