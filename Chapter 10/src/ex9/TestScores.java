package ex9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class TestScores implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double[] testScores;
	
	//constructor
	public TestScores(double[] scores) throws IOException
	{
		testScores = scores;
	}
	
	//method to serialize array
	public void serializeScores() throws IOException
	{
		FileOutputStream outputStream = new FileOutputStream("Objects.dat");
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
		
		for(int x = 0; x <testScores.length; ++x)
		{
			objectOutputStream.writeObject(testScores[x]);
		}
		objectOutputStream.close();
	}
	
	//method to deserialize array
	public void deserializeScores() throws IOException, ClassNotFoundException
	{
		FileInputStream fileInputStream = new FileInputStream("Objects.dat");
		ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
		
		for(int x = 0; x < testScores.length; ++x)
		{
			testScores[x] = (double) objectInputStream.readObject();
		}
		objectInputStream.close();
	}
	
	//method to display the objects
	public void displayObjects()
	{
		for(int x = 0; x < testScores.length; ++x)
		{
			System.out.println("Deserialized score: " + testScores[x]);
		}
	}
	
	//method that returns the average of the testscores
	public double getAverageTestScores()
	{
		double sum = 0;
		for(int i = 0; i < testScores.length; ++i)
		{
			sum += testScores[i];
			
			if(testScores[i] < 0 || testScores[i] > 100)
			{
				throw new IllegalArgumentException("Negative or higher than 100");
			}
		}
		return sum / testScores.length;
	}
}
