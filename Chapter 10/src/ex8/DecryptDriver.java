package ex8;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class DecryptDriver {

	public static void main(String[] args) throws IOException {
		
		//////////////////////////// READING THE ORIGINAL FILE
		
		int[] empty = new int[5];
		File filename = new File("test.txt");
		Scanner inputFile = new Scanner(filename);
		int x = 0;
		while(inputFile.hasNext())
		{
			empty[x] = inputFile.nextInt();
			x++;
		}
		
		inputFile.close();
		
		/////////////////////////////// ENCRYPTING
		
		FileOutputStream outputStream = new FileOutputStream("encrypted.dat");
		DataOutputStream outputFile = new DataOutputStream(outputStream);
		
		for(int i = 0; i < empty.length; ++i)
		{
			char val =  (char)(empty[i] + '0');
			outputFile.writeChar(val+10);
		}
		System.out.println("Encryption Complete.");
		outputFile.close();
		
		/////////////////////////////// READING THE ENCRYPTED FILE
		
		x=0;
		boolean endFile = false;
		char empty2[] = new char[5];
		FileInputStream inputStream2 = new FileInputStream("encrypted.dat");
		DataInputStream dataInputStream2 = new DataInputStream(inputStream2);
		
			while(!endFile)
			{
				try 
				{
					empty2[x] = dataInputStream2.readChar();
					++x;
				}
				catch(EOFException e)
				{
					endFile = true;
				}
			}
		
		dataInputStream2.close();
		
		///////////////////////////////	DECRYPTING
		
		FileOutputStream outputStream2 = new FileOutputStream("decrypted.txt");
		DataOutputStream outputFile2 = new DataOutputStream(outputStream2);
		
		for(int i = 0; i < empty2.length; ++i)
		{
			int val = (int)empty2[i];
			outputFile2.writeInt(val-10);
		}
		System.out.println("Decryption Complete.");
		outputFile2.close();
	}

}
