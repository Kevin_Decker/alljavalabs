package ex11;

public class InvalidShift extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidShift()
	{
		System.out.println("Invalid shift number, must be 1 or 2");
	}
	
}
