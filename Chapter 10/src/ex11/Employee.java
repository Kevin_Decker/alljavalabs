package ex11;

public class Employee {

	private String name;
	private int employeeNumber;
	private String hireDate;
	
	//constructor
	public Employee(String n, int num, String date) throws InvalidEmployeeNumber
	{
		name = n;
		employeeNumber = num;
		hireDate = date;
		
		if(employeeNumber < 0 || employeeNumber > 9999)
		{
			throw new InvalidEmployeeNumber();
		}
	}
	
	public Employee()
	{
		
	}
	
	//getters and setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(int employeeNumber) throws InvalidEmployeeNumber {
		this.employeeNumber = employeeNumber;
	}
	public String getHireDate() {
		return hireDate;
	}
	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	
/*	private boolean isValidEmpNum(String e)
	{
		if(e.matches("[0-9]{3}-[A-Ma-m]"))
		{
			return true;
		}
		return false;
	}*/
	
	@Override
	public String toString()
	{
		return String.format("Name: %s\nEMP Num: %s\nHire Date: %s\n%s", name, employeeNumber, hireDate, employeeNumber);
	}
}
