package ex11;

public class InvalidEmployeeNumber extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidEmployeeNumber()
	{
		System.out.println("Employee # must be b/w 0 and 9999");
	}
	
}
