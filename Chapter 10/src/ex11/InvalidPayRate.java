package ex11;

public class InvalidPayRate extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidPayRate()
	{
		System.out.println("Can't have a negative pay rate");
	}
	
}
