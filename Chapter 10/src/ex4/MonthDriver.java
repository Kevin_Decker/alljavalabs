package ex4;

import java.util.Scanner;

public class MonthDriver {

	public static void main(String[] args) throws InvalidMonthNumber, InvalidMonthName {
		
		Scanner keyboard = new Scanner(System.in);
		
		try 
		{
			System.out.println("Enter a month number in an integer: ");
			int m1 = keyboard.nextInt();
			keyboard.nextLine();
			System.out.println("Enter a second month as a string: ");
			String m2 = keyboard.nextLine();
			
			if(m1 < 1 || m1 > 12)
			{
				throw new InvalidMonthNumber();
			}
			if(m2 != "January" || m2 != "February" || m2 != "March" || m2 != "April" || 
					m2 != "May" || m2 != "June" || m2 != "July" || m2 != "August" || 
					m2 != "September" || m2 != "October" || m2 != "November" || m2 != "December")
			{
				throw new InvalidMonthName();
			}
			
			Month month1 = new Month(m1);
			Month month2 = new Month(m2);
			
			System.out.println(month1);
			System.out.println(month2);
			System.out.printf("Are they equal? %s", month1.equals(month2));
		}
		catch(InvalidMonthNumber e)
		{
			
		}
		catch(InvalidMonthName e)
		{
			
		}
	}

}
