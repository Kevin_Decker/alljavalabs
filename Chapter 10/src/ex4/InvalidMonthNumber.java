package ex4;

public class InvalidMonthNumber extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMonthNumber()
	{
		System.out.println("Invalid month number.");		
	}
	
}
