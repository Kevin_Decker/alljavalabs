package ex4;

public class InvalidMonthName extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMonthName()
	{
		System.out.println("Invalid month name.");	
	}
	
}
