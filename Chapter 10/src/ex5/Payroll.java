package ex5;

public class Payroll {

	private String name;
	private int id;
	private double payRate;
	private double hoursWorked;
	
	public Payroll(String Name, int ID)
	{
		name = Name;
		id = ID;
	}
	
	public void setName(String n)
	{
		name = n;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setID(int i)
	{
		id = i;
	}
	
	public int getID()
	{
		return id;
	}
	
	public void setPayRate(double p)
	{
		payRate = p;
	}
	
	public double getPayRate()
	{
		return payRate;
	}
	
	public void setHoursWorked(double h)
	{
		hoursWorked = h;
	}
	
	public double getHoursWorked()
	{
		return hoursWorked;
	}
	
	public double calcGrossPay()
	{
		return hoursWorked * payRate;
	}
}
