package ex5;

public class EmptyEmployeeName extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmptyEmployeeName()
	{
		System.out.println("No name given for employee.");
	}
	
}
