package ex5;

public class InvalidHoursWorked extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidHoursWorked()
	{
		System.out.println("Hours worked cannot be negative or over 84 hours.");
	}
	
}
