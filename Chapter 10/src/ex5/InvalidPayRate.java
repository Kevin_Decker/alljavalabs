package ex5;

public class InvalidPayRate extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidPayRate()
	{
		System.out.println("Pay rate can't be negative or over 25 an hour.");
	}
	
}
