package ex5;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) throws EmptyEmployeeName, InvalidEmployeeID {
		// TODO Auto-generated method stub

		Scanner userInput = new Scanner(System.in);
		
		try
		{
			System.out.println("Enter name: ");
			String name = userInput.nextLine();
			System.out.println("Enter ID #: ");
			int idNum = userInput.nextInt();
			System.out.println("Enter pay rate: ");
			double pay = userInput.nextDouble();
			System.out.println("Enter hours worked: ");
			double hours = userInput.nextInt();
			
			if(name.isEmpty())
			{
				throw new EmptyEmployeeName();
			}
			if(idNum < 0 || idNum == 0)
			{
				throw new InvalidEmployeeID();
			}
			if(hours < 0 || hours > 84)
			{
				throw new InvalidHoursWorked();
			}
			if(pay < 0 || pay > 25)
			{
				throw new InvalidPayRate();
			}
			
			Payroll p1 = new Payroll(name, idNum);
			p1.setPayRate(pay);
			p1.setHoursWorked(hours);
			
			System.out.println();
			System.out.printf("Your gross pay is: $%,.2f", p1.calcGrossPay());
		}
		catch(EmptyEmployeeName e)
		{
			
		}
		catch(InvalidEmployeeID e)
		{
			
		}
		catch(InvalidHoursWorked e)
		{
			
		}
		catch(InvalidPayRate e)
		{
			
		}
	}

}
