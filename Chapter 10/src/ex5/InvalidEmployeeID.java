package ex5;

public class InvalidEmployeeID extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidEmployeeID()
	{
		System.out.println("Employee ID cannot be negative or zero.");
	}
	
}
