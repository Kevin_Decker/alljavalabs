package ex3;

public class PersonalDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PersonalInformation p1 = new PersonalInformation("Kevin D", "Columbia, IL", 25, "555-555-5555");
		PersonalInformation p2 = new PersonalInformation("Momma D", "Columbia, IL", 100, "444-444-4444");
		PersonalInformation p3 = new PersonalInformation("Papa D", "Columbia, IL", 100, "777-777-7777");
		
		System.out.println("Name: " + p1.getName());
		System.out.println("Address: " + p1.getAddress());
		System.out.println("Age: " + p1.getAge());
		System.out.println("Phone #: " + p1.getPhone());
		System.out.println();
		
		System.out.println("Name: " + p2.getName());
		System.out.println("Address: " + p2.getAddress());
		System.out.println("Age: " + p2.getAge());
		System.out.println("Phone #: " + p2.getPhone());
		System.out.println();
		
		System.out.println("Name: " + p3.getName());
		System.out.println("Address: " + p3.getAddress());
		System.out.println("Age: " + p3.getAge());
		System.out.println("Phone #: " + p3.getPhone());
		
	}

}
