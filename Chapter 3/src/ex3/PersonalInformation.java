package ex3;

public class PersonalInformation {

	// instance fields
	private String name;
	private String address;
	private int age;
	private String phoneNum;
	
	public PersonalInformation(String Name, String Address, int Age, String Phone) 
	{
		name = Name;
		address = Address;
		age = Age;
		phoneNum = Phone;
	}
	
	public void setName(String n)
	{
		name = n;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setAddress(String a)
	{
		address = a;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public void setAge(int g)
	{
		age = g;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public void setPhone(String p)
	{
		phoneNum = p;
	}
	
	public String getPhone()
	{
		return phoneNum;
	}
	
	
	
	
	
	
	
	
	
	
	
}
