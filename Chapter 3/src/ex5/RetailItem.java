package ex5;

public class RetailItem {

	// instance fields 
	private String description;
	private int unitsOnHand;
	private double price;
	
	public void setDesc(String d)
	{
		description = d;
	}
	
	public String getDesc()
	{
		return description;
	}
	
	public void setUnits(int u)
	{
		unitsOnHand = u;
	}
	
	public int getUnits()
	{
		return unitsOnHand;
	}
	
	public void setPrice(double p)
	{
		price = p;
	}
	
	public double getPrice() 
	{
		return price;
	}
	
}
