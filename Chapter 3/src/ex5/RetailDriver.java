package ex5;

public class RetailDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		RetailItem r1 = new RetailItem();
		RetailItem r2 = new RetailItem();
		RetailItem r3 = new RetailItem();
		
		r1.setDesc("Jacket");
		r1.setUnits(12);
		r1.setPrice(59.95);
		
		r2.setDesc("Jeans");
		r2.setUnits(40);
		r2.setPrice(34.95);
		
		r3.setDesc("Shirt");
		r3.setUnits(20);
		r3.setPrice(24.95);
		
		System.out.println(r1.getDesc() + "\n" + r1.getUnits() + "\n" + r1.getPrice() + "\n");
		System.out.println(r2.getDesc() + "\n" + r2.getUnits() + "\n" + r2.getPrice() + "\n");
		System.out.println(r3.getDesc() + "\n" + r3.getUnits() + "\n" + r3.getPrice());
	}

}
