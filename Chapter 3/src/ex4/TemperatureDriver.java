package ex4;
import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter degrees in Fahrenheit");
		double conversion = userInput.nextDouble();

		Temperature t = new Temperature(conversion);
		
		System.out.println();
		System.out.println("Fahrenheit temp is: " + t.getFahrenheit());
		System.out.printf("Fahrenheit to Celsius is: %.2f\n", t.getCelsius());
		System.out.printf("Fahrenheit to Kelvin is: %.2f", t.getKelvin());
		
	}

}
