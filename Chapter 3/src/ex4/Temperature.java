package ex4;

public class Temperature {
	
	// instance fields
	private double ftemp;
	
	// constructor
	public Temperature(double temp)
	{
		ftemp = temp;
	}
	
	public void setFahrenheit(double f)
	{
		ftemp = f;
	}
	
	public double getFahrenheit()
	{
		return ftemp;
	}
	
	public double getCelsius()
	{
		return (5.0/9) * (ftemp - 32);
	}
	
	public double getKelvin()
	{
		return ((5.0/9) * (ftemp - 32)) + 273;
	}

}
