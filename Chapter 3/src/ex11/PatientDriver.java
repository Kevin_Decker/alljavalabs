package ex11;

public class PatientDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Patient p1 = new Patient("Kevin", "Blank", "Decker", "1731", "Columbia", "IL", 11111, "555-555-5555", "Mom, 444-444-4444");
		
		Procedure pro1 = new Procedure("Physical Exam", "1/19/2018", "Dr. Irving", 250.00);		
		Procedure pro2 = new Procedure("X-ray", "1/19/2018", "Dr. Jamison", 500.00);		
		Procedure pro3 = new Procedure("Blood test", "1/19/2018", "Dr. Smith", 200.00);	
		
		System.out.println("Patient Info Below: ");
		System.out.println(p1.getFirstName() + " " + p1.getMiddleName() + " " + p1.getLastName() + "\n" + p1.getAddress() + "\n" + p1.getCity() + ", " + p1.getState() + "" + p1.getZipcode() + "\n" + p1.getPhone() + "\n" + p1.getEmergency());
		
		System.out.println();
		
		System.out.println("Procedure #1: ");
		System.out.println("Name: " + pro1.getNameProcedure() + "\n" + "Date: " + pro1.getDateProcedure() + "\n" + "Practitioner: " + pro1.getNamePractitioner() + "\n" + "Charge: $" + pro1.getCharges());
		
		System.out.println();
		
		System.out.println("Procedure #2: ");
		System.out.println("Name: " + pro2.getNameProcedure() + "\n" + "Date: " + pro2.getDateProcedure() + "\n" + "Practitioner: " + pro2.getNamePractitioner() + "\n" + "Charge: $" + pro2.getCharges());
		
		System.out.println();
		
		System.out.println("Procedure #3: ");
		System.out.println("Name: " + pro3.getNameProcedure() + "\n" + "Date: " + pro3.getDateProcedure() + "\n" + "Practitioner: " + pro3.getNamePractitioner() + "\n" + "Charge: $" + pro3.getCharges());
	}

}
