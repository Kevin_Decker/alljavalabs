package ex11;

public class Patient {

	private String firstName;
	private String middleName;
	private String lastName;
	private String address;
	private String city;
	private String state;
	private int zipcode;
	private String phone;
	private String emergency;
	
	//constructor
	public Patient(String first, String middle, String last, String addres, String cit, String st, int zip, String pNum, String emerg)
	{
		firstName = first;
		middleName = middle;
		lastName = last;
		address = addres;
		city = cit;
		state = st;
		zipcode = zip;
		phone = pNum;
		emergency = emerg;
	}
	
	//mutators
	public void setFirstName(String f)
	{
		firstName = f;
	}
	
	public void setMiddleName(String m)
	{
		middleName = m;
	}
	
	public void setLastName(String l)
	{
		lastName = l;
	}
	
	public void setAddress(String a)
	{
		address = a;
	}
	
	public void setCity(String c)
	{
		city = c;
	}
	
	public void setState(String s)
	{
		state = s;
	}
	
	public void setZipcode(int z)
	{
		zipcode = z;
	}
	
	public void setPhone(String p)
	{
		phone = p;
	}
	
	public void setEmergency(String e)
	{
		emergency = e;
	}
	
	//accessors
	public String getFirstName()
	{
		return firstName;
	}
	
	public String getMiddleName()
	{
		return middleName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public String getState()
	{
		return state;
	}
	
	public int getZipcode()
	{
		return zipcode;
	}
	
	public String getPhone()
	{
		return phone;
	}
	
	public String getEmergency()
	{
		return emergency;
	}
	
}
