package ex11;

public class Procedure {

	private String nameOfProcedure;
	private String dateOfProcedure;
	private String nameOfPractitioner;
	private double chargesOfProcedure;
	
	//constructor
	public Procedure(String namePro, String date, String namePra, double charges)
	{
		nameOfProcedure = namePro;
		dateOfProcedure = date;
		nameOfPractitioner = namePra;
		chargesOfProcedure = charges;
	}
	
	//mutators
	public void setNameProcedure(String nameProcedure) 
	{
		nameOfProcedure = nameProcedure;
	}
	
	public void setDateProcedure(String dateOf)
	{
		dateOfProcedure = dateOf;
	}
	
	public void setNamePractitioner(String namePractitioner)
	{
		nameOfPractitioner = namePractitioner;
	}
	
	public void setCharges(double charge)
	{
		chargesOfProcedure = charge;
	}
	
	//accessors
	public String getNameProcedure()
	{
		return nameOfProcedure;
	}
	
	public String getDateProcedure()
	{
		return dateOfProcedure;
	}
	
	public String getNamePractitioner()
	{
		return nameOfPractitioner;
	}
	
	public double getCharges()
	{
		return chargesOfProcedure;
	}
	
}
