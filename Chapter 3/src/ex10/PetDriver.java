package ex10;

import java.util.Scanner;

public class PetDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your pet's name: ");
		String petName = keyboard.nextLine();
		
		System.out.println("Enter your pet's type: ");
		String petType = keyboard.nextLine();
		
		System.out.println("Enter your pet's age: ");
		int petAge = keyboard.nextInt();
		
		Pet pet1 = new Pet(petName, petType, petAge);
		
		System.out.println();
		System.out.println("Name: " + pet1.getName());
		System.out.println("Type: " + pet1.getType());
		System.out.println("Age: " + pet1.getAge());
	}

}
