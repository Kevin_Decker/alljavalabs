package ex10;

public class Pet {

	private String name;
	private String type;
	private int age;
	
	//constructors
	public Pet()
	{
		
	}
	
	public Pet(String nam, String typ, int ag)
	{
		name = nam;
		type = typ;
		age = ag;
	}
	
	//mutators
	public void setName(String n)
	{
		name = n;
	}
	
	public void setType(String t)
	{
		type = t;
	}
	
	public void setAge(int a)
	{
		age = a;
	}
	
	//accessors
	public String getName()
	{
		return name;
	}
	
	public String getType()
	{
		return type;
	}
	
	public int getAge()
	{
		return age;
	}
	
}
