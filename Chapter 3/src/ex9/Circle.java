package ex9;

public class Circle {

	private double radius;
	private final double PI = 3.14159;
	
	//constructors
	public Circle()
	{
		
	}
	
	public Circle(double rd)
	{
		radius = rd;
	}
	
	//mutator
	public void setRadius(double r) 
	{
		radius = r;
	}
	
	//accessor
	public double getRadius()
	{
		return radius;
	}
	
	//methods
	public double getArea()
	{
		double area; 
		return area = PI * radius * radius;
	}
	
	public double getDiameter()
	{
		double diameter;
		return diameter = radius * 2;
	}
	
	public double getCircumference()
	{
		double circumference;
		return circumference = 2 * PI * radius;
	}
	
	
}
