package ex9;
import java.util.Scanner;

public class CircleDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter a radius: ");
		double rad = userInput.nextDouble();
		
		Circle c1 = new Circle(rad);
		
		System.out.println();
		System.out.printf("Your radius is : %.2f\n", c1.getRadius());
		System.out.printf("The area is : %.2f\n", c1.getArea());
		System.out.printf("The diameter is : %.2f\n", c1.getDiameter());
		System.out.printf("Your circumference is : %.2f", c1.getCircumference());
	}

}
