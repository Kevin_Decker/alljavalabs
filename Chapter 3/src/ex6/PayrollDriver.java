package ex6;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter name: ");
		String name = userInput.nextLine();
		
		System.out.println("Enter ID #: ");
		int idNum = userInput.nextInt();
		
		System.out.println("Enter pay rate: ");
		double pay = userInput.nextDouble();
		
		System.out.println("Enter hours worked: ");
		double hours = userInput.nextInt();
		
		
		Payroll p1 = new Payroll(name, idNum);
		p1.setPayRate(pay);
		p1.setHoursWorked(hours);
		
		System.out.println();
		System.out.printf("Your gross pay is: $%,.2f", p1.calcGrossPay());
	}

}
