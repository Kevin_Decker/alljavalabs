package ex7;

public class WidgetDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Widget w1 = new Widget(20);
		
		System.out.println(w1.getWidget() + " widgets will take " + w1.calcDays() + " days.");
		
		Widget w2 = new Widget(500);
		
		System.out.println();
		System.out.println(w2.getWidget() + " widgets will take " + w2.calcDays() + " days.");
		
		Widget w3 = new Widget(2548);
		
		System.out.println();
		System.out.println(w3.getWidget() + " widgets will take " + w3.calcDays() + " days.");
		
	}

}
