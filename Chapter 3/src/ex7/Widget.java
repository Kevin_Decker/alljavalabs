package ex7;

public class Widget {

	private int widgets;
	private double days;
	
	//constructor
	public Widget(int numWidgets)
	{
		widgets = numWidgets;
	}
	
	//mutators
	public void setWidget(int widget) 
	{
		widgets = widget;
	}
	
	public void setDay(double day)
	{
		days = day;
	}
	
	//accessors
	public int getWidget()
	{
		return widgets;
	}
	
	public double getDay()
	{
		return days;
	}
	
	//method to calculate days
	public double calcDays()
	{
		double oneDayWidget = 160;
		days = widgets / oneDayWidget;
		
		return days;
	}
	
}
