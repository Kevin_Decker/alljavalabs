package ex8;

public class Testscores {

	private double test1;
	private double test2;
	private double test3;
	
	//constructors
	public Testscores() 
	{
		
	}
	
/*	public Testscores(double tOne, double tTwo, double tThree)
	{
		test1 = tOne;
		test2 = tTwo;
		test3 = tThree;
	}*/
	
	//mutators
	public void setTest1(double one)
	{
		test1 = one;
	}
	
	public void setTest2(double two)
	{
		test2 = two;
	}
	
	public void setTest3(double three)
	{
		test3 = three;
	}
	
	//accessors
	public double getTest1()
	{
		return test1;
	}
	
	public double getTest2()
	{
		return test2;
	}
	
	public double getTest3()
	{
		return test3;
	}

	//method to return the average
	public double calcAverage()
	{
		return (test1 + test2 + test3) / 3;
	}
	
}
