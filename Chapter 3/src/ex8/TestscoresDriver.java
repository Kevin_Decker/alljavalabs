package ex8;
import java.util.Scanner;

public class TestscoresDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter test score 1: ");
		double t1 = userInput.nextDouble();
		
		System.out.println("Enter test score 2: ");
		double t2 = userInput.nextDouble();
		
		System.out.println("Enter test score 3: ");
		double t3 = userInput.nextDouble();
		
		Testscores test = new Testscores();
		
		test.setTest1(t1);
		test.setTest2(t2);
		test.setTest3(t3);
		
		System.out.println();
		System.out.printf("Your average score is: %.2f", test.calcAverage());
	}

}
