package ex2;

public class CarDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Car c1 = new Car(2010, "Chevrolet");
	
		
		// will show the "car" accelerating
		c1.Accelerate();
		System.out.println(c1.getSpeed());
		
		c1.Accelerate();
		System.out.println(c1.getSpeed());
		
		c1.Accelerate();
		System.out.println(c1.getSpeed());
		
		c1.Accelerate();
		System.out.println(c1.getSpeed());
		
		c1.Accelerate();
		System.out.println(c1.getSpeed());
		System.out.println();
		
		
		// will show the "car" braking
		c1.Brake();
		System.out.println(c1.getSpeed());
		
		c1.Brake();
		System.out.println(c1.getSpeed());
		
		c1.Brake();
		System.out.println(c1.getSpeed());
		
		c1.Brake();
		System.out.println(c1.getSpeed());
		
		c1.Brake();
		System.out.println(c1.getSpeed());
	}

}
