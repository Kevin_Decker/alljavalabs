package ex2;

public class Car {
	
	// instance fields
	private int yearModel;
	private String make;
	private int speed;
	
	// constructor
	public Car(int year, String Make) 
	{
		yearModel = year;
		make = Make;
		speed = 0;
	}
	
	public void setYear(int y)
	{
		yearModel = y;
	}
	
	public int getYear()
	{
		return yearModel;
	}
	
	public void setMake(String m)
	{
		make = m;
	}
	
	public String getMake()
	{
		return make;
	}
	
	public void setSpeed(int s)
	{
		speed = s;
	}
	
	public int getSpeed()

	{
		return speed;
	}
	
	public double Accelerate()
	{
		return speed += 5;
	}
	
	public double Brake()
	{
		return speed -= 5;
	}
}




