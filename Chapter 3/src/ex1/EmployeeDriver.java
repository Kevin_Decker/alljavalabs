package ex1;

public class EmployeeDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		
		e1.setName("Kevin");
		e1.setID(12345);
		e1.setDept("IT");
		e1.setPos("Web Developer");
		
		System.out.println(e1.getName());
		System.out.println(e1.getID());
		System.out.println(e1.getDept());
		System.out.println(e1.getPos());
		System.out.println();
		
		e2.setName("Rick");
		e2.setID(67890);
		e2.setDept("Accounting");
		e2.setPos("Accountant/CPA");
		
		System.out.println(e2.getName());
		System.out.println(e2.getID());
		System.out.println(e2.getDept());
		System.out.println(e2.getPos());
		System.out.println();
		
		e3.setName("Daryl");
		e3.setID(22222);
		e3.setDept("Management");
		e3.setPos("CEO");
		
		System.out.println(e3.getName());
		System.out.println(e3.getID());
		System.out.println(e3.getDept());
		System.out.println(e3.getPos());
		
	}

}
