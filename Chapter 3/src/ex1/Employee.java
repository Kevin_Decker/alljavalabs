package ex1;

public class Employee {
	
	// instance fields
	private String name;
	private int idNumber;
	private String department;
	private String position;
	
	// getter & setter for name
	public void setName(String str) 
	{
		name = str;
	}
	
	// mutator - method to assign a value to the instance field
	public String getName() 
	{
		return name;
	}
	
	// getter & setter for ID
	public void setID(int idNum) 
	{
		idNumber = idNum;
	}
	
	public int getID() 
	{
		return idNumber;
	}
	
	// getter and setter for department
	public void setDept(String dept) 
	{
		department = dept;
	}
	
	public String getDept() 
	{
		return department;
	}
	
	// getter & setter for position
	public void setPos(String posi) 
	{
		position = posi;
	}
	
	public String getPos() 
	{
		return position;
	}
	
	
	
	
	
	
	
	
	
	
	
}
