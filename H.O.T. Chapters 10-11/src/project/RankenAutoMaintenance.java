package project;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.event.ActionEvent;
import java.text.DecimalFormat;
import javafx.scene.control.Alert.AlertType;

public class RankenAutoMaintenance extends Application {

	private CheckBox chkOil;
	private CheckBox chkLube;
	private CheckBox chkRadiator;
	private CheckBox chkTransmission;
	private CheckBox chkInspection;
	private CheckBox chkMuffler;
	private CheckBox chkTire;
	private TextField txtPartCharges;
	private TextField txtHoursLabor;
	private Label lblError;
	
	public static void main(String[] args) {
		
		launch();

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		arg0.setTitle("Ranken's Automotive Maintenance");
		
		Label routine = new Label("Routine Services");
		Label nonroutine = new Label("Nonroutine Services");
		
		chkOil = new CheckBox("Oil Change ($26.00)");
		chkLube = new CheckBox("Lube Job ($18.00)");
		chkRadiator = new CheckBox("Radiator Flush ($30.00)");
		chkTransmission = new CheckBox("Transmission Flush ($80.00)");
		chkInspection = new CheckBox("Inspection ($15.00)");
		chkMuffler = new CheckBox("Muffler Replacement ($100.00)");
		chkTire = new CheckBox("Tire Rotation ($20.00)");
		
		Label lblParts = new Label("Parts Charges:");
		Label lblHours = new Label("Hours of Labor:");
		txtPartCharges = new TextField("");
		txtHoursLabor = new TextField("");
		
		HBox hBox1 = new HBox(22, lblParts, txtPartCharges);
		HBox hBox2 = new HBox(15, lblHours, txtHoursLabor);
		
		Button calcBtn = new Button("Calculate Charges");
		calcBtn.setOnAction(new ButtonClickHandler());
		Button exitBtn = new Button("Exit");
		exitBtn.setOnAction(e -> System.exit(0));
		
		HBox hBox3 = new HBox(5, calcBtn, exitBtn);
		hBox3.setStyle("-fx-padding: 3 5 0 10");
		
		lblError =new Label("");
		
		GridPane gridPane = new GridPane();
		gridPane.add(routine, 0, 0);
		gridPane.add(chkOil, 0, 1);
		gridPane.add(chkLube, 0, 2);
		gridPane.add(chkRadiator, 0, 3);
		gridPane.add(chkTransmission, 0, 4);
		gridPane.add(chkInspection, 0, 5);
		gridPane.add(chkMuffler, 0, 6);
		gridPane.add(chkTire, 0, 7);
		gridPane.add(nonroutine, 0, 8);
		gridPane.add(hBox1, 0, 9);
		gridPane.add(hBox2, 0, 10);
		gridPane.add(hBox3, 0, 11);
		gridPane.add(lblError, 0, 12);
		
		gridPane.setVgap(5);
		
		Scene newScene = new Scene(gridPane, 400, 400);
		arg0.setScene(newScene);
		arg0.show();
		
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
			
			DecimalFormat dFormat = new DecimalFormat("##,##0.00");
			double total = 0;
			double parts = 0;
			double hours;
			double labor;

			if(chkOil.isSelected()){total += 26;}
			if(chkLube.isSelected()){total += 18;}
			if(chkRadiator.isSelected()){total += 30;}
			if(chkTransmission.isSelected()){total += 80;}
			if(chkInspection.isSelected()){total += 15;}
			if(chkMuffler.isSelected()){total += 100;}
			if(chkTire.isSelected()){total += 20;}
			
			//try-catch to avoid exceptions for no user input or bad user input
			try
			{
				hours = Double.parseDouble(txtHoursLabor.getText());
				parts = Double.parseDouble(txtPartCharges.getText());
				
				if(txtHoursLabor.getText().equals("") || txtPartCharges.getText().equals(""))
				{
					throw new Exception();
				}
				
				labor = hours * 20;
				
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Message");
				alert.setHeaderText("Total Charges: $" + dFormat.format((total + parts + labor)));
				
				alert.showAndWait();
				
			}
			catch(Exception e)
			{
				Alert badAlert = new Alert(AlertType.INFORMATION);
				badAlert.setTitle("Error");
				badAlert.setHeaderText("Error: Input was blank or not a number");
				
				badAlert.showAndWait();
			}
		}
		
	}

}
