package project;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Exception;
import java.lang.String;
import java.util.Optional;
import javafx.scene.control.ChoiceDialog;
import javafx.css.PseudoClass;
import javafx.scene.web.HTMLEditor;

public class TextEditor extends Application {

	private TextArea textArea;
    //private PseudoClass centered;
    //private PseudoClass left;
    //private PseudoClass right;
    
	
	public static void main(String[] args) {
		
		launch();

	}

	@Override
	public void start(Stage arg0) throws Exception {
		
		arg0.setTitle("TextEditor");
	    BorderPane root = new BorderPane();
	    Label label = new Label("Simple Text Editor. Created by Kevin Decker");
	    HBox hBox = new HBox(label);
	    hBox.setPrefHeight(40);
	    
	    /*textArea = new TextArea();
	    textArea.setPrefColumnCount(20);
	    textArea.setPrefRowCount(40);
	    textArea.setStyle("-fx-font-size: 1.10em");*/
	    
		Scene newScene = new Scene(root, 900, 750);
		
		HTMLEditor htmlEditor = new HTMLEditor();
		
	    MenuBar menuBar = new MenuBar();
	    VBox vBox2 = new VBox(menuBar, htmlEditor);
	    menuBar.prefWidthProperty().bind(arg0.widthProperty());
	    root.setTop(vBox2);
	    root.setCenter(textArea);
	    root.setBottom(hBox);

	    //=========================  File menu - new, open, save, exit//
	    Menu fileMenu = new Menu("File");
	    //fileMenu.setStyle("-fx-background-color: blue");
	    //fileMenu.setStyle("-fx-color: white");
	    MenuItem newMenuItem = new MenuItem("New");
	    newMenuItem.setOnAction(e -> {
	    	
	    	textArea.clear();
	    	
	    });
	    MenuItem openMenuItem = new MenuItem("Open");
	    openMenuItem.setOnAction(e-> {
	    	
            FileChooser fileChooser = new FileChooser();

            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
            fileChooser.getExtensionFilters().add(extFilter);
             
            File file = fileChooser.showOpenDialog(arg0);
            if(file != null){
            	htmlEditor.setHtmlText(readFile(file));
            }
	    });
	    
	    MenuItem saveMenuItem = new MenuItem("Save");
	    saveMenuItem.setOnAction(e -> {
	    	
	    	FileChooser fc = new FileChooser();

            FileChooser.ExtensionFilter extFilter = 
                new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
            fc.getExtensionFilters().add(extFilter);
             
            File file = fc.showSaveDialog(arg0);
             
            if(file != null){
                try {
					saveAs(htmlEditor.getHtmlText(), file);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
	    });
	    
	    MenuItem exitMenuItem = new MenuItem("Exit");
	    exitMenuItem.setOnAction(actionEvent -> System.exit(0));

	    fileMenu.getItems().addAll(newMenuItem, openMenuItem, saveMenuItem,
	        new SeparatorMenuItem(), exitMenuItem);
	    
	    menuBar.getMenus().addAll(fileMenu);//, textMenu, alignment, font);

	    String css = TextEditor.class.getResource("/style.css").toExternalForm();
	    newScene.getStylesheets().add(css);
	    
		arg0.setScene(newScene);
		arg0.show();
		
	    
	    //===== DID NOT KNOW OF THE HTMLEditor CLASS UNTIL RECENTLY. USING A MENU I CREATED THE FOLLOWING CAPABILITIES BELOW
	    
	    
	    //=================== ALLOWS THE USER TO BOLD AND ITALICIZE TEXT =======//
	    
	   /* Menu textMenu = new Menu("Modify Text");
	    CheckMenuItem boldMenuItem = new CheckMenuItem("Bold");
	    boldMenuItem.setSelected(false);
	    textMenu.getItems().add(boldMenuItem);
	    
	    boldMenuItem.setOnAction(e -> {
	    	
	    	if(boldMenuItem.isSelected())
	    	{
	    		//textArea.getSelectedText();
	    		textArea.setStyle("-fx-font-weight: bold;");
	    	}
	    	else 
	    	{
	    		textArea.setStyle("-fx-font-weight: normal;");
	    	}
	    	
	    });

	    CheckMenuItem italicMenuItem = new CheckMenuItem("Italicize");
	    italicMenuItem.setSelected(false);
	    textMenu.getItems().add(italicMenuItem);
	    italicMenuItem.setOnAction(e -> {
	    	
	    	if(italicMenuItem.isSelected())
	    	{
	    		textArea.setStyle("-fx-font-style: italic;");
	    	}
	    	else
	    	{
	    		textArea.setStyle("-fx-font-style: normal;");
	    	}
	    });*/
	    

	    //================== ALLOWS THE USER TO ALIGN TEXT ==================//
	    
	  /*  Menu alignment = new Menu("Alignment");
	    MenuItem centerAlign = new MenuItem("Center"); 
	    alignment.getItems().add(centerAlign);
	    centerAlign.setOnAction(e -> {
	    	
	    	centered = PseudoClass.getPseudoClass("centered");
	    	textArea.pseudoClassStateChanged(centered, true);
	    	textArea.pseudoClassStateChanged(left, false);
	    	textArea.pseudoClassStateChanged(right, false);
	    	
	    });
	    
	    MenuItem leftAlign = new MenuItem("Left");
	    alignment.getItems().add(leftAlign);
	    leftAlign.setOnAction(e -> {
	    	
	    	left = PseudoClass.getPseudoClass("left");
	    	textArea.pseudoClassStateChanged(left, true);
	    	textArea.pseudoClassStateChanged(centered, false);
	    	
	    });
	    
	    MenuItem rightAlign = new MenuItem("Right");
	    alignment.getItems().add(rightAlign);
	    rightAlign.setOnAction(e -> {
	    	
	    	right = PseudoClass.getPseudoClass("right");
	    	textArea.pseudoClassStateChanged(right, true);
	    	textArea.pseudoClassStateChanged(left, false);
	    	textArea.pseudoClassStateChanged(centered, false);
	    	
	    });*/
	    
	    //===================================== ALLOWS USER TO CHANGE THE FONT FAMILY AND SIZE
	    
	  /*  Menu font = new Menu("Font");
	    MenuItem arialFont = new MenuItem("Arial");
	    font.getItems().add(arialFont);
	    arialFont.setOnAction(e -> {
	    	textArea.setStyle("-fx-font-family: arial;");
	    });
	    
	    MenuItem verdanaFont = new MenuItem("Verdana"); 
	    font.getItems().add(verdanaFont);
	    verdanaFont.setOnAction(e -> {
	    	textArea.setStyle("-fx-font-family: verdana");
	    });
	    
	    MenuItem timesFont = new MenuItem("Monospace");
	    font.getItems().add(timesFont);
	    timesFont.setOnAction(e -> {
	    	textArea.setStyle("-fx-font-family: monospace");
	    });
	    
	    font.getItems().add(new SeparatorMenuItem());
	    
	    MenuItem fontSize = new MenuItem("Font Size"); 
	    font.getItems().add(fontSize);
	    fontSize.setOnAction(e -> {
	    	
	    	ChoiceDialog<Integer> fontSizes = new ChoiceDialog<Integer>(12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32);
	    	fontSizes.setTitle("Font Size");
	    	fontSizes.setHeaderText("Change Font Size");
	    	fontSizes.setContentText("Size: ");
	    	
	    	Optional<Integer> result = fontSizes.showAndWait();
	    	
	    	if(result.isPresent())
	    	{
	    		if(fontSizes.getSelectedItem() == 12)
	    		{
	    			textArea.setStyle("-fx-font-size: 12px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 14)
	    		{
	    			textArea.setStyle("-fx-font-size: 14px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 16)
	    		{
	    			textArea.setStyle("-fx-font-size: 16px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 18)
	    		{
	    			textArea.setStyle("-fx-font-size: 18px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 20)
	    		{
	    			textArea.setStyle("-fx-font-size: 20px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 22)
	    		{
	    			textArea.setStyle("-fx-font-size: 22px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 24)
	    		{
	    			textArea.setStyle("-fx-font-size: 24px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 26)
	    		{
	    			textArea.setStyle("-fx-font-size: 26px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 28)
	    		{
	    			textArea.setStyle("-fx-font-size: 28px");
	    		}
	    		else if(fontSizes.getSelectedItem() == 30)
	    		{
	    			textArea.setStyle("-fx-font-size: 30px");
	    		}
	    		else
	    		{
	    			textArea.setStyle("-fx-font-size: 32px");
				}
	    	}
	    });
	    
	    MenuItem fontColor = new MenuItem("Font Color");
	    font.getItems().add(fontColor);
	    fontColor.setOnAction(e -> {
	    	
	    	ChoiceDialog<String> fontColors = new ChoiceDialog<String>("Red", "Yellow", "Blue", "Green", "Black");
	    	fontColors.setTitle("Set Font Color");
	    	fontColors.setHeaderText("Set Font Color");
	    	fontColors.setContentText("Type in a font color:");

	    	// Traditional way to get the response value.
	    	Optional<String> result = fontColors.showAndWait();
	    	if (result.isPresent())
	    	{
	    	    if(fontColors.getSelectedItem().equals("Red"))
	    	    {
	    	    	textArea.setStyle("-fx-text-fill: red");
	    	    }
	    	    else if(fontColors.getSelectedItem().equals("Yellow"))
	    	    {
	    	    	textArea.setStyle("-fx-text-fill: yellow");
	    	    }
	    	    else if(fontColors.getSelectedItem().equals("Blue"))
	    	    {
	    	    	textArea.setStyle("-fx-text-fill: blue");
	    	    }
	    	    else if(fontColors.getSelectedItem().equals("Green"))
	    	    {
	    	    	textArea.setStyle("-fx-text-fill: green");
	    	    }
	    	    else
	    	    {
	    	    	textArea.setStyle("-fx-text-fill: black");
	    	    }
	    	}
	    	
	    });*/
		
	}
	
	// method that writes current text in the textarea to a new file
	private void saveAs(String content, File file) throws IOException
	{
		FileWriter fw = new FileWriter(file);
		fw.write(content);
		fw.close();
		
	}
	
	// method that reads a file when opened and brings the text into the text editor
    private String readFile(File file){
        StringBuilder stringBuffer = new StringBuilder();
        BufferedReader bufferedReader = null;
         
        try {
 
            bufferedReader = new BufferedReader(new FileReader(file));
             
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                stringBuffer.append(text);
            }
 
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(TextEditor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(TextEditor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
               // Logger.getLogger(TextEditor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
         
        return stringBuffer.toString();
    }
  
}
